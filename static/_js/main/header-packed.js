	//positions the loading div to middle of current screen
	function positionOverlay() {
		var bH = $(window).height();
		var sW = $(window).width();
		var sT = document.documentElement.scrollTop;
		var nH = (((bH / 2)-30)+sT);
		var nW = ((sW / 2)-100);
		 
		$("#sortoverlay").css({top:nH, left:nW});
	}
	
	//getting vars from query string
	function gup( name ) {
	 	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( window.location.href );
	  if( results == null )
	    return "";
	  else
	    return results[1];
	}
	
	//event to check session time variable declaration (global)
	var checkSessionTimeEvent;
	var countdownTickerEvent; 
	
	$(document).ready(function() {
		//event to check session time left (times 1000 to convert seconds to milliseconds)
    checkSessionTimeEvent = setInterval("checkSessionTime()",10*1000);
    keepMeLoggedInEvent = setInterval("keepMeLoggedIn()",10*1000);
	 	$('.reLogNow').click(function() {
	  	$.get("/admin/AjaXUser.php", { uname:$('#reloguser').val(), repass:$('#relogpass').val(), relogLocale:$('#currLocale').val(), accid:$('#accid').val(), relogNow: true },
		  function(data){
				if(jQuery.trim(data) == 'true') {
					//reset the session times as they have just logged in!
					checkSessionTimeEvent = setInterval("checkSessionTime()",10*1000);
			    pageRequestTime = new Date();
			    timeoutLength = sessionLength*1000;
			    warningTime = timeoutLength - (warning*1000);
			    countdownTime = warning;
			    warningStarted = false;
			    tickerActive = false;
					$("#loginBox").overlay().close();
					$.jGrowl('Your login was successful', {
						header:'<strong>Login Alert</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
				} else {
					alert('Login incorrect, Please try again');
				}
		  });
		});
	 	$('#keepSessionAliveDiv').click(function(){
	 		$('#keepSessionAliveDiv').toggleClass('checked unchecked');
	 		if($('#keepSessionAlive').prop("checked")) {
	 			$('#keepSessionAlive').prop("checked", false);
	 			$(location).attr('href',"/?saviio=main&logout=1");
	 		} else {
	 			$('#keepSessionAlive').prop("checked", true);
	 			$('#keepSessionAliveDivText').hide();
	 			$('#keepSessionAliveDivImg').show();
	 		}
	 	});
	});
	
	//total length of session in seconds
	var sessionLength = 900;
	//time warning shown (10 = warning box shown 10 seconds before session starts)
	var warning = 60;
	//time session started
	var pageRequestTime = new Date();
	//session timeout length
	var timeoutLength = sessionLength*1000;
	//set time for first warning, ten seconds before session expires
	var warningTime = timeoutLength - (warning*1000);
	//set number of seconds to count down from for countdown ticker
	var countdownTime = warning;
	//warning dialog open; countdown underway
	var warningStarted = false;
	var tickerActive = false;
	var refreshTime = warningTime / 2;
	
	function checkSessionTime() {
		var timeNow = new Date(); 
		var timeDifference = 0;
		timeDifference = timeNow - pageRequestTime;
		  if(timeDifference > warningTime && warningStarted === false) {
				$('#sessionWarning').jGrowl("Session Expiration countdown", { 
					speed: 2000,
					sticky: true,
					corners:'8px',
					header:'<strong>Session Expiration</strong>',
					closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>'
				});
		    warningStarted = true;
		    
		    if(tickerActive == false) {
			    //call now for initial dialog box text (time left until session timeout)
			    countdownTicker(); 
			    //set as interval event to countdown seconds to session timeout
			    countdownTickerEvent = setInterval("countdownTicker()", 1000);
			    tickerActive = true;
			  }
		    
		  } else if (timeDifference > timeoutLength){
		    //$("#loginBox").data("overlay").close();
		    $('#sessionWarning').jGrowl('close');
	    	var loginAPI = $("#loginBox").overlay({ 
				  top: 'center', 
				  mask: { 
				      color: '#fff',  
				      loadSpeed: 200, 
				      opacity: 0.5 
				  }, 
				  closeOnClick: false, 
				  onLoad: function() {			  	
						clearInterval(countdownTickerEvent);
						clearInterval(checkSessionTimeEvent);
				  },
				  api: true, 
				  fixed: true
				});
		    loginAPI.load();
		 }	
	}

	function countdownTicker() {
		$(".jGrowl-message").html('<p>Due to inactivity, your session is about to expire, to prevent this from happening please click the "Refresh Session" link below. When your session times out you will have to login again.</p><p>Your session will expire in <strong>' + countdownTime + '</strong></p><p><a href="javascript:void(0);" class="refreshSession">Refresh Session</a></p>');
		countdownTime--;
	}
	
	function keepMeLoggedIn() {
		if($('#keepSessionAlive').length > 0 && $('#keepSessionAlive').prop("checked")) {
			var timeNow = new Date(); 
			var timeDifference = 0;
			timeDifference = timeNow - pageRequestTime;
			if(timeDifference > refreshTime) {
				$.get("/admin/AjaXUser.php", { refreshSession:true },
						function(data){pageRequestTime = new Date();
				});
			}
		}
	}

	$(function() {
		
		jQuery.fn.fadeToggle = function(speed, easing, callback) {
 			return this.animate({opacity: 'toggle'}, speed, easing, callback);
		};

		//SHOWS THE WAITING OVERLAY ON ALL AJAX CALLS
		$("#sortoverlay").ajaxStart(function() {
			positionOverlay();
			//reset timeout script when ajax call is performed
			pageRequestTime = new Date();
			$("#sortoverlay").show(0);
		});	   
		$("#sortoverlay").ajaxComplete(function() {
    	$("#sortoverlay").hide(0);
		});
		
		$(".refreshSession").live("click", function() {
			$.get("/admin/AjaXUser.php", { refreshSession:true },
			  function(data){
					//do nothin here just close the current jGrowl? No need to reset the timer again as this is down above when an ajax call is dealt with
					$('#sessionWarning').jGrowl('close');
			  });
		});

		//TOGGLES THE LOGIN BOX
		$("#loginFloat").click(function() {
			if($("#loginbox").is(':hidden')) {
				$("#loginbox").slideDown(400);
			} else {
				$("#loginbox").slideUp(400);
			}
		});
		
		//TOGGLES THE SETTINGS BOX
		$(".showSettings").click(function() {
			if($("#settingsbox").is(':hidden')) {
				$("#settingsbox").fadeIn('fast');
			} else {
				$("#settingsbox").fadeOut('fast');
			}
		});
		
		//CLOSES THE LOGIN BOX AND FORGOTTEN PWD BOX
		$(".closeme").live("click", function() {
			if($(this).parent().attr("id") == 'closeBtnLogin') {
				$('#loginbox').fadeOut('fast');
				if($('#forgottenpass_box').is(':visible')) {
					$('#forgottenpass_box').fadeOut('fast');
				}
			} else if($(this).parent().attr("id") == 'closeBtnForgot') {
				$('#forgottenpass_box').fadeOut('fast');
			}
		});

		//TOGGLES LANGUAGE SELECTION BOX
		$("#lang_select").hover(
			 function() {$("#langselbox").fadeToggle(200);}, 
       function() {$("#langselbox").fadeToggle(200);} 
		);
		
		//TOGGLE FORGOTTEN PWSD BOX
		$("#forgotpass").click(function() {
			if($("#forgottenpass_box").is(':hidden')) {
				$("#forgottenpass_box").fadeIn('fast');
			} else {
				$("#forgottenpass_box").fadeOut('fast');
			}
		});
				
		$("#resetpass").click(function() {
			$.get("/_inc/AjaXUser.php", { lostuser:$("#lostusername").val(), resetPass:true },
			  function(data){
					$.jGrowl(data, {
						header:'<strong>System Message</strong>',
						closeTemplate:'<div class="jGrowlCloseMe">close (x)</div>',
						position:'bottom-right'
					});
			  });
		});
	  
	});//end doc rdy