

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{ 			// Add your regex rules here, you can take telephone as an example
						"regex":"brak",
						"alertText":"* Pole wymagane",
						"alertTextCheckboxMultiple":"* Prosimy wybrac opcje",
						"alertTextCheckboxe":"* Wymagane pole wyboru"},
					"length":{
						"regex":"brak",
						"alertText":"*Pomiedzy ",
						"alertText2":" a ",
						"alertText3": " znaki dopuszczalne"},
					"maxCheckbox":{
						"regex":"brak",
						"alertText":"* Przekroczono dopuszczalna liczbe zaznaczen"},	
					"minCheckbox":{
						"regex":"brak",
						"alertText":"* Prosimy dokonac wyboru",
						"alertText2":" opcje"},	
					"equals":{
						"regex":"brak",
						"alertText":"* Nieprawidlowe haslo"},		
					"telephone":{
						"regex":/^[0-9\-\(\)\ ]+$/,
						"alertText":"* Nieprawidlowy numer telefonu"},	
					"email":{
						"regex":/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|COM|ORG|NET|EDU|GOV|MIL|BIZ|INFO|MOBI|NAME|AERO|ASIA|JOBS|MUSEUM)$/i,
						//regex":/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/,
						"alertText":"* Nieprawidlowy adres e-mailowy"},	
					"date":{
 "regex":/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
 "alertText":"* Nieprawidlowa data, nalezy wpisac w formacie RRRR-MM-DD"},
					"onlyNumber":{
						"regex":/^[0-9\ ]+$/,
						"alertText":"* Wylacznie cyfry"},	
					"noSpecialCharacters":{
						"regex":/^[0-9a-zA-Z]+$/,
						"alertText":"* Znaki specjalne nie sa dopuszczalne"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Ten uzytkownik jest dostepny",	
						"alertTextLoad":"* Ladowanie, prosze czekac",
						"alertText":"* Taki uzytkownik juz istnieje"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Taka nazwa juz istnieje",
						"alertTextOk":"* Taka nazwa jest dostepna",	
						"alertTextLoad":"* Ladowanie, prosze czekac"},		
					"onlyLetter":{
						"regex":/^[a-zA-Z\ \']+$/,
						"alertText":"* Wylacznie litery"}
            };
            
        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


