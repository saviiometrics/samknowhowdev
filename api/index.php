<?php
session_start();
include_once './_inc/_classes/db-interface.php';
include_once 'utilities.php';
include_once './_inc/_classes/api.php';
$db = new dbInterface();
$response = array('code'=>0, 'message'=>'An unknown internal error has occured');
$xml = isset($_REQUEST['xml'])? $_REQUEST['xml'] : null;
$xml = is_null($xml) && isset($_REQUEST['XML']) ? $_REQUEST['XML'] : $xml;
$postedXML = $xml;
$validXML = true;
try {
	$xml = is_null($xml) ? null : new SimpleXMLElement(utilities::decodeCData($xml));
} catch (Exception $e) {
	$response['message'] = "Supplied XML is invalid";
	$validXML = false;
}
if($validXML) {
	try{
		$origin = $xml->origin;
		$uid = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : $xml->uid;
		$apiKey = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : $xml->apiKey;
	} catch (Exception $e) {
		$origin = null;
		$uid = null;
		$apiKey = null;
	}
	$origin = empty($origin) ? null : $origin;
	$uid = empty($uid) ? null : $uid;
	$apiKey = empty($apiKey) ? null : $apiKey;
	if(!is_null($uid) && !is_null($apiKey)) {
		if(is_null($xml) || !$validXML) {
			$response['message'] = $validXML ? 'XML must be supplied with the key xml' : $response['message']; 
		} else {
			if(is_null($origin)) {
				$response['message'] = 'Origin must be specified';
			} else {
				if($db->UIDHasOriginAccess($origin, $uid)) {
					if($db->UIDHasApiAccess($uid)) {
						if($apiKey == $db->getApiKeyForUID($uid)) {
							$api = new api($uid, $xml, $origin);
							$response = $api->doRequest();
						} else {
							$response['message']='ID and API KEY do not match records';
						}
					} else {
						$response['message']='Specified UID ' . $uid . ' doesn\'t have API access';
					}
				} else {
					$response['message']='Specified UID ' . $uid . ' doesn\'t have access to the ' . $origin . ' API';
				}
			}
		}
	} elseif(!is_null($xml)){
		if($xml->request->function == 'getAPIData') {
			$api = new api(null, $xml, $origin);
			$response = $api->doRequest();
		} else {
			$response['message'] = 'Username and password must be supplied in some form';
		}
	} else {
		$response['message']='Username and password must be supplied';
	}
}
if(!isset($response['code'])) {
	//Assume success and badly written code
	$response['code'] = 1;
}
$message = isset($response['message']) ? $response['message'] : '';
$db->logAPIAccess($uid, $response['code'], $postedXML, $origin, $message);
$response = array_change_key_case($response, CASE_LOWER);
$unformattedXML = utilities::arrayToXml($response);
$dom = new DOMDocument();
$dom->loadXML($unformattedXML);
$dom->formatOutput = true;
$formattedXML = $dom->saveXML();
if(strlen($formattedXML) > 22) {
	echo $formattedXML;
} else {
	echo $unformattedXML;
}
