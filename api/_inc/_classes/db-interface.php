<?php
include_once 'db-class.php';
class dbInterface extends DbaseMySQLi {
	public function __construct() {
		parent::__construct();
	}
	
	public function getFunctionListForOrigin($origin) {
		$out = array();
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare('SELECT func.id, func.name FROM system_api_origins AS origin RIGHT JOIN system_api_originfunctions AS of ON of.origin_id = origin.id RIGHT JOIN `system_api_functions` AS func ON of.function_id = func.id where origin.name = ? OR func.originLocked = "0"');
		$stmt->bind_param('s', $origin);
		$stmt->execute();
		$stmt->bind_result($fid, $funcName);
		while($stmt->fetch()) {
			$out[] = array('id'=>$fid, 'name'=>$funcName);
		}
		$stmt->close();		
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getFunctionData($function, $origin) {
		$out = array();
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare('SELECT func.name, func.description, func.required, func.returns FROM `system_api_origins` AS origin RIGHT JOIN `system_api_originfunctions` AS of ON of.origin_id = origin.id RIGHT JOIN `system_api_functions` AS func ON of.function_id = func.id where (origin.name = ? OR func.originLocked = "0") AND func.name = ?');
		$stmt->bind_param('ss', $origin, $function);
		$stmt->execute();
		$stmt->bind_result($name, $desc, $required, $returns);
		while($stmt->fetch()) {
			$out = array('name'=>$name, 'description'=>$desc, 'requires'=>$required, 'returns'=>$returns);
		}
		$stmt->close();		
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	} 
	
	public function OriginCanUseFunction($origin, $function) {
		$out = false;
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT id, originLocked FROM system_api_functions WHERE name = ?");
		$stmt->bind_param('s', $function);
		$stmt->execute();
		$stmt->bind_result($fid, $originLocked);
		$stmt->fetch();
		$stmt->close();
		if($originLocked == 0 && $fid != 0) {
			$out = true;
		} else {
			$stmt = $this->mySQLConnection->prepare('SELECT of.id FROM `system_api_originfunctions` AS of LEFT JOIN `system_api_origins` AS origin ON origin.id = of.origin_id WHERE of.function_id = ? AND origin.name = ?');
			$stmt->bind_param('is', $fid, $origin);
			$stmt->execute();
			$stmt->bind_result($id);
			$stmt->fetch();
			if($id > 0) {
				$out = true;
			}
			$stmt->close();
		}		
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function UIDHasOriginAccess($origin, $uid){
		$out = false;
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT id, userLocked FROM system_api_origins WHERE name = ?");
		$stmt->bind_param('s', $origin);
		$stmt->execute();
		$stmt->bind_result($originID, $userLocked);
		$stmt->fetch();
		$stmt->close();
		if($userLocked == 0) {
			$out = true;
		} else {
			$stmt = $this->mySQLConnection->prepare("SELECT id FROM system_api_originusers WHERE user_id = ? AND origin_id = ?");
			$stmt->bind_param('ii', $uid, $originID);
			$stmt->execute();
			$stmt->bind_result($id);
			$stmt->fetch();
			if($id > 0) {
				$out = true;
			}
			$stmt->close();
		}		
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getApiKeyForUID($uid) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT API_Token FROM sav_users WHERE uid = ?");
		$stmt->bind_param('i', $uid);
		$stmt->execute();
		$stmt->bind_result($api);
		$stmt->fetch();
		$out = $api;
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getAccidForUID($uid) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT accid FROM sav_users WHERE uid = ?");
		$stmt->bind_param('i', $uid);
		$stmt->execute();
		$stmt->bind_result($accid);
		$stmt->fetch();
		$out = $accid;
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getFullNameForUID($uid) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT firstname, surname FROM sav_users WHERE uid = ?");
		$stmt->bind_param('i', $uid);
		$stmt->execute();
		$stmt->bind_result($firstname, $surname);
		$stmt->fetch();
		$out = "$firstname $surname";
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function UIDHasApiAccess($uid) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT API_Access FROM sav_users WHERE uid = ?");
		$stmt->bind_param('i', $uid);
		$stmt->execute();
		$stmt->bind_result($api);
		$stmt->fetch();
		$out = $api == '1';
		if(!$out) {
			$out = $api == 1;
		}
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function generateNewAPIKey($uid) {
		$connected = $this->mySQLConnect();
		$apikey = sha1(microtime());
		$out = $apikey;
		$stmt = $this->mySQLConnection->prepare("UPDATE sav_users SET API_Token = ? WHERE uid = ?");
		$stmt->bind_param('si', $apikey, $uid);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function enableAPIAccessFor($uid) {
		$connected = $this->mySQLConnect();
		if(!$this->UIDHasApiAccess($uid)) {
			$stmt = $this->mySQLConnection->prepare("UPDATE sav_users SET API_Access = '1' WHERE uid = ?");
			$stmt->bind_param('i', $uid);
			$stmt->execute();
			$stmt->fetch();
			$stmt->close();
			$apikey = $this->getApiKeyForUID($uid);
			if(is_null($apikey) || $apikey == '') {
				$this->generateNewAPIKey($uid);
			}
			$out = true;
			if($connected) {
				$this->mysqlclose();
			}
		} else {
			$out = true;
		}
		return $out;
	}
	
	public function logAPIAccess($uid, $code, $xml, $origin, $message='') {
		$uid = is_null($uid) ? '' : $uid; 
		$code = is_null($code) ? '' : $code; 
		$xml = is_null($xml) ? '' : $xml; 
		$origin = is_null($origin) ? '' : $origin;
		$message = is_null($message) ? '' : $message;  
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("INSERT INTO system_api_log (uid, resultCode, xml, origin, errorMessage, time) VALUES (?, ?, ?, ?, ?, NOW())");
		$stmt->bind_param('iisss', $uid, $code, $xml, $origin, $message);
		$stmt->execute();
		$out = $stmt->insert_id > 0;
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getCountryName($countryid) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT Name FROM system_countries WHERE countryid = ?");
		$stmt->bind_param('i', $countryid);
		$stmt->execute();
		$stmt->bind_result($name);
		$stmt->fetch();
		$out = $name;
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
 	
	public function getUIDforEmailPassword($email, $password) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT uid FROM sav_users WHERE username = ? AND password = ?");
		$stmt->bind_param('ss', $email, $password);
		$stmt->execute();
		$stmt->bind_result($uid);
		$stmt->fetch();
		if($uid == 0) {
			$out = null;
		} else {
			$out = $uid;
		}
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getUIDforEmail($email) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT uid FROM sav_users WHERE email = ?");
		$stmt->bind_param('s', $email);
		$stmt->execute();
		$stmt->bind_result($uid);
		$stmt->fetch();
		$out = $uid;
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getEmailForUID($uid) {
		$connected = $this->mySQLConnect();
		$stmt = $this->mySQLConnection->prepare("SELECT email FROM sav_users WHERE uid = ?");
		$stmt->bind_param('i', $uid);
		$stmt->execute();
		$stmt->bind_result($email);
		$stmt->fetch();
		$out = $email;
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}
	
	public function getQuestionsForFactorGroup($id) {
		$connected = $this->mySQLConnect();
		$out = array();
		$stmt = $this->mySQLConnection->prepare("SELECT fid FROM system_factors WHERE factor_align = ? AND factor_group = ?");
		$align = 'l';
		$stmt->bind_param('si', $align, $id);
		$stmt->execute();
		$stmt->bind_result($fid);
		$stmt->fetch();
		$leftFactor = $fid;
		$align = 'r';
		$stmt->execute();
		$stmt->fetch();
		$rightFactor = $fid;
		$stmt->close();
		$stmt = $this->mySQLConnection->prepare("SELECT qid, response, alignment FROM system_responses WHERE fid = ? OR fid = ?");
		$stmt->bind_param('ii', $leftFactor, $rightFactor);
		$stmt->bind_result($questionID, $responseText, $alignment);
		$stmt->execute();
		while($stmt->fetch()) {
			if(!isset($out["question$questionID"])) {
				$out["question$questionID"] = array();
			}
			$out["question$questionID"]['id'] = $questionID;
			$align = $alignment == 'l' ? 'left' : 'right';
			$out["question$questionID"][$align . "response"] = $responseText;
		}
		$stmt->close();
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}

	public function getFactorGroupDescriptions($id) {
		$connected = $this->mySQLConnect();
		$out = array('left'=>array(), 'right'=>array());
		$stmt = $this->mySQLConnection->prepare("SELECT factor_text, fid, clusterid FROM system_factors WHERE factor_align = ? AND factor_group = ?");
		$align = 'l';
		$stmt->bind_param('si', $align, $id);
		$stmt->execute();
		$stmt->bind_result($text, $fid, $clusterid);
		$stmt->fetch();
		$out['left']['title'] = $text;
		$leftfid = $fid;
		$align = 'r';
		$stmt->execute();
		$stmt->fetch();
		$out['right']['title'] = $text;
		$rightfid = $fid;
		$out['clusterid'] = $clusterid;
		$stmt->close();
		$type = '';
		$stmt = $this->mySQLConnection->prepare("SELECT content FROM system_factorDesc WHERE type = ? AND fid = ?");
		$stmt->bind_param('ss', $type, $fid);
		$stmt->bind_result($text);
		
		foreach(array('desc', 'positive', 'negative') as $type) {
			$key = $type == 'desc' ? 'description' : $type;
			$fid = $leftfid;
			$stmt->execute();
			$i = 0;
			while($stmt->fetch()) {
				$out['left']["$key$i"] = $text;
				$i ++;
			}
			$fid = $rightfid;
			$stmt->execute();
			$i = 0;
			while($stmt->fetch()) {
				$out['right']["$key$i"] = $text;
				$i ++;	
			}
			$i ++;
		}			
		if($connected) {
			$this->mysqlclose();
		}
		return $out;
	}

}
