<?php
class utilities {
	static function arrayToXml($array, $rootNodeName = 'data', $xml=null) { 
		//$array = self::encodeCData($array);
		if (ini_get('zend.ze1_compatibility_mode') == 1) {
			ini_set ('zend.ze1_compatibility_mode', 0);
		} 
		if ($xml == null) {
			$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
		}
		foreach($array as $key => $value) {
			if (is_numeric($key)) {
				$key = "unknownNode_". (string) $key;
			} 
			$key = preg_replace('/[^a-z]/i', '', $key);
			if (is_array($value)) {
				$node = $xml->addChild($key);
				self::arrayToXml($value, $rootNodeName, $node);
			}
			else {
				$value = urlencode($value);
				$xml->addChild($key,$value);
			}
		}
		return $xml->asXML();
	}
	

	
	static function encodeCData($value) {
		$dat = str_replace("/", '//', $value);
		$dat = str_replace("&", '/&', $dat);
		$dat = str_replace('<', '/<', $dat);
		$dat = str_replace('>', '/>', $dat);
		return $dat;
	}
	
	static function decodeCData($xmlstring) {
		$xml = str_replace('/&', '&', $xmlstring);
		$xml = str_replace('/<', '<', $xml);
		$xml = str_replace('/>', '>', $xml);
		$xml = str_replace('//', '/', $xml);
		return $xml;
	}
	
	/**
	 * Returns true if one of the needles is present
	 * @param string $haystack
	 * @param mixed $needle
	 * @return boolean
	 */
	static function strposArray($haystack, $needles) {
		foreach($needles as $needle) {
			if(strpos($haystack, $needle) > -1) {
				return true;
			}
		}
		return false;
	}
}