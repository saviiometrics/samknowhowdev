		
		<?php
			$countries = $controlObj->listCountries();
		?>
			
		<script type="text/javascript">
			$(function() {
				$('.goFreeTrial').click(function() {
					var valid = $("#freeTrialForm").validationEngine('validate');
	  			if(valid == true) {
						 $.post("/AjaXFreeTrial.php", { accName:$('#company').val(), sPhone:$('#phone').val(), contactWhen:$('#contactmein').val(), sEmail:$('#email').val(), sFirstName:$('#first_name').val(), sLastName:$('#last_name').val(), countryid:$('#countryid').val(), sCountry:$('#countryid :selected').text(), freeTrial:true},
						   function(data){
							   	$('#freetrialHeader').html('Thanks for signing up!');
							   	$('#maincontentPad').html('Thanks for your interest in Saviio MAPs!<br /><br />An email has been sent to you to confirm your details and to let you know what happens next.<p>&nbsp;</p><br />');
						   });
					}
			  });
				$("#freeTrialForm").validationEngine('attach');
			});
		</script>
     <div id="innerContent">
	      <h1 class="MWxHeader">Free Trial</h1>
	      <div class="headerDark-Grey-pro subheadtext" id="freetrialHeader">60 days free.. no catch... yes really!</div>
	    	<div id="maincontentPad">
	    		<p>You're invited to use Saviio MAPs free for 60 days. We want you to feel entirely comfortable that using Saviio MAPs provides the missing link to helping people succeed with each other.</p>
		 			<form id="freeTrialForm">
			 			<table border="0" cellpadding="6" cellspacing="0" class="contactTable">
			 				<tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">First Name</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="first_name" maxlength="40" name="first_name" size="20" type="text" class="validate[required,length[3,32]] textfield" /></td>
			 			  </tr>
			 			 	<tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Family Name</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="last_name" maxlength="80" name="last_name" size="20" type="text" class="validate[required,length[3,32]] textfield" /></td>
			 			  </tr>
			 			  <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Email Address</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="email" maxlength="80" name="email" size="20" type="text" class="validate[required,length[3,32],custom[email]] textfield" /></td>
			 			  </tr>
			 			  	<tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Organisation</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="company" maxlength="40" name="company" size="20" type="text" class="validate[required,length[3,32]] textfield" /></td>
			 			  </tr>
			 			  <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Phone Number</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput"><input id="phone" maxlength="40" name="phone" size="20" type="text" class="validate[required,length[3,32],custom[onlyNumberSp]] textfield" /></td>
			 			  </tr>
			 			  <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Country List</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput">
			 						<select name="countryid" id="countryid" class="listfield">
				 						<?php
										foreach($countries as $cid => $country) {
											if($cid == 242) {
												$selected = ' selected="selected"';
											} else {
												$selected = '';
											}
											echo '<option value="' . $cid . '"' . $selected. '>' . $country . '</option>' . "\n";
										}
										?>
								 </select>
								</td>
			 			  </tr>
			 			   <tr>
			 					<td class="contactFormTitle">
			 						<span class="headerDark-Grey-pro">Contact me:</span><br />
			 						<span class="small-required">Required</span>
			 					</td>
			 					<td class="contactFormInput">
			 						<select name="contactmein" id="contactmein" class="listfield">
											<option value="ASAP">ASAP</option>
											<option value="Today">Today</option>
											<option value="This Week">This Week</option>
								 </select>
								</td>
			 			  </tr>
			 			</table>
			 			<p>Checking this box indicates that you accept the terms and conditions of the 60 day free trial.</p>
			 			<p>We won't spam you, sell your details on or any other activity which we wouldn't want to happen to ourselves. It's a promise!</p>
			 			<p>I accept the <a href="http://static.saviio.com/_files/Saviio_60_day_trialAgreement.pdf" target="_blank">Terms & Conditions</a>: <input type="checkbox" value="1" id="confirmTC" name="confirmTC" class="validate[required]"></p>
		 			</form>
		 			<br />
					<a href="javascript:void(0);" class="goFreeTrial"><img src="/_images/freetrial_tab.png" width="166" height="34" /></a><br /><br />
		 	 </div>
  	</div>
  	