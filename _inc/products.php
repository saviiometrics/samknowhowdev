		<div id="mainbox_middle">
			 <div id="top_narrow_header"><img src="/_images/view_products_header.jpg" /></div>
			<div id="main_content">
				<div class="prodholder">
					<div class="prodleftbox" id="fticonbox"></div>
					<div id="ftprod_box" class="productbox">
						<h3><em>Free Trial (Ft)</em></h3>
						<p>The free trial as it suggests is completely free. It gives you access to our admin section and allows you to issue a maximum of 3 MAP's to your friends and family!</p>
					</div>
					<div class="prodrightbox" id="ftpricebox">
						<img src="/_images/findoutmore_reflect_off.jpg" />
					</div>
				</div>
				
				<div class="cleaner"></div>
				<div class="greyline"></div>
				
				<div class="prodholder">
					<div class="prodleftbox" id="qsiconbox"></div>
					<div id="qsprod_box" class="productbox">
						<h3><em>Quick Start (Qs)</em></h3>
						<p>Quick Start is a cheap way to get started with Saviio and gives you a number of advantages and extra features over the Free version. Most importantly it allows you to create talent/team MAPs to enable you to compare groups of people!</p>
					</div>
					<div class="prodrightbox" id="qspricebox">
						<img src="/_images/findoutmore_reflect_off.jpg" />
					</div>
				</div>
				
				<div class="cleaner"></div>
				<div class="greyline"></div>		
						
				<div class="prodholder">
					<div class="prodleftbox" id="priconbox"></div>
					<div id="prprod_box" class="productbox">
						<h3><em>Premium (Pr)</em></h3>
						<p>Our premium accounts allow you access to everything you need in order to use Saviio to it's maximum potential. You can create and store up to 100 MAP's and can organise your data better with sub accounts.</p>
					</div>
					<div class="prodrightbox" id="prpricebox">
						<img src="/_images/findoutmore_reflect_off.jpg" />
					</div>
				</div>
				
				<div style="padding:0px 0px 0px 10px; float:left; width:300px;">
					<h1 class="MWxHeader">Compare our Products</h1>
				</div>
				<div class="cleaner"></div>
				<div class="greyline"></div>	
				
				<div style="padding:18px 0px 0px 10px; float:left; width:908px;">
				<table class="comparetable">
					<thead>
					<tr>
						<th></th>
						<th>
							<a href="saviio/free-trial" class="product">Free Trial</a><br />
							The free version of Saviio MAP's for personal use
							

						</th>
						<th>
							<a href="/uk/get-spotify/unlimited/" class="product">Spotify Unlimited</a><br>
							No time limits, no ads. <a href="/uk/get-spotify/unlimited/">Learn&nbsp;more&nbsp;�</a>
						</th>
						<th>
							<a href="/uk/get-spotify/free/" class="product">Spotify Free</a><br>

							Get a taste of the full Spotify experience. <a href="/uk/get-spotify/free/">Learn&nbsp;more&nbsp;�</a>
						</th>
						<th class="last">
							<a href="/uk/get-spotify/open/" class="product">Spotify Open</a><br>
							The best media player in the world. <a href="/uk/get-spotify/open/">Learn&nbsp;more&nbsp;�</a>

						</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td class="feature">Price</td>
						<td class="even"><span class="GBP"><span class="currency">�</span><span class="ammount GBP">9.99</span></span> per month</td>
						<td class="odd"><span class="GBP"><span class="currency">�</span><span class="ammount GBP">4.99</span></span> per month</td>
						<td class="even">Free (invitation needed)</td>
						<td class="odd">Free</td>
					</tr>
					<tr>
						<td class="feature">Millions of tracks available instantly</td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>

						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes">20 hours/month</td>
					</tr>
					<tr>
						<td class="feature"><a href="/uk/about/local-music/">Play and organise your own MP3s</a></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>

						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
					</tr>
					<tr>
						<td class="feature"><a href="/uk/about/social/">Spotify social</a></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
					</tr>

					<tr>
						<td class="feature"><a href="/uk/about/features/use-from-anywhere/">Take your music abroad</a></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="even yes">14 days</td>
						<td class="odd yes">14 days</td>
					</tr>

					<tr>
						<td class="feature">Spotify radio mode</td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
					</tr>

					<tr>

						<td class="feature">No advertising</td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
					</tr>

					<tr>
						<td class="feature"><a href="/uk/about/features/mobile-music/">Play local files on your mobile</a></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>

						<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>

						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>

					</tr>

					<tr>
						<td class="feature"><a href="/uk/about/features/mobile-music/">Play music from Spotify on your mobile</a></td>

						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
					</tr>
					<tr>
						<td class="feature"><a href="/uk/about/features/offline-mode/">Offline mode on your desktop</a></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>

						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
					</tr>
					<tr>
						<td class="feature"><a href="/uk/about/features/offline-mode/">Offline mode on your mobile</a></td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>

						<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
					</tr>
	<tr>
		<td class="feature">Enhanced sound quality</td>
		<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
		<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
		<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
		<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
	</tr>
					<tr>
						<td class="feature">Exclusive content</td>
						<td class="even yes"><img src="/wp-content/themes/spotify/images/comparebox_marks_yes.gif" alt="Yes" width="17" height="20"></td>
						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
						<td class="even no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>

						<td class="odd no"><img src="/wp-content/themes/spotify/images/comparebox_marks_no.gif" alt="No" width="13" height="16"></td>
					</tr>
					<tr class="buttoncontainer">
						<td></td>
                
                <td><div><a href="/uk/get-spotify/go/premium" class="small_button"><span>Get <span class="product">Spotify Premium</span> �</span></a></div></td>
                
                
                
						<td><div><a href="/uk/get-spotify/go/unlimited" class="small_button"><span>Get <span class="product">Spotify Unlimited</span> �</span></a></div></td>
                
                
                <td><div><a href="/uk/get-spotify/free/#get-started" class="small_button"><span>Get <span class="product">Spotify Free</span> �</span></a></div></td>
                
                
                
                <td><div><a href="/uk/get-spotify/go/open" class="small_button"><span>Get <span class="product">Spotify Open</span> �</span></a></div></td>
                
					</tr>
					</tbody>
				</table>
				</div>
				
   		<div class="cleaner"></div>
  		</div>
  	</div>