		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">Individuals choose to join an organisation for an increasingly wide range of reasons:</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
					<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="featureHighlights">
	     	<div class="contentPadTop">
	     	<p>Corporate reputation and culture, the way they are treated during the recruitment process, the people they meet and the financial and non-financial rewards that they are promised.</p>
				<p>Saviio can help to make the sourcing and hiring process more effective by addressing all aspects of the joining decision through its ever-growing portfolio of tools and practices including:</p>
      	</div>
         <ul>
             <li><strong>Developing the right culture</strong><p>Saviio helps organisations to show the compelling aspects of their cultures to prospective employees through values audits, perception research, internal communication and training and PR.</p></li>
             <li><strong>Capability and competency</strong><p>Saviio takes clients through rigorous and in-depth processes to identify the skill-sets, wider abilities and personal attributes that will ensure the right "fit" between organisation and individual.</p></li>
             <li class="cleaner"><strong>On-boarding</strong><p>For a new employee, particularly one at a senior level or in a highly specialist area, to become effective in the shortest possible time, an organisation needs to engage at the earliest possible opportunity. We work with individual hires to smooth the transition from one employer to another, minimising culture shock and specifying expectations amongst all relevant stakeholders.</p></li>
        </ul>
      </div>
</div>    	