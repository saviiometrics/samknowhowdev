		<script type="text/javascript">
			$(function() {
				$('.infoTab').click(function() {
					var currTabID = this.id;
					$('.infoContent').each(function(index) {
						$(this).hide();
					});
					$('.infoTab').each(function(index) {
						$(this).removeClass('selected');
					});
					$('#info' + currTabID).show();
					$('#' + currTabID).addClass('selected');
				});
			});
		</script>
		
		<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox"><?= _('Saviio MAPs displays - onscreen and in pdf - the results of the completed survey in eleven sets of preferences "opposites" organized into four main factors or groups.'); ?></h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
					<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <ul class="submenu">
      	<li><a href="javascript:void(0);" class="infoTab selected" id="REL"><?= _("Relationships"); ?></a></li>	
      	<li><a href="javascript:void(0);" class="infoTab" id="TAP"><?= _("Thinking and Planning"); ?></a></li>
      	<li><a href="javascript:void(0);" class="infoTab" id="MAK"><?= _("Making Decisions"); ?></a></li>
      	<li><a href="javascript:void(0);" class="infoTab" id="GTD"><?= _("Getting Things Done"); ?></a></li>
	     </ul>
	     <br />
	     
	     <div class="infoLeft infoContent" id="infoREL">
				<span class="contentHeader"><?= _("Relationships"); ?></span>
				<br />
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Wary"); ?></span>
					<p><?= _("Cautious and politically aware. Could appear unfriendly and not approachable to others."); ?></p>
				</div>
				<div class="prefRight">
					<span class="preferenceHeader"><?= _("Trusting"); ?></span>
					<p><?= _("Assumes the best in others. Could therefore be manipulated and exploited."); ?></p>
				</div>
				<div class="cleaner"></div>
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Concern for Task"); ?></span>
					<p><?= _("Revels in systems and processes. Could therefore appear insensitive to others."); ?></p>
				</div>
				<div class="prefRight">
					<span class="preferenceHeader"><?= _("Concern for People"); ?></span>
					<p><?= _("Interested in people, their interest and feelings. Could lose the connection to the task."); ?></p>
				</div>
				<div class="cleaner"></div>
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Independent"); ?></span>
					<p><?= _("Likes to 'go it alone' when making decisions. May not take the experiences of others into account. May not like to share the credit."); ?></p>
				</div>
				<div class="prefRight">
					<span class="preferenceHeader"><?= _("Interdependent"); ?></span>
					<p><?= _("May check out stuff with others first. May be influenced too much by their opinion."); ?></p>
				</div>
			 </div>
			 

			 <div class="infoLeft infoContent" id="infoTAP">
				<span class="contentHeader"><?= _("Thinking and Planning."); ?></span>
				<br />
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Trouble-Shooter"); ?></span>
					<p><?= _("Motivated by minimising or avoiding risk. May be concentrated too much for what may go wrong."); ?></p>
				</div>
				<div class="prefRight">
					<span class="preferenceHeader"><?= _("Risk-Taker"); ?></span>
					<p><?= _("Risk-taker, motivated by commission. May be ignorant to things that may stop them"); ?></p>
				</div>
				<div class="cleaner"></div>
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Similar"); ?></span>
					<p><?= _("Likes stability and prefers to have things in common. May be uncomfortable with change or new situations"); ?></p>
				</div>
				<div class="prefRight">
					<span class="preferenceHeader"><?= _("Different"); ?></span>
					<p><?= _("Relishes change, variety and differences. Motivated by alternation. May therefore initiate change just for the sake of it!"); ?></p>
				</div>
				<div class="cleaner"></div>
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Procedures"); ?></span>
					<p><?= _("Thrives on working within set procedures and 'the right' way to do things. However, may be stuck for ideas when there isn't a set way of doing things."); ?></p>
				</div>
				<div class="prefRight">	
					<span class="preferenceHeader"><?= _("Options"); ?></span>
					<p><?= _("Good at creating new approaches and considering the options but may waste time on unnecessary alternatives"); ?></p>
				</div>
			  <div class="cleaner"></div>
			 </div>
			 

			 <div class="infoLeft infoContent" id="infoMAK">
				 <span class="contentHeader"><?= _("Making Decisions"); ?></span>
				 <br />
				 <div class="prefLeft">
					 <span class="preferenceHeader"><?= _("Thinking"); ?></span>
					 <p><?= _("Thoroughly evaluates before deciding but may miss the moment or depend on others to get the ball rolling"); ?></p>
				 </div>
				 <div class="prefRight">
					 <span class="preferenceHeader"><?= _("Doing"); ?></span>
					 <p><?= _("Action-orientated and swift yet may make self-enforced errors in judgement and action"); ?></p>
				 </div>
				 <div class="cleaner"></div>
				 <div class="prefLeft">
					 <span class="preferenceHeader"><?= _("Rational"); ?></span>
					 <p><?= _("Demands evidence to support decisions not mere hunches! May disregard intuitive essentials to take decisions and may also disregard possibilities which are not proven yet."); ?></p>
				 </div>
				 <div class="prefRight">
					 <span class="preferenceHeader"><?= _("Intuitive"); ?></span>
					 <p><?= _("Decisions are based upon a complex composition of conscious and unconscious perceptions. Could underestimate essential facts. Could be seen as an unrealistic person."); ?></p>
				 </div>
				 <div class="cleaner"></div>
				 <div class="prefLeft">
					 <span class="preferenceHeader"><?= _("Social Independence"); ?></span>
					 <p><?= _("Decision making is based more on independent factors rather than what's popular. The decisions made are regarded by others as altruistic yet maybe unpopular and suffer in their acceptance."); ?></p>
				 </div>
				 <div class="prefRight">
					 <span class="preferenceHeader"><?= _("Social Compliance"); ?></span>
					 <p><?= _("Decision making is based more on considering social norms and predicted behaviour. The decisions made are popular yet may suffer in quality and be regarded as opportunistic."); ?></p>
				 </div>
				 <div class="cleaner"></div>
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Social Independence"); ?></span>
					<p><?= _("Decision making is based more on independent factors rather than what's popular. The decisions made are regarded by others as altruistic yet maybe unpopular and suffer in their acceptance."); ?></p>
				</div>
				<div class="prefRight">	
					<span class="preferenceHeader"><?= _("Social Compliance"); ?></span>
					<p><?= _("Decision making is based more on considering social norms and predicted behaviour. The decisions made are popular yet may suffer in quality and be regarded as opportunistic."); ?></p>
				</div>
				<div class="cleaner"></div>
			 </div>
			 

			 <div class="infoLeft infoContent" id="infoGTD">
				<span class="contentHeader"><?= _("Getting Things Done"); ?></span>
				<br />
				<div class="prefLeft">
					<span class="preferenceHeader"><?= _("Specific"); ?></span>
					 <p><?= _("The preferred action is driven by dealing intensively with detail. Could disregard 'the bigger picture'. Could be frustrated if details are missing."); ?></p>
				 </div>
				 <div class="prefRight">
					 <span class="preferenceHeader"><?= _("General"); ?></span>
					 <p><?= _("The preferred action origins from 'the bigger picture', concepts and general ideas. Can be bored or distracted by too much detail."); ?></p>
				 </div>
				 <div class="cleaner"></div>
				 <div class="prefLeft">
					 <span class="preferenceHeader"><?= _("Finisher"); ?></span>
					 <p><?= _("Resilient and determined to see things through to completion. Could feel frustrated if instructed to move on without closing unfinished things."); ?></p>
				 </div>
				 <div class="prefRight">
					 <span class="preferenceHeader"><?= _("Starter"); ?></span>
					 <p><?= _("Happy to start and multi-task several projects at a time. Could handle deadlines, priorities and finishing projects in a too flexible way."); ?></p>
				 </div>
				 <div class="cleaner"></div>
				 <div class="prefLeft">
					 <span class="preferenceHeader"><?= _("Perfectionist"); ?></span>
					 <p><?= _("Sets very high standards to get things 'just right'. Could mean they don't know when to let go and start another task."); ?></p>
				 </div>
				 <div class="prefRight">
					 <span class="preferenceHeader"><?= _("Pragmatist"); ?></span>
					 <p><?= _("Focus on what can reasonably be achieved. Could inappropriately lower standards to get it done."); ?></p>
				 </div>
				 <div class="cleaner"></div>
			</div>
   </div> 