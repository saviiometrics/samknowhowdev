		
			<div id="innerContent">
			<div id="contentMenuBar">
	      <div id="contentSiFR">
	      	<h2 class="MWxBox">
						The first 100 days with your new starter will determine if they will succeed or fail with you and your organisation.<br /><br />
						What can you do to positively influence this situation?
					</h2>
	      </div>
	      <div class="cleaner"></div>
	     </div>
	     <div id="contentSideGFX">
	     	<img src="/_images/_sidebar/<?=$sideGFX;?>" width="194" height="150" />
	     </div>
	     <div class="cleaner"></div>
	     <div class="caseStudyList">
	     	<div class="cleaner"></div>
					<div id="maincontentPad">
						<span class="contentHeader">"Our company's greatest asset is our people!"</span>
						<p>Seems sensible enough. The reality is, people are NOT your greatest asset, unless they're in position to work in a way that they prefer - in a way that they find motivating - those things they do well consistently and energetically. Tapping into a person's unique motivation and preferences (MAP) is the key to helping people succeed.</p>
						<p>Years of research show that individuals and teams outperform in almost every business metric when they are more motivated and are allowed to exercise their preferred way of working. In fact, the single best predictor of a consistently high-performing team is the answer to this question:</p>
						<span class="contentHeader">"At work, do you have the opportunity to do what you do best everyday in a way that makes sense to you?"</span>
						</p>
						<p>Teams with individuals who do - massively outperform teams with people who don't - they're more profitable, more productive, less likely to quit, less likely to have accidents on the job... the list goes on.</p>
						<p>That's compelling, but this is confounding: Gallup research reveals that only 12% of people in the workplace play to their strengths "most of the time." In general, society is fascinated by weaknesses (most employee reviews bear this out), and we take strengths for granted. Saviio MAPs helps to link WHAT people do with HOW they prefer to do it.</p>
						<span class="contentHeader">An engaged workforce will make a big difference to your business. </span>
						<ul>
							<li>They'll each take 5 fewer sick days a year and raise profitability by 12% , as well as productivity by 18%</li>
							<li>This same workforce will also generate 43% more revenue .</li>
							<li>And, if you asked them, they'd tell you they were 87% less likely to leave.</li>
						</ul>
						<p>Source: MacLeod and Clarke (2009) - "Engaging for Success: Enhancing performance through employee engagement"<br /><br />
						<a href="http://www.businesslink.gov.uk/employeeengagement" target="_blank">http://www.businesslink.gov.uk/employeeengagement</a></p>
					</div>
      </div>
		</div>