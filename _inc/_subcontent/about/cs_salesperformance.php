		<div id="innerContent">
			<div id="caseStudyTop">
				<h1>A Business process Outsourcing Call Centre</h1>
				<p><strong>Same People | Same Products | Busted Sales Targets! How did they do that?</strong></p>
			</div>
			<div class="cleaner"></div>
	    <div class="caseStudyList">	
         <div class="infoLeft">
         	<img src="/_images/_sidebar/sidebar_popup.jpg" width="194" height="150" align="left" />
         	<strong>The Situation</strong>
         	<p>The organisation provides call centre services to it's clients in London, Brighton and Nottingham, England  - specialising in outbound selling through specialist teams dedicated to their clients' products and services (Orange, Sky, Barclaycard and others).</p>
         </div>
         <div class="infoRight">
         	<img src="/_images/_sidebar/sidebar_quadman.jpg" width="194" height="150" align="right" />
         	<strong>The Challenges</strong>
         	<p>How do you align new hires so that they quickly absorb their clients' culture, get to grips with the product range and sales process and then hit target? Recruiting people capable of outbound sales calls is part of the challenge. The other main challenge is how to re-energise and motivate existing people to perform well - consistently.</p>
         </div>
          <div class="infoLeft">
         	<strong>The Solution</strong><p>The contact centre profiled the top performers within the role at their client with Saviio MAPs to create a talentMAP. This was used as an essential part of the selection and recruitment process by benchmarking each individual against this talentMAP.</p> 
					<p>Internal Sales Trainers were taught how to interpret Saviio MAPs for their teams and then rewrite the training programmes to cater towards these preferences.</p>
         </div>
         <div class="infoRight">
         	<strong>The Outcomes</strong>
         		<p>This is a direct quotation from a letter written by a Saviio MAPs client:</p>
	         	 <ul>
			       	<li>Since attending the training I have re-designed the Sales and Communications Skills workshop using NLP principles, with very positive results, 55 % of new starters attending this workshop had exceeded their sales target within 4 days of being on the phones</li>
							<li>Another important benefit is that agents are maintaining their improvements over a longer period indicating that they have really bought in to the ideas and key learning points and not just implemented a quick fix.</li>
							<li>New agents attending a sales workshop before starting on the phones, have maintained a consistent conversion rate of 9.5% and exceed their premium income target in the first month by &pound;17,485 that's up 27%!</li>
							<li>I used my knowledge of Saviio MAPs in the design and delivery of training for Orange outbound and also in support of the campaign, with very pleasing results. All 3 sites over achieved on this campaign however Nottingham's conversion rate continued to improve throughout, whereas the other sites ( whose trainers on this campaign had not attended Saviio MAPs training) peaked and then decreased.</li>
							<li>This demonstrates again, that the agents bought in to the learning points and used the key skills consistently.</li>
						</ul>
         </div>
      </div>
		</div>
