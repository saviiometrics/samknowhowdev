
		<div class="cleaner"></div>
		<div id="mainbox_btm"></div>
		<div id="footer">Copyright © Saviio <?=date("Y", time());?></div>
		<div id="btmInfo">
			Copyright © Saviio Ltd. 2005-<?=date("Y", time());?>. All rights reserved.<br />
			5th Floor, 2 Wellington Place, Leeds, LS1 4AP<br />
			+44 113 366 2000 | Email: <a href="mailto:info@saviio.com" target="_blank">info@saviio.com</a> | Twitter: <a href="http://www.twitter.com/saviio" target="_blank">@saviio</a><br />
		</div>
	</div>
</body>
</html>