﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Aggiungere qui le proprie espressioni regolari, il telefono può essere usato com esempio
						"regex":"nessuno",
						"alertText":"* Testo obbligatorio",
						"alertTextCheckboxMultiple":"* Selezionare un'opzione",
						"alertTextCheckboxe":"* Casella di controllo obbligatoria"},
					"length":{
						"regex":"nessuno",
						"alertText":"* Tra ",
						"alertText2":" e ",
						"alertText3": " caratteri consentiti"},
					"maxCheckbox":{
						"regex":"nessuno",
						"alertText":"* Indica le eccezioni consentite"},	
					"minCheckbox":{
						"regex":"nessuno",
						"alertText":"* Selezionare ",
						"alertText2":" opzioni"},	
					"confirm":{
						"regex":"nessuno",
						"alertText":"* Le password non corrispondono"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Numero di telefono non valido"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Indirizzo e-mail non valido"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Data non valida; è necessario utilizzare il formato AAAA-MM-GG"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Solo numeri"},	
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Caratteri speciali non consentiti"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Utente disponibile",	
						"alertTextLoad":"* Caricamento in corso. Attendere",
						"alertText":"* Utente già scelto"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Nome già scelto",
						"alertTextOk":"* Nome disponibile",	
						"alertTextLoad":"* Caricamento in corso. Attendere"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Solo lettere"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});
