﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Tilføj dine regex regler her, du kan tage telefon som et eksempel
						"regex":"ingen",
						"alertText":"* Dette felt er obligatorisk",
						"alertTextCheckboxMultiple":"* Venligst vælg en valgmulighed",
						"alertTextCheckboxe":"* Dette afkrydsningsfelt er obligatorisk"},
					"length":{
						"regex":"ingen",
						"alertText":"*Mellem ",
						"alertText2":" og ",
						"alertText3": " tilladte tegn"},
					"maxCheckbox":{
						"regex":"ingen",
						"alertText":"* Tilladte afkrydsninger overskredet"},	
					"minCheckbox":{
						"regex":"ingen",
						"alertText":"* vælg venligst ",
						"alertText2":" valgmuligheder"},	
					"confirm":{
						"regex":"ingen",
						"alertText":"* Dine passwords matcher ikke"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* ugyldigt telefonnummer"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Ugyldig e-mail adresse"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Ugyldig dato, skal være i YYYY-MM-DD format"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Kun tal"},	
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Ingen specielle tegn tilladt"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Denne bruger er ledig",	
						"alertTextLoad":"* Loader, vent venligst",
						"alertText":"* Denne bruger er allerede optaget"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Dette navn er allerede optaget",
						"alertTextOk":"* Dette navn er ledigt",	
						"alertTextLoad":"* Loader, vent venligst"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Kun bogstaver"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});