﻿

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Legg til regex-reglene her, du kan ta telefon som et eksempel
						"regex":"ingen",
						"alertText":"* Dette feltet må fylles ut",
						"alertTextCheckboxMultiple":"* Vennligst foreta et valg",
						"alertTextCheckboxe":"* Du må krysse av her"},
					"length":{
						"regex":"ingen",
						"alertText":"*Mellom ",
						"alertText2":" og ",
						"alertText3": " tegn er lov"},
					"maxCheckbox":{
						"regex":"ingen",
						"alertText":"* Du har valgt for mange"},	
					"minCheckbox":{
						"regex":"ingen",
						"alertText":"* Vennligst velg ",
						"alertText2":" valg"},	
					"confirm":{
						"regex":"ingen",
						"alertText":"* Passordene stemmer ikke overens"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Ugyldig telefonnummer"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Ugyldig epostadresse"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Ugyldig dato, dato må angis som YYYY-MM-DD"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Kun tall"},	
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Ingen spesialtegn tillatt"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Denne brukeren er tilgjengelig",	
						"alertTextLoad":"* Laster, vennligst vent",
						"alertText":"* Denne brukeren er allerede tatt"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Dette navnet er allerede tatt",
						"alertTextOk":"* Dette navnet er tilgjengelig",	
						"alertTextLoad":"* Laster, vennligst vent"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Kun bokstaver"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});