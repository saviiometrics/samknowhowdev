

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{    			// Añade aquí tus reglas regex, puedes utilizar teléfono como ejemplo 
						"regex":"ninguna",
						"alertText":"* Campo obligatorio",
						"alertTextCheckboxMultiple":"* Por favor, selecciona una opción",
						"alertTextCheckboxe":"* Casilla de verificación obligatoria"},
					"length":{
						"regex":"ninguna",
						"alertText":"*Entre ",
						"alertText2":" y ",
						"alertText3": " caracteres permitidos"},
					"maxCheckbox":{
						"regex":"ninguna",
						"alertText":"* Se ha excedido el número de selecciones permitido"},	
					"minCheckbox":{
						"regex":"ninguna",
						"alertText":"* Por favor, selecciona ",
						"alertText2":" opciones"},	
					"confirm":{
						"regex":"ninguna",
						"alertText":"* Tus contraseñas no coinciden"},		
					"telephone":{
						"regex":"/^[0-9\-\(\)\ ]+$/",
						"alertText":"* Número de teléfono inválido"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						//"regex":"/^[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/",
						"alertText":"* Dirección de e-mail inválida"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* Fecha inválida, debe ser en formato AAAA-MM-DD"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* Sólo cifras"},	
					"noSpecialCharacters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* Uso de caracteres especiales no permitido"},	
					"ajaxUser":{
						"file":"validateUser.php",
						"extraData":"",
						"alertTextOk":"* Este usuario está disponible",	
						"alertTextLoad":"* Cargando. Por favor, espera",
						"alertText":"* Este usuario ya está en uso"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* Este nombre ya está en uso",
						"alertTextOk":"* Este nombre está disponible",	
						"alertTextLoad":"* Cargando. Por favor, espera"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* Sólo letras"}
					}	
		}
	}
})(jQuery);

$(document).ready(function() {	
	$.validationEngineLanguage.newLang()
});
