<?php

session_start();
$phpsessid = session_id();
$start_time=microtime(true);

if((!isset($_SESSION['arrDeviations']) || (array)$_SESSION['arrDeviations'] !== $_SESSION['arrDeviations']) || (!isset($_SESSION['arrFactorScores']) || (array)$_SESSION['arrFactorScores'] !== $_SESSION['arrFactorScores'])) {
	printf (_("I'm sorry, there seems to be a problem generating your PDF because your sessions have timed out or been reset! Please close this window and re login to your %s account. Thank You"), 'Saviio');
	exit;
}

//include localization and site config files
require_once("site.config.php");
//include DB AND ACCOUNT INFO CLASSES
include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';
$accobj = new Account($_SESSION['accid']);

//include other classes
include FULL_PATH . '/_inc/_classes/question-class.php';
include FULL_PATH . '/_inc/_classes/MAP-class.php';
include FULL_PATH . '/_inc/_classes/user-class.php';
include FULL_PATH . '/_inc/_classes/admin-class.php';
include FULL_PATH . '/_inc/_classes/pdf-class.php';

//$_GET['pdflocale'] = 'da_DK';
require_once(FULL_PATH . "/_inc/localization.php");
require_once(FULL_PATH . "/_inc/scripts.php");

$dispPDFObj = new DisplayPDF();
$fontn = $dispPDFObj->getFont($_SESSION['locale']);
$_SESSION['fontn'] = $fontn;
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer

// create new PDF document
$PDF_PAGE_ORIENTATION = 'L';
$pdf = new SaviioTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, 16.8, 55.5);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor(ACC_NAME);
$pdf->SetTitle(ACC_NAME . ' Report');
$pdf->SetSubject('Analytics Summary Report');
$pdf->SetKeywords(ACC_NAME . ',analytics, summary, report');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

$userobj = new Admin($_SESSION['uid']);
$MAPobj = new MAP();
$fids = $MAPobj->getfids();
$fgs = $MAPobj->getFactorGroups();
$cids = $MAPobj->getClusters();
$stquestions = $_SESSION['stquestions'];

$access = $userobj->getLevelAccess('tmap');
if($access['EDIT'] == 'MA') {
	$compid = $userobj->headid;
} else {
	$compid = $userobj->companyid;
}

//get factor scores set from AjaXGen Script!
$arrFactorScores = $_SESSION['arrFactorScores'];
$arrDeviations = $_SESSION['arrDeviations'];

if(!isset($_SESSION['fdescs'])) {
	$fdescs = $MAPobj->getFactDescs();
	$_SESSION['fdescs'] = $fdescs;
} else {
	$fdescs = $_SESSION['fdescs'];
}
//$rankid = $_GET['rankID'];
if(!isset($_SESSION['rankID'])) {
	$rankid = 0;
} else {
	$rankid = $_SESSION['rankID'];
}

$font = 'helvetica';

// ---------------------------------------------------------

// set font
$pdf->SetFont($fontn, '', 16);

// add a page
$pdf->AddPage();
$timenow = time();

if(count($_SESSION['rank']) == 0) {
	$headerpagehtml .= '<table border="0" cellpadding="0" cellspacing="0" width="506"><tr><td width="506" align="center">I\'m sorry you must generate a report that has more than one item in it!</td></tr></table>';
} else {
	$rankOrder = '<tr><td width="506" align="center"><br />' . _('Rank Order') . '<br /><br />';
	$kr = 0;
	foreach($_SESSION['rank'] AS $rankid => $arrdata) {
		$kr++;
		$rankOrder .= ordinal($kr) . ' - ' . $arrFactorScores[$rankid]['rightname'] . '<br />';
	}
	$rankOrder .= '<br /></td></tr>';
	
	
	$headerpagehtml .= '<table border="0" cellpadding="0" cellspacing="0" width="506">
	<tr><td width="506" align="center">' . _("Rank Summary Report") . ': ' . $arrFactorScores[0]['leftname'] . '<br />' . _("Date Created") . ': ' .  date("d/m/Y", $timenow) . '</td></tr>';
		$headerpagehtml .= $rankOrder;
	$headerpagehtml .='</table>';
	$c = count($_SESSION['rank']);
	$pagesofRanks =  floor(($c - 18) / 22);
	$ranksOnLastPage = ($c-(18 + (22 * $pagesofRanks))); 
	if(($c >=  7 && $c < 18) || $ranksOnLastPage >= 11) {
		$pdf->writeHTMLCell(0, 0, 72.4, 40, $headerpagehtml, 0, 1, 0, true, 'L');
		$pdf->AddPage();
		$headerpagehtml = '';
	}
	$headerpagehtml .= '<table border="0" width="508" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">' . _('Relationships') . '</td><td align="center">' . _('Thinking & Planning') . '</td>
	</tr>
	<tr>
		<td align="center" colspan="2"><img src="' . ACCOUNT_PATH . '_images/_pdf/quad_blue.png" /></td>
	</tr>
	<tr>
		<td align="center">' . _('Making Decisions') . '</td><td align="center">' . _('Getting Things Done') . '</td>
	</tr>
</table><br />';
}

// output the HTML content
$pdf->writeHTMLCell(0, 0, 72.4, 40, $headerpagehtml, 0, 1, 0, true, 'L');

$pdf->SetFont($_SESSION['fontn'], '', 10);

/**********************************************************************
*																																			*
*																																			*
*											PREFERENCES AND SCORES													*
*																																			*
*																																			*
**********************************************************************/

$excel = '#0e8737';
$good = '#3f8ff5';
$average = '#e28c1b';
$poor = '#e61500';
$left_range = array(110,164,218);
$right_range = array(229,160,182);
$clus_1 = '#82bcfb';
$clus_2 = '#e7a95e';
$clus_3 = '#a9d9af'; 
$clus_4 = '#d796aa';
$top10bg = '#6fa7d1';

function getDiffBG($intDiff) {
		global $excel;
		global $good;
		global $average;
		global $poor;
		if($intDiff <= 1.01){
			return $excel;
		}
		elseif($intDiff <= 3.01){
			return $good;
		}
		elseif($intDiff <= 5.01){
			return $average;
		}
		else{
			return $poor;
		}
}

function getDevImg($stddev) {
		if($stddev <= 1.01){
			return "excellent_bullet.png";
		}
		elseif($stddev <= 2.01){
			return "good_bullet.png";
		}
		elseif($stddev <= 3.01){
			return "poor_bullet.png";
		}
		else{
			return "bad_bullet.png";
		}
}

/**********************************************************************
*																																			*
*																																			*
*											TOP 10 STATEMENTS & STAR												*
*																																			*
*																																			*
**********************************************************************/

//calc star score for each q!
function calcStar($nummaps, $stdev) {
	$const = 0.01;
	$rating = ($nummaps / ($stdev + $const));
	return $rating; 
}

$pos = 0;
foreach($_SESSION['rank'] as $rankid => $rankarr) {
	$pos++;
	$pdf->AddPage();

	$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $arrFactorScores[$rankid]['leftname'], $arrFactorScores[$rankid]['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
	
	$pdf->writeHTML($legendhtml, true, 0, true, 0);
	$yscore = $pdf->GetY();
	if($_SESSION['rightTypeDisp'] == 'single') {
		uasort($arrDeviations[$rankid]['allinfo'], cmpdiff);
	} else {
		//sort all diffs and devs hehe!
		if(($_SESSION['leftTypeDisp'] != 'talentMAP' && $_SESSION['leftTypeDisp'] != 'teamMAP') && ($_SESSION['rightTypeDisp'] != 'talentMAP' && $_SESSION['rightTypeDisp'] != 'teamMAP')) {
			$matching = 'diff';
			uasort($arrDeviations[$rankid]['allinfo'], cmpdiff);
		} else {
			$matching = 'diff';
			if($_SESSION['leftTypeDisp'] == 'talentMAP' || $_SESSION['leftTypeDisp'] == 'teamMAP') {
				uasort($arrDeviations[$rankid]['allinfo'], cmpdevleft);
				$stdevside = 'stddev_l';
			} else {
				uasort($arrDeviations[$rankid]['allinfo'], cmpdev);
				$stdevside = 'stddev';
			}
		}
	}
	
	$gcount = 0;
	$avcount = 0;
	$pcount = 0;
	$bcount = 0;
	$leftStateGCOUNT = 0;
	$rightStateGCOUNT = 0;
	$qtotal = count($arrDeviations[$rankid]['allinfo']);
	
	$leftmapids = $arrFactorScores[$rankid]['l_mapids'];
	$leftmaparr = explode(',',$leftmapids);
	$lmcount = count($leftmaparr);
	$rightmapids = $arrFactorScores[$rankid]['r_mapids'];
	$rightmaparr = explode(',',$rightmapids);
	$rmcount = count($rightmaparr);
	
	$i = 0;

	$toptenhtml = '<table border="0" cellpadding="3">
	  <tr>
	    <td bgcolor="'.$top10bg.'" color="#FFFFFF">' . _('Top 10 Matched Statements') . '</td>
	  </tr>
	</table><br /><table border="0" cellpadding="0" cellspacing="2">';
	
	foreach($arrDeviations[$rankid]['allinfo'] as $qid => $value) {
		
		//THIS WORKS OUT THE % NUMBERS ON NUM STATEMENTS MIDDLE BOX
		if($value[$matching] <= 1.01) {
			$gcount++;
		} else if ($value[$matching] <= 3.01) {
			$avcount++;
		} else if($value[$matching] <= 5.01) {
			$pcount++;
		} else {
			$bcount++;
		}

		//loop array of q's stored in as customs... so then do foreach of those then do $arrDeviations[$rankid]['allinfo']['KEYHERE_QID']['lscore'] etc to work values and display in order! (as keys will have been moved by sort);
		if($i <= 9) {
			if($value['lscore'] <= 5.0) {
				$qresponse = $stquestions[$qid]['l'];
			} else if($value['lscore'] >= 5.01) {
				$qresponse = $stquestions[$qid]['r'];
			}
			//check if scores are on opposite sides
			if(oppositeSide($value['lscore'],$value['rscore'])) {
				$starSTR = ' *';
			} else {
				$starSTR = '';
			}
			$qscore = round((10 - $value['diff']),1);
			$diffbg = getDiffBG($value['diff']);
			$stddevimg = getDevImg($value[$stdevside]);
			$toptenhtml .= '<tr><td width="30"><img src="' . ACCOUNT_PATH . '_images/_pdf/' . $stddevimg. '" width="10" height="10" /></td><td width="850">' . _($qresponse) . '</td><td width="36"><table border="0" cellpadding="0" cellspacing="0"><tr><td width="54" height="14" bgcolor="' . $diffbg. '" color="#FFFFFF" align="center">' . getClusterABRID($value['clusid']) . $starSTR . '</td></tr></table></td></tr>';
		}
		$i++;
	}
	
	$toptenhtml .= '</table>';
	$toptenhtml = $toptenhtml;
	$pdf->writeHTMLCell(0, 0, '', 128, $toptenhtml, 0, 1, 0, true, 'L');
	
	/**********************************************************************
	*																																			*
	*																																			*
	*													STARS AND DIFFERENCE												*
	*																																			*
	*																																			*
	**********************************************************************/
	
	$totalscore = round($_SESSION['rank'][$rankid]['total'],1);
	//$totalscore = 3;
	switch($totalscore) {
		case ($totalscore >= 9):
		$RKmClass = 'green';
		break;
		case ($totalscore >= 7):
		$RKmClass = 'blue';
		break;
		case ($totalscore >= 5):
		$RKmClass = 'orange';
		break;
		case ($totalscore >= 0):
		$RKmClass = 'red';
		break;
	}
	
	$quadhtml = '<table border="0" width="508" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center"><img src="' . ACCOUNT_PATH . '_images/_pdf/quad_' . $RKmClass. '.png" /></td>
		</tr>
		<tr>
			<td align="center"><font size="16">' . ordinal($pos) . '</font></td>
		</tr>
	</table>';

	$xmid = 68;
	
	$pdf->writeHTMLCell(0, 0, $xmid, $yscore, $quadhtml, 0, 1, 0, true, 'L');
	
	
	/**********************************************************************************
	*																	RANKSCORES																			*
	**********************************************************************************/
	
	$rankx = 108.2;
	$rankHTML = '<table border="0" width="220" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" height="46"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $_SESSION['rank'][$rankid][1] * 10 . '%</font></td><td align="center"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $_SESSION['rank'][$rankid][2] * 10 . '%</font></td>
	</tr>
	<tr>
		<td align="center" colspan="2" height="62"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $totalscore * 10 . '%</font></td>
	</tr>
	<tr>
		<td align="center" height="50"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $_SESSION['rank'][$rankid][3] * 10 . '%</font></td><td align="center"><font size="14" color="' .$dispPDFObj->getWhite() . '">' . $_SESSION['rank'][$rankid][4] * 10 . '%</font></td>
	</tr>
	</table>';

	$pdf->writeHTMLCell(0, 0, $rankx, $yscore+5.8, $rankHTML, 0, 1, 0, true, 'L');
	
	
	/**********************************************************************************
	*																	PERCSCORES																			*
	**********************************************************************************/

	$scorehtml = '<table border="0" width="296" cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="' . ACCOUNT_PATH . '_images/_pdf/greenscore_bg.jpg" /></td>
			<td><img src="' . ACCOUNT_PATH . '_images/_pdf/bluescore_bg.jpg" /></td>
			<td><img src="' . ACCOUNT_PATH . '_images/_pdf/orangescore_bg.jpg" /></td>
			<td><img src="' . ACCOUNT_PATH . '_images/_pdf/redscore_bg.jpg" /></td>
		</tr>
	</table>';
	
	
	$xmid = 99;
	$yscore = $pdf->GetY();
	
	$pdf->writeHTMLCell(0, 0, $xmid, $yscore+10.6, $scorehtml, 0, 1, 0, true, 'L');
	
	$excel_val = round((($gcount/$qtotal)*100),0);
	$good_val = round((($avcount/$qtotal)*100),0);
	$average_val = round((($pcount/$qtotal)*100),0);
	$poor_val = round((($bcount/$qtotal)*100),0);
		
	$scorehtmltext = '<table border="0" width="296" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center"><font color="'.$excel.'" size="20">' . $excel_val. '%</font></td>
			<td align="center"><font color="'.$good.'" size="20">' . $good_val. '%</font></td>
			<td align="center"><font color="'.$average.'" size="20">' . $average_val. '%</font></td>
			<td align="center"><font color="'.$poor.'" size="20">' . $poor_val. '%</font></td>
		</tr>
	</table>';
	
	$pdf->writeHTMLCell(0, 0, $xmid-0.6, $yscore+15.8, $scorehtmltext, 0, 1, 0, true, 'L');

}//end if not single!
		
//Close and output PDF document
$pdf->Output(ACC_NAME . '_' . $_SESSION['leftname'] . '-vs-' . $_SESSION['rightname'] . '.pdf', 'I');

//============================================================+
// END OF FILE                                                 
//============================================================+

?>
