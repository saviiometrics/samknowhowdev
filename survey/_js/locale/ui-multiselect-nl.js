/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale nl, nl-NL
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Alle toegvoegen',
	removeAll:'Alle verwijderen',
	itemsCount:'#{count} Geselecteerde onderdelen',
	itemsTotal:'#{count} Totaal aantal onderdelen',
	busy:'een ogenblik geduld...',
	errorDataFormat:"CKan opties niet toevoegen,onbekende gegevensindeling",
	errorInsertNode:"Er is een proleem opgetreden tijdens het toevoegen van het onderdeel:\n\n\t[#{key}] => #{value}\n\nDe bewerking is onderbroken.",
	errorReadonly:"De optie #{option} is alleen-lezen",
	errorRequest:"Excuses! Er is mogelijk een fout opgetreden tijdens de externe oproep. (Type: #{status})"
});

