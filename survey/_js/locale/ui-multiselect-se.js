/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:'Lägg till alla',
	removeAll:'Ta bort alla',
	itemsCount:'#{count} valda objekt',
	itemsTotal:'#{count} objekt totalt',
	busy:'vänligen vänta...',
	errorDataFormat:"Alternativen kunde inte läggas till, okänt dataformat",
	errorInsertNode:"Ett problem inträffade när systemet försökte lägga till objektet:\n\n\t[#{key}] => #{value}\n\nÅtgärden har avbrutits.",
	errorReadonly:"Alternativet #{option} är skrivskyddat",
	errorRequest:"Ett problem verkar ha uppstått vid fjärranropet. (Typ: #{status})"
});
