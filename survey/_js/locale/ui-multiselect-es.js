/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale es, es-ES
 */
 
$.extend($.ui.multiselect.locale, {
	addAll:'Agregar todos',
	removeAll:'Remover todos',
	itemsCount:'#{count} Objetos seleccionados',
	itemsTotal:'#{count} Objetos total',
	busy:'por favor, espere...',
	errorDataFormat:"No puede añadir las opciones, formato de datos desconocido",
	errorInsertNode:"Ha habido un problema al tratar de agregar el tema:\n\n\t[#{key}] => #{value}\n\nLa operación fue abortada.",
	errorReadonly:"La opción #{option} es de sólo lectura.",
	errorRequest:"Lo sentimos. Parece que hubo un problema con la llamada remota. (Tipo: #{status})"
});


/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */
/*
$.extend($.ui.multiselect.locale, {
	addAll:'Añadir todos',
	removeAll:'Eliminar todos',
	itemsCount:'#{count} elementos seleccionados',
	itemsTotal:'#{count} total de elementos',
	ocupado:'por favor, espera...',
	errorDataFormat:"No puede añadir las opciones, formato de datos desconocido",
	errorInsertNode:"Hubo un problema al intentar añadir el elemento:\n\n\t[#{key}] => #{value}\n\nLa operación fue interrumpida.",
	errorReadonly:"La opción #{option} es de sólo lectura",
	errorRequest:"Lo sentimos. Parece que hubo un problema con la llamada remota. (Tipo: #{status})"
});
*/