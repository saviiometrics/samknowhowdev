
/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:'إضافة الجميع',
	removeAll:'إزاحة الجميع',
	itemsCount:'#{count} البنود المختارة',
	itemsTotal:'#{count} إجمالي البنود',
    busy: "رجاء الانتظار...",  
	errorDataFormat:"لا يمكن إضافة خيارات، تنسيق بيانات غير معروف",
    errorInsertNode:":كان هناك خطأ عند محاولة إضافة البند \n\n\t[#{key}] => #{value}\n\n .توقف العملية تم",
	errorReadonly:"قراءة فقط #{option} هذا الخيار",
    errorRequest:"عذرا! يبدو أن هناك مشكلة ما مع المكالمة عن بعد.(أكتب: #{status})"
});
