<?php
//see if this versions
session_start();
$phpsessid = session_id();
$start_time=microtime(true);

/******************************* CHECK SESSION IS SET AND THAT ARR DEV ARRAY IS ACTUALLY AN ARRAY!!!! ******************************/

if((!isset($_SESSION['arrDeviations']) || (array)$_SESSION['arrDeviations'] !== $_SESSION['arrDeviations']) || (!isset($_SESSION['arrFactorScores']) || (array)$_SESSION['arrFactorScores'] !== $_SESSION['arrFactorScores'])) {
	printf (_("I'm sorry, there seems to be a problem generating your PDF because your sessions have timed out or been reset! Please close this window and re login to your %s account. Thank You"), 'Saviio.');
	exit;
}

//include localization and site config files
require_once("site.config.php");
//include DB AND ACCOUNT INFO CLASSES
include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';
$accobj = new Account($_SESSION['accid']);

//include other classes
include FULL_PATH . '/_inc/_classes/question-class.php';
include FULL_PATH . '/_inc/_classes/MAP-class.php';
include FULL_PATH . '/_inc/_classes/user-class.php';
include FULL_PATH . '/_inc/_classes/admin-class.php';
include FULL_PATH . '/_inc/_classes/pdf-class.php';

//$_GET['pdflocale'] = 'da_DK';
require_once(FULL_PATH . "/_inc/localization.php");
require_once(FULL_PATH . "/_inc/scripts.php");

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

$dispPDFObj = new DisplayPDF();
$fontn = $dispPDFObj->getFont($_SESSION['locale']);
	$_SESSION['fontn'] = $fontn;
	if(isset($_REQUEST['orientation']) && $_REQUEST['orientation'] == 'landscape') {
		$PDF_PAGE_ORIENTATION = 'L';
		$height = 16.8;
		$width = 55.5;
	} else {
		$PDF_PAGE_ORIENTATION = 'P';
		$height = 16.8;
		$width = 55.5;
	}

// create new PDF document
$pdf = new SaviioTCPDF($PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, 16.8, 55.5);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor(ACC_NAME);
$pdf->SetTitle(ACC_NAME . ' Report');
$pdf->SetSubject('Analytics Quickview Report');
$pdf->SetKeywords(ACC_NAME . ',analytics, summary, report');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

$userobj = new Admin($_SESSION['uid']);
$MAPobj = new MAP();
$fids = $MAPobj->getfids();
$fgs = $MAPobj->getFactorGroups();
$cids = $MAPobj->getClusters();
$stquestions = $_SESSION['stquestions'];

$access = $userobj->getLevelAccess('tmap');
if($access['EDIT'] == 'MA') {
	$compid = $userobj->headid;
} else {
	$compid = $userobj->companyid;
}

//get factor scores set from AjaXGen Script!
$arrFactorScores = $_SESSION['arrFactorScores'];
$arrDeviations = $_SESSION['arrDeviations'];

if(!isset($_SESSION['fdescs'])) {
	$fdescs = $MAPobj->getFactDescs();
	$_SESSION['fdescs'] = $fdescs;
} else {
	$fdescs = $_SESSION['fdescs'];
}
//$rankid = $_GET['rankID'];
if(!isset($_SESSION['rankID'])) {
	$rankid = 0;
} else {
	$rankid = $_SESSION['rankID'];
}
	$_SESSION['rightname'] = $arrFactorScores[$rankid]['rightname'];
	$_SESSION['leftname'] = $arrFactorScores[$rankid]['leftname'];
	$_SESSION['rightemail'] = $arrFactorScores[$rankid]['rightemail'];
	$_SESSION['leftemail'] = $arrFactorScores[$rankid]['leftemail'];
	$currentUsername = $_SESSION['cuFname'] . ' ' . $_SESSION['cuLname'];
	$_SESSION['rightissuedata'] = $arrFactorScores[$rankid]['rightissuedata'];
	$_SESSION['leftissuedata'] = $arrFactorScores[$rankid]['leftissuedata'];
	
$grey_rbg = array(138,138,138);
function getPDFFlexBarColour($score) {
	if($score >= 90){
		return array(14,135,55);
	}elseif($score >= 70){
		return array(63,143,245);
	}elseif($score >= 50){
		return array(226,140,27);
	}else{
		return array(229,21,0);
	}
}

	
// ---------------------------------------------------------

// set font
$pdf->SetFont($fontn, '', 16);
if($_SESSION['locale'] == 'ar_SA') {
		$pdf->SetFont($fontn, '', 18);
	} else {
		$pdf->SetFont($fontn, '', 16);
	}
	
	// add a page
	$pdf->AddPage();
	$timenow = time();
	$headerpagehtmlArray = $dispPDFObj->getFirstPage($PDF_PAGE_ORIENTATION, $currentUsername, $_SESSION['username'], $_SESSION['leftTypeDisp'], $_SESSION['leftname'], $_SESSION['leftemail'], $_SESSION['rightTypeDisp'], $_SESSION['rightname'], $_SESSION['rightemail'], $_SESSION['leftissuedata'], $_SESSION['rightissuedata']);
	
	// encode HTML
	// output the HTML content
	$pdf->writeHTMLCell(0, 0, '', 30, $headerpagehtmlArray['main'], 0, 1, 0, true, 'center');
	$pdf->SetFont($fontn, '', 10);
	$pdf->writeHTMLCell(0, 0, '', 130, $headerpagehtmlArray['mapdata'], 0, 1, 0, true, 'center');
$pdf->SetFont($fontn, '', 10);
/**********************************************************************
*																																			*
*																																			*
*											PREFERENCES AND SCORES													*
*																																			*
*																																			*
**********************************************************************/

	$excel = $dispPDFObj->getExcellentColour();
	$good = $dispPDFObj->getGoodColour();
	$average = $dispPDFObj->getAverageColour();
	$poor = $dispPDFObj->getPoorColour();
	$clus_1 = $dispPDFObj->getCluster1Colour();
	$clus_2 = $dispPDFObj->getCluster2Colour();
	$clus_3 = $dispPDFObj->getCluster3Colour();
	$clus_4 = $dispPDFObj->getCluster4Colour();
	$top10bg = $dispPDFObj->getTop10BackgroundColour();

function getDiffBG($intDiff) {
		global $excel;
		global $good;
		global $average;
		global $poor;
		if($intDiff <= 1.01){
			return $excel;
		}
		elseif($intDiff <= 3.01){
			return $good;
		}
		elseif($intDiff <= 5.01){
			return $average;
		}
		else{
			return $poor;
		}
}

/**********************************************************************
*																																			*
*																																			*
*											TOP 10 STATEMENTS & STAR												*
*																																			*
*																																			*
**********************************************************************/

$pdf->AddPage();
$yscore = $pdf->GetY();

/**********************************************************************
*																																			*
*																																			*
*													CLUSTERS AND SCORING												*
*																																			*
*																																			*
**********************************************************************/
if($PDF_PAGE_ORIENTATION == 'L') {
				$tableW = 940;
				$td1W =340;
				$titleW = 880;
				$forcedFont = 16;
	foreach($cids as $cid => $value) {

		if($cid == 3) {
			//now its landscape have to only fit 2 clusters on a page!
			$pdf->AddPage();
		}

		if($cid == 1 || $cid == 3) {
			if($_SESSION['rightTypeDisp'] == 'single') {
				$legendhtml = $dispPDFObj->showLegendSingle($_SESSION['leftTypeDisp'], $_SESSION['leftname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail']);
			} else {
				$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
			}
			$pdf->writeHTML($legendhtml, true, 0, true, 0);
		}

	  $clusname = $value;
		switch($cid) {
			case 1:
			$clustbg = $clus_1;
			break;
			case 2:
			$clustbg = $clus_2;
			break;
			case 3:
			$clustbg = $clus_3;
			break;
			case 4:
			$clustbg = $clus_4;
			break;
		}

		if($_SESSION['rightTypeDisp'] != 'single') {
		  $clustertotal	= round($_SESSION['clustotal_' . $cid],1);
			//which class to use based on clustertotal!
			switch($clustertotal) {
				case ($clustertotal >= 89.5):
				$clustscorebg = $excel;
				break;
				case ($clustertotal >= 69.5):
				$clustscorebg = $good;
				break;
				case ($clustertotal >= 49.5):
				$clustscorebg = $average;
				break;
				case ($clustertotal >= 0):
				$clustscorebg = $poor;
				break;
			}
		}

		//this is for extra spacing at the bottom of each cluster (well 2 and 4 as 1 and 3 are start of a new page anyway)
		if($cid == 2 || $cid == 4) {
			$extraspacing = '<br /><br /><br />';
		} else {
			$extraspacing = '';
		}

	  if($_SESSION['rightTypeDisp'] != 'single') {
			//cmpdev is deviation - cmpdiff is difference (for instance MAP vs MAP)
			if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
				uasort($arrDeviations[$rankid][$cid], cmpdiff);
			} else {
				if($_SESSION['leftTypeDisp'] == 'talentMAP' || $_SESSION['leftTypeDisp'] == 'teamMAP') {
					uasort($arrDeviations[$rankid][$cid], cmpdevleft);
					$stdevside = 'stddev_l';
				} else {
					uasort($arrDeviations[$rankid][$cid], cmpdev);
					$stdevside = 'stddev';
				}
			}
			$clusterhtml = $extraspacing . '<table border="0" cellpadding="3" cellspacing="0">
			  <tr>
			    <td color="#FFFFFF" bgcolor="' . $clustbg . '" width="' . $titleW . '"><strong>' . _($value) . '</strong></td>
			    <td width="7"></td>
			    <td width="54" align="center" color="#FFFFFF" bgcolor="' . $clustscorebg . '">' . $clustertotal . '%</td>
			  </tr>
			</table>';
		} else {
			$clusterhtml = $extraspacing . '<table border="0" cellpadding="3" cellspacing="0">
		  <tr>
		    <td color="#FFFFFF" bgcolor="' . $clustbg . '"><strong>' . _($value) . '</strong></td>
		  </tr>
			</table>';
			uasort($arrDeviations[$rankid][$cid], cmpdiff);
		}


		$pdf->writeHTML($clusterhtml, true, 0, true, 0);

		/********************************************PREF SCORES HERE *********************************/

	  $fgcount = 0;
		$clusterscore = 0;
		$k = 1;
		foreach($fgs as $fg => $clusterid) {

			$leftpositive = "";
			$leftnegative = "";
			$leftfacdesc = "";
			$rightpositive = "";
			$rightnegative = "";
			$rightfacdesc = "";
			foreach($fids as $fid => $info) {
				if($info['factor_group'] == $fg) {
					if($info['factor_align'] == 'l') {
						$left = $info['factor_text'];

						//get pos and neg for this factor group (left side)
						$thisfactors = $fdescs[$fid];
						foreach($thisfactors as $type => $factdescs) {
							foreach($factdescs as $fact) {
								if($type == 'positive') {
									$leftpositive .= '<li>' . _($fact) . '</li>';
								} else if($type == 'negative') {
									$leftnegative .= '<li>' . _($fact) . '</li>';
								} else if($type == 'desc') {
									$leftfacdesc = _($fact);
								}
							}
						}
					} else if ($info['factor_align'] == 'r') {
						$right = $info['factor_text'];

						//get pos and neg for this factor group (right side)
						$thisfactors = $fdescs[$fid];
						foreach($thisfactors as $type => $factdescs) {
							foreach($factdescs as $fact) {
								if($type == 'positive') {
									$rightpositive .= '<li>' . _($fact) . '</li>';
								} else if($type == 'negative') {
									$rightnegative .= '<li>' . _($fact) . '</li>';
								} else if($type == 'desc') {
									$rightfacdesc = _($fact);
								}
							}
						}

					}
				}
			}
			if($clusterid == $cid) {
				$fgcount++;

				//if you get a detailed page need to write the legend and clustername HTML at top
				if($_GET['pagetype'] == 'detailed') {
						if($_SESSION['rightTypeDisp'] == 'single') {
							$legendhtml = $dispPDFObj->showLegendSingle($_SESSION['leftTypeDisp'], $_SESSION['leftname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail']);
						} else {
							$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
						}
						if($clusterid == 3) {
							$ktotal = 3;
						} else {
							$ktotal = 4;
						}
						if($fgcount != $ktotal) {
							$pdf->AddPage();
							$pdf->writeHTML($legendhtml, true, 0, true, 0);
							$pdf->writeHTML($clusterdetailed, true, 0, true, 0);
						}
				}

				//this value is the distance between each point on the scale. so 4.85 width units = 1 survey point
				$WsConstant = 4.85;
				if($PDF_PAGE_ORIENTATION == 'L') {
						$xmargin = 114.2;
					} else {
						$xmargin = 75;
					}
				$intScoreMin = min($arrFactorScores[$rankid][$fg]['lscoreall']);
				$intScoreMax = max($arrFactorScores[$rankid][$fg]['lscoreall']);
				if($_SESSION['rightTypeDisp'] != 'single') {
					$intNumF = $arrFactorScores[$rankid][$fg]['num'];
					$intModF = $arrFactorScores[$rankid][$fg]['diff'];
					$intIdealMin = min($arrFactorScores[$rankid][$fg]['rscoreall']);
					$intIdealMax = max($arrFactorScores[$rankid][$fg]['rscoreall']);
					$scorediff = ($intModF/$intNumF);
					$permatch = round((10 - $scorediff),2);
					$clusterscore += $permatch;
					$intIdealWidth = round((($intIdealMax - $intIdealMin) * $WsConstant),0);

					switch($permatch) {
						case ($permatch >= 9.00):
						$matchbg = $excel;
						break;
						case ($permatch >= 7.00):
						$matchbg = $good;
						break;
						case ($permatch >= 5.00):
						$matchbg = $average;
						break;
						case ($permatch >= 0):
						$matchbg = $poor;
						break;
					}
				} else {
					$matchbg = '#6EA8D9';
					$permatch = round(($arrFactorScores[$rankid][$fg]['lscore']/$arrFactorScores[$rankid][$fg]['num']),1);
				}

				if($_SESSION['rightTypeDisp'] == 'single') {
					$dispScore = round($permatch,1);
				} else {
					$dispScore = round($permatch,1) * 10 . '%';
				}

				$intScoreWidth = round((($intScoreMax - $intScoreMin) * $WsConstant),0);
				$intScoreWidth = number_format($intScoreWidth,2,'.','');
				$intIdealWidth = number_format($intIdealWidth,2,'.','');

				//These functions get colours for the bars and for the icons
				$factscore = round((($permatch / 10) * 100),0);  //gets factor score as a percentage
				$iconclass = getScoreColour($factscore);
				if($_SESSION['rightTypeDisp'] == 'single') {
					$matchbg = '#6EA8D9';
				} elseif($factscore >= 90){
					$matchbg = $excel;
				} elseif ($factscore >= 70){
					$matchbg = $good;
				} elseif ($factscore >= 50){
					$matchbg = $average;
				} else {
					$matchbg = $poor;
				}
				//adding +3 here for additional padding between preference sets
				$y = $pdf->GetY() + 3;
				$naby = $y+4.8;

				$scoregroup = '<table border="0" cellpadding="8" cellspacing="0" width="' . $tableW . '" border="0">
					<tr>
					<td width="' . $td1W . '" valign="center" align="left"><br /><strong><font size="'. $forcedFont .'">' . _($left) . '</font></strong></td>
					<td width="196" align="center"><br /><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/surveybar_new.gif" /></td>
					<td width="' . $td1W . '" valign="center" align="right"><br /><strong><font size="'. $forcedFont .'">' . _($right) . '</font></strong></td>
					<td width="60"><table border="0" cellpadding="8"><tr><td valign="center" align="center" bgcolor="' . $matchbg. '" color="#FFFFFF" height="10" width="52">  ' . $dispScore . '</td></tr></table></td>
					</tr>
				</table>';

				$testDiv = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="54" height="14"></td></tr></table>';
				$pdf->writeHTMLCell(0, 0, 15, $y+5, $scoregroup, 0, 1, false, true, 'C', true);

				//============================================================+
				// BEHAVIOUR FLEXIBILITY BARS
				//============================================================+

	    	switch($_SESSION['leftTypeDisp']) {
			  		case 'MAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
			  			$left_bar_col = getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else if($_SESSION['rightTypeDisp'] == 'MAP') {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		}
			  		break;
			  		case 'idealMAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
			  			$left_bar_col = getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		}
			  		break;
			  		case 'talentMAP':
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		break;
			  		case 'teamMAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP') {
			  			$left_bar_col = getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		}
			  }
			  if($_SESSION['rightTypeDisp'] == 'single') {
				  $left_bar_col = $grey_rbg;
			  }
	    	//This writes a HTML cell with the border array defining colours depending on the score and whether it is the benchmark or not
if($intIdealWidth < 1) {
	$intIdealWidth = 1;
}
if($intIdealMin < 1) {
	$intIdealMin = 1;
}				

				$pdf->writeHTMLCell($intScoreWidth, 0, ($xmargin + round(($WsConstant * $intScoreMin), 0)), $y+3, $testDiv, array('LTR' => array('color' => $left_bar_col, 'width' => 0.5)), 1, false, true);
				if($_SESSION['rightTypeDisp'] != 'single') {
					$pdf->writeHTMLCell($intIdealWidth, 0, ($xmargin + round(($WsConstant * $intIdealMin), 0)), $y+11, $testDiv, array('LRB' => array('color' => $right_bar_col, 'width' => 0.5)), 1, false, true);
				} else {
					//fake one here to increase spacing
					$pdf->writeHTMLCell(20, 0, $xmargin, $y+11, $testDiv, array('LRB' => array('color' => array(255,255,255), 'width' => 0.5)), 1, false, true);
				}

				//needed for stupid locale comma shit hahah <3 french!
				$lf_total = number_format($arrFactorScores[$rankid][$fg]['lscore'],2,'.','');
			  $fac_total = number_format($arrFactorScores[$rankid][$fg]['num'],2,'.','');
			  $roundnum_l = $lf_total/$fac_total;
			  $engnum_l = number_format($roundnum_l, 2, '.', '');

				if($_SESSION['rightTypeDisp'] != 'single') {

		    	/*************************************************
			  	*																								 *
			  	*											RIGHT SIDE 								 *
			  	* 																							 *
			  	*************************************************/

			  	$rf_total = number_format($arrFactorScores[$rankid][$fg]['rscore'],2,'.','');
				  $roundnum_r = $rf_total/$fac_total;
				  $engnum_r = number_format($roundnum_r, 2, '.', '');

			  	switch($_SESSION['rightTypeDisp']) {
			  		case 'MAP':
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3),2,'.','') - 4, $naby,6.6,9, '', '', '', false, 300);
			  		break;
			  		case 'idealMAP':
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-4.4),2,'.','') - 4, $naby,8.6,8.2, '', '', '', false, 300);
			  		break;
			  		case 'teamMAP':
			  		if($_SESSION['leftTypeDisp'] == 'MAP' || $_SESSION['leftTypeDisp'] == 'idealMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  		} else {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  		}
			  		break;
			  		case 'talentMAP':
			  			if($_SESSION['leftTypeDisp'] == 'talentMAP') {
								$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  			} else {
								$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  			}
			  		break;
			  	}
				}

		  	/*************************************************
		  	*																								 *
		  	*											LEFT SIDE 								 *
		  	* 																							 *
		  	*************************************************/

		  	switch($_SESSION['leftTypeDisp']) {
		  		case 'MAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
		  			$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_' . $iconclass . '.png', $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.','') - 4, $naby,6.6,9, '', '', '', false, 300);
		  		} else if($_SESSION['rightTypeDisp'] == 'MAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.','') - 4, $naby,6.6,9, '', '', '', false, 300);
		  		} else {
		  			$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.','') - 4, $naby,6.6,9, '', '', '', false, 300);
		  		}
		  		break;
		  		case 'idealMAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_' . $iconclass . '.png', $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.','') - 4, $naby,8.6,8.2, '', '', '', false, 300);
		  		} else if($_SESSION['rightTypeDisp'] == 'idealMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.','') - 4, $naby,8.6,8.2, '', '', '', false, 300);
		  		} else {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.','') - 4, $naby,8.6,8.2, '', '', '', false, 300);
		  		}
		  		break;
		  		case 'teamMAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  		} else {
		  			if($_SESSION['rightTypeDisp'] == 'teamMAP') {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  			} else {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  			}
		  		}
		  		break;
		  		case 'talentMAP':
		  			if($_SESSION['rightTypeDisp'] == 'talentMAP') {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  			} else {
							$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  			}
		  		break;
		  	}


				//if detailed write that shit here under the bar!
				if($_GET['pagetype'] == 'detailed') {
					$detailedhtml = '<br /><br /><table border="0" cellpadding="12" cellspacing="0"><tr><td width="468"><strong>' .
					 _($left) . '</strong><br />' . _($leftfacdesc) . '<br /><br /><strong>' . _("Constructive Behaviours") . '</strong><ul>' . $leftpositive . '</ul><strong>' . _("Less Constructive Behaviours") . '</strong><ul>' . $leftnegative . '</ul>
					 </td><td width="20"></td><td width="468"><strong>' . _($right) . '</strong><br />' . _($rightfacdesc) . '<br /><br /><strong>' . _("Constructive Behaviours") . '</strong><ul>' . $rightpositive . '</ul><strong>' . _("Less Constructive Behaviours") . '</strong><ul>' . $rightnegative . '</ul></td></tr></table>';
					$pdf->writeHTML($detailedhtml, true, 0, true, 0);
				}

			}//end if clusterid;
		}
	}
} elseif($PDF_PAGE_ORIENTATION == 'P') {
				$tableW = 630;
				$td1W =187;
				$titleW = 574;
				$forcedFont = 12;

	foreach($cids as $cid => $value) {



		if($cid == 1 || $cid == 3) {
			if($_SESSION['rightTypeDisp'] == 'single') {
				//$legendhtml = $dispPDFObj->showLegendSingle($_SESSION['leftTypeDisp'], $_SESSION['leftname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail']);
			} else {
				//$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
			}
			$pdf->writeHTML($legendhtml, true, 0, true, 0);
		}

	  $clusname = $value;
		switch($cid) {
			case 1:
			$clustbg = $clus_1;
			break;
			case 2:
			$clustbg = $clus_2;
			break;
			case 3:
			$clustbg = $clus_3;
			break;
			case 4:
			$clustbg = $clus_4;
			break;
		}

		if($_SESSION['rightTypeDisp'] != 'single') {
		  $clustertotal	= round($_SESSION['clustotal_' . $cid],1);
			//which class to use based on clustertotal!
			switch($clustertotal) {
				case ($clustertotal >= 89.5):
				$clustscorebg = $excel;
				break;
				case ($clustertotal >= 69.5):
				$clustscorebg = $good;
				break;
				case ($clustertotal >= 49.5):
				$clustscorebg = $average;
				break;
				case ($clustertotal >= 0):
				$clustscorebg = $poor;
				break;
			}
		}

		//this is for extra spacing at the bottom of each cluster (well 2 and 4 as 1 and 3 are start of a new page anyway)
		if($cid > 1) {
			$extraspacing = '<br /><br /><br />';
		} else {
			$extraspacing = '';
		}

	  if($_SESSION['rightTypeDisp'] != 'single') {
			//cmpdev is deviation - cmpdiff is difference (for instance MAP vs MAP)
			if($_SESSION['leftTypeDisp'] == 'MAP' && $_SESSION['rightTypeDisp'] == 'MAP') {
				uasort($arrDeviations[$rankid][$cid], cmpdiff);
			} else {
				if($_SESSION['leftTypeDisp'] == 'talentMAP' || $_SESSION['leftTypeDisp'] == 'teamMAP') {
					uasort($arrDeviations[$rankid][$cid], cmpdevleft);
					$stdevside = 'stddev_l';
				} else {
					uasort($arrDeviations[$rankid][$cid], cmpdev);
					$stdevside = 'stddev';
				}
			}
			$clusterhtml = $extraspacing . '<table border="0" cellpadding="3" cellspacing="0">
			  <tr>
			    <td color="#FFFFFF" bgcolor="' . $clustbg . '" width="' . $titleW . '"><strong>' . _($value) . '</strong></td>
			    <td width="7"></td>
			    <td width="54" align="center" color="#FFFFFF" bgcolor="' . $clustscorebg . '">' . $clustertotal . '%</td>
			  </tr>
			</table>';
		} else {
			$clusterhtml = $extraspacing . '<table border="0" cellpadding="3" cellspacing="0">
		  <tr>
		    <td color="#FFFFFF" bgcolor="' . $clustbg . '"><strong>' . _($value) . '</strong></td>
		  </tr>
			</table>';
			uasort($arrDeviations[$rankid][$cid], cmpdiff);
		}


		$pdf->writeHTML($clusterhtml, true, 0, true, 0);

		/********************************************PREF SCORES HERE *********************************/

	  $fgcount = 0;
		$clusterscore = 0;
		$k = 1;
		foreach($fgs as $fg => $clusterid) {

			$leftpositive = "";
			$leftnegative = "";
			$leftfacdesc = "";
			$rightpositive = "";
			$rightnegative = "";
			$rightfacdesc = "";
			foreach($fids as $fid => $info) {
				if($info['factor_group'] == $fg) {
					if($info['factor_align'] == 'l') {
						$left = $info['factor_text'];

						//get pos and neg for this factor group (left side)
						$thisfactors = $fdescs[$fid];
						foreach($thisfactors as $type => $factdescs) {
							foreach($factdescs as $fact) {
								if($type == 'positive') {
									$leftpositive .= '<li>' . _($fact) . '</li>';
								} else if($type == 'negative') {
									$leftnegative .= '<li>' . _($fact) . '</li>';
								} else if($type == 'desc') {
									$leftfacdesc = _($fact);
								}
							}
						}
					} else if ($info['factor_align'] == 'r') {
						$right = $info['factor_text'];

						//get pos and neg for this factor group (right side)
						$thisfactors = $fdescs[$fid];
						foreach($thisfactors as $type => $factdescs) {
							foreach($factdescs as $fact) {
								if($type == 'positive') {
									$rightpositive .= '<li>' . _($fact) . '</li>';
								} else if($type == 'negative') {
									$rightnegative .= '<li>' . _($fact) . '</li>';
								} else if($type == 'desc') {
									$rightfacdesc = _($fact);
								}
							}
						}

					}
				}
			}
			if($clusterid == $cid) {
				$fgcount++;

				//if you get a detailed page need to write the legend and clustername HTML at top
				if($_GET['pagetype'] == 'detailed') {
						if($_SESSION['rightTypeDisp'] == 'single') {
							$legendhtml = $dispPDFObj->showLegendSingle($_SESSION['leftTypeDisp'], $_SESSION['leftname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail']);
						} else {
							$legendhtml = $dispPDFObj->showLegend($_SESSION['leftTypeDisp'], $_SESSION['rightTypeDisp'], $_SESSION['leftname'], $_SESSION['rightname'], $PDF_PAGE_ORIENTATION, $arrFactorScores[$rankid]['leftemail'], $arrFactorScores[$rankid]['rightemail']);
						}
						if($clusterid == 3) {
							$ktotal = 3;
						} else {
							$ktotal = 4;
						}
						if($fgcount != $ktotal) {
							$pdf->AddPage();
							$pdf->writeHTML($legendhtml, true, 0, true, 0);
							//$pdf->writeHTML($clusterdetailed, true, 0, true, 0);
						}
				}

				//this value is the distance between each point on the scale. so 4.85 width units = 1 survey point
				$WsConstant = 4.85;

						$xmargin = 75;

				$intScoreMin = min($arrFactorScores[$rankid][$fg]['lscoreall']);
				$intScoreMax = max($arrFactorScores[$rankid][$fg]['lscoreall']);
				if($_SESSION['rightTypeDisp'] != 'single') {
					$intNumF = $arrFactorScores[$rankid][$fg]['num'];
					$intModF = $arrFactorScores[$rankid][$fg]['diff'];
					$intIdealMin = min($arrFactorScores[$rankid][$fg]['rscoreall']);
					$intIdealMax = max($arrFactorScores[$rankid][$fg]['rscoreall']);
					$scorediff = ($intModF/$intNumF);
					$permatch = round((10 - $scorediff),2);
					$clusterscore += $permatch;
					$intIdealWidth = round((($intIdealMax - $intIdealMin) * $WsConstant),0);

					switch($permatch) {
						case ($permatch >= 9.00):
						$matchbg = $excel;
						break;
						case ($permatch >= 7.00):
						$matchbg = $good;
						break;
						case ($permatch >= 5.00):
						$matchbg = $average;
						break;
						case ($permatch >= 0):
						$matchbg = $poor;
						break;
					}
				} else {
					$matchbg = '#6EA8D9';
					$permatch = round(($arrFactorScores[$rankid][$fg]['lscore']/$arrFactorScores[$rankid][$fg]['num']),1);
				}

				if($_SESSION['rightTypeDisp'] == 'single') {
					$dispScore = round($permatch,1);
				} else {
					$dispScore = round($permatch,1) * 10 . '%';
				}

				$intScoreWidth = round((($intScoreMax - $intScoreMin) * $WsConstant),0);
				$intScoreWidth = number_format($intScoreWidth,2,'.','');
				$intIdealWidth = number_format($intIdealWidth,2,'.','');

				//These functions get colours for the bars and for the icons
				$factscore = round((($permatch / 10) * 100),0);  //gets factor score as a percentage
				$iconclass = getScoreColour($factscore);

				//adding +3 here for additional padding between preference sets
				$y = $pdf->GetY();
				$naby = $y+4.8;

				$scoregroup = '<table border="0" cellpadding="8" cellspacing="0" width="' . $tableW . '" border="0">
					<tr>
					<td width="' . $td1W . '" valign="center" align="left"><br /><strong><font size="'. $forcedFont .'">' . _($left) . '</font></strong></td>
					<td width="196" align="center"><br /><img src="' . FULL_PATH . ACCOUNT_PATH . '_images/_map/surveybar_new.gif" /></td>
					<td width="' . $td1W . '" valign="center" align="right"><br /><strong><font size="'. $forcedFont .'">' . _($right) . '</font></strong></td>
					<td width="60"><table border="0" cellpadding="8"><tr><td valign="center" align="center" bgcolor="' . $matchbg. '" color="#FFFFFF" width="52">  ' . $dispScore . '</td></tr></table></td>
					</tr>
				</table>';

				//$testDiv = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="54" height="14"></td></tr></table>';
				$pdf->writeHTMLCell(0, 0, 15, $y+6, $scoregroup, 0, 1, false, true, 'C', true);

				//============================================================+
				// BEHAVIOUR FLEXIBILITY BARS
				//============================================================+

	    	switch($_SESSION['leftTypeDisp']) {
			  		case 'MAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
			  			$left_bar_col = getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else if($_SESSION['rightTypeDisp'] == 'MAP') {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		}
			  		break;
			  		case 'idealMAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
			  			$left_bar_col = getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		}
			  		break;
			  		case 'talentMAP':
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		break;
			  		case 'teamMAP':
			  		if($_SESSION['rightTypeDisp'] == 'talentMAP') {
			  			$left_bar_col = getPDFFlexBarColour($factscore);
			  			$right_bar_col = $grey_rbg;
			  		} else {
			  			$left_bar_col = $grey_rbg;
			  			$right_bar_col = getPDFFlexBarColour($factscore);
			  		}
			  }
			  if($_SESSION['rightTypeDisp'] == 'single') {
				  $left_bar_col = $grey_rbg;
			  }
	    	//This writes a HTML cell with the border array defining colours depending on the score and whether it is the benchmark or not
	    	if($intScoreWidth < 1) {
	    		$intScoreWidth =1;
	    	}
				$pdf->writeHTMLCell($intScoreWidth, 0, ($xmargin + round(($WsConstant * $intScoreMin), 0)) - 4, $y+1, $testDiv, array('LTR' => array('color' => $left_bar_col, 'width' => 0.5)), 1, false, true);
				if($_SESSION['rightTypeDisp'] != 'single') {
					$pdf->writeHTMLCell($intIdealWidth, 0, ($xmargin + round(($WsConstant * $intIdealMin), 0)) - 4, $y+9, $testDiv, array('LRB' => array('color' => $right_bar_col, 'width' => 0.5)), 1, false, true);
				} else {
					//fake one here to increase spacing
					$pdf->writeHTMLCell(20, 0, $xmargin, $y+9, $testDiv, array('LRB' => array('color' => array(255,255,255), 'width' => 0.5)), 1, false, true);
				}

				//needed for stupid locale comma shit hahah <3 french!
				$lf_total = number_format($arrFactorScores[$rankid][$fg]['lscore'],2,'.','');
			  $fac_total = number_format($arrFactorScores[$rankid][$fg]['num'],2,'.','');
			  $roundnum_l = $lf_total/$fac_total;
			  $engnum_l = number_format($roundnum_l, 2, '.', '');

				if($_SESSION['rightTypeDisp'] != 'single') {

		    	/*************************************************
			  	*																								 *
			  	*											RIGHT SIDE 								 *
			  	* 																							 *
			  	*************************************************/

			  	$rf_total = number_format($arrFactorScores[$rankid][$fg]['rscore'],2,'.','');
				  $roundnum_r = $rf_total/$fac_total;
				  $engnum_r = number_format($roundnum_r, 2, '.', '');

			  	switch($_SESSION['rightTypeDisp']) {
			  		case 'MAP':
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3),2,'.','') - 4, $naby,6.6,9, '', '', '', false, 300);
			  		break;
			  		case 'idealMAP':
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-4.4),2,'.','') - 4, $naby,8.6,8.2, '', '', '', false, 300);
			  		break;
			  		case 'teamMAP':
			  		if($_SESSION['leftTypeDisp'] == 'MAP' || $_SESSION['leftTypeDisp'] == 'idealMAP') {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  		} else {
						$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  		}
			  		break;
			  		case 'talentMAP':
			  			if($_SESSION['leftTypeDisp'] == 'talentMAP') {
								$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  			} else {
								$pdf->Image(FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png', $xmargin + number_format((($engnum_r * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
			  			}
			  		break;
			  	}
				}

		  	/*************************************************
		  	*																								 *
		  	*											LEFT SIDE 								 *
		  	* 																							 *
		  	*************************************************/

		  	switch($_SESSION['leftTypeDisp']) {
		  		case 'MAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
		  			$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_' . $iconclass . '.png';
		  		} else if($_SESSION['rightTypeDisp'] == 'MAP') {
					$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey_alt.png';
		  		} else {
		  			$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/single_grey.png';
		  		}
		  		$pdf->Image($image, $xmargin + number_format((($engnum_l * $WsConstant)-3),2,'.','') - 4, $naby,6.6,9, '', '', '', false, 300);
		  		break;
		  		case 'idealMAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP' || $_SESSION['rightTypeDisp'] == 'teamMAP') {
						$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_' . $iconclass . '.png';
		  		} else if($_SESSION['rightTypeDisp'] == 'idealMAP') {
						$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey_alt.png';
		  		} else {
						$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/ideal_grey.png';
		  		}
		  		$pdf->Image($image, $xmargin + number_format((($engnum_l * $WsConstant)-4.4),2,'.','') - 4, $naby,8.6,8.2, '', '', '', false, 300);
		  		break;
		  		case 'teamMAP':
		  		if($_SESSION['rightTypeDisp'] == 'talentMAP') {
						$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_' . $iconclass . '.png';
		  		} else {
		  			if($_SESSION['rightTypeDisp'] == 'teamMAP') {
							$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png';
		  			} else {
							$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png';
		  			}
		  		}
		  		$pdf->Image($image, $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  		break;
		  		case 'talentMAP':
		  			if($_SESSION['rightTypeDisp'] == 'talentMAP') {
							$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey_alt.png';
		  			} else {
							$image = FULL_PATH . ACCOUNT_PATH . '_images/_pdf/cross_grey.png';
		  			}
		  			$pdf->Image($image, $xmargin + number_format((($engnum_l * $WsConstant)-3.2),2,'.','') - 4, $naby,8.2,8.2, '', '', '', false, 300);
		  		break;
		  	}


			}//end if clusterid;
		}
	}
}

//for each Cluster!
//Close and output PDF document
if($_SESSION['rightTypeDisp'] == 'single') {
	$pdf->Output(ACC_NAME . '_' . $_SESSION['leftname'] . '.pdf', 'I');
} else {
	$pdf->Output(ACC_NAME . '_' . $_SESSION['leftname'] . '-vs-' . $_SESSION['rightname'] . '.pdf', 'I');	
}



//============================================================+
// END OF FILE                                                 
//============================================================+
?>
