<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 **/
 
session_start();
$phpsessid = session_id();

//include localization and site config files
require_once("../site.config.php");

//include DB AND ACCOUNT INFO CLASSES
include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';
if(!isset($_SESSION['accid'])) {
	$accid = 1;
} else {
	$accid = $_SESSION['accid'];
}
$accobj = new Account($accid);
return array(
    'static_css' => array(
    	'//' . ACCOUNT_PATH . '_css/ui.multiselect.css',
    	'//' . ACCOUNT_PATH . '_css/stylish-select.css',
    	'//' . ACCOUNT_PATH . '_css/redmond/redmond.css',
    	'//admin/' . ACCOUNT_PATH . '_css/validationEngine.jquery.css',
    	'//' . ACCOUNT_PATH . '_css/main.css',
    	'//' . ACCOUNT_PATH . '_css/jquery.jgrowl.css',
    	'//' . ACCOUNT_PATH . '_css/jquery.jscrollpane.css'
    ),
    'admin_css' => array(
   		'//admin/' . ACCOUNT_PATH . '_css/idealmap/idealmap.css',
	    '//admin/' . ACCOUNT_PATH . '_css/validationEngine.jquery.css',
	    '//admin/' . ACCOUNT_PATH . '_css/adminui/adminui.css',
	    '//admin/' . ACCOUNT_PATH . '_css/admin.css',
    	'//' . ACCOUNT_PATH . '_css/ui.multiselect.css',
    	'//' . ACCOUNT_PATH . '_css/jquery.jgrowl.css',
    	'//admin/' . ACCOUNT_PATH . '_css/blue/style.css',
    	'//admin/' . ACCOUNT_PATH . '_css/data_table.css'
    )
);