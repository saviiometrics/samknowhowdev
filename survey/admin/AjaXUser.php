 <?php

	session_start();
	$phpsessid = session_id();
	//ini_set("display_errors", 1);
	//include localization and site config files
	require_once("../site.config.php");

	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';

	if(isset($_GET['relogNow'])) {
		$accobj = new Account($_GET['accid']);
		$_SESSION['accid'] = $_GET['accid'];
	} else {
		$accobj = new Account($_SESSION['accid']);
	}

	//include other classes
	include FULL_PATH . '/_inc/_classes/MAP-class.php';
	include FULL_PATH . '/_inc/_classes/question-class.php';
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	
	//stop this running when sessions aren't set. Mainly to do with ajax login breaking
	if(isset($_SESSION['uid'])) {
		$adminobj = new Admin($_SESSION['uid']);
		$limitsArr = $adminobj->getLimits();
	}
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	if(isset($_GET['relogNow'])) {
		$userobj = new User();
		if(!$userobj->checkLogin(addslashes($_GET['uname']), md5(trim($_GET['repass'])))) {
			$errorstr = _("I'm sorry your username and password were incorrect");
			echo 'false';
		} else {
			session_regenerate_id(true);
			$_SESSION['username'] = addslashes($_GET['uname']);
			$_SESSION['passw'] = md5($_GET['repass']);
			$_SESSION['dashpass'] = $_POST['repass'];
			$userobj->getUservUN($_GET['uname']);
			$_SESSION['uid'] = $userobj->uid;
			$_SESSION['level'] = $userobj->level;
			$_SESSION['accid'] = $userobj->accid;
			echo 'true';
		}
		exit();
	}
	
	if(isset($_GET['refreshSession'])) {
		//BOOM!!
	}
	
	function UnsharpMask($img, $amount, $radius, $threshold)    { 
	
			////////////////////////////////////////////////////////////////////////////////////////////////  
			////  
			////                  Unsharp Mask for PHP - version 2.1.1  
			////  
			////    Unsharp mask algorithm by Torstein H�nsi 2003-07.  
			////             thoensi_at_netcom_dot_no.  
			////               Please leave this notice.  
			////  
			///////////////////////////////////////////////////////////////////////////////////////////////  
	
	
	    // $img is an image that is already created within php using 
	    // imgcreatetruecolor. No url! $img must be a truecolor image. 
	
	    // Attempt to calibrate the parameters to Photoshop: 
	    if ($amount > 500)    $amount = 500; 
	    $amount = $amount * 0.016; 
	    if ($radius > 50)    $radius = 50; 
	    $radius = $radius * 2; 
	    if ($threshold > 255)    $threshold = 255; 
	     
	    $radius = abs(round($radius));     // Only integers make sense. 
	    if ($radius == 0) { 
	        return $img; imagedestroy($img); break;        } 
	    $w = imagesx($img); $h = imagesy($img); 
	    $imgCanvas = imagecreatetruecolor($w, $h); 
	    $imgBlur = imagecreatetruecolor($w, $h); 
	     
	
	    // Gaussian blur matrix: 
	    //                         
	    //    1    2    1         
	    //    2    4    2         
	    //    1    2    1         
	    //                         
	    ////////////////////////////////////////////////// 
	         
	
	    if (function_exists('imageconvolution')) { // PHP >= 5.1  
	            $matrix = array(  
	            array( 1, 2, 1 ),  
	            array( 2, 4, 2 ),  
	            array( 1, 2, 1 )  
	        );  
	        imagecopy ($imgBlur, $img, 0, 0, 0, 0, $w, $h); 
	        imageconvolution($imgBlur, $matrix, 16, 0);  
	    }  
	    else {  
	
	    // Move copies of the image around one pixel at the time and merge them with weight 
	    // according to the matrix. The same matrix is simply repeated for higher radii. 
	        for ($i = 0; $i < $radius; $i++)    { 
	            imagecopy ($imgBlur, $img, 0, 0, 1, 0, $w - 1, $h); // left 
	            imagecopymerge ($imgBlur, $img, 1, 0, 0, 0, $w, $h, 50); // right 
	            imagecopymerge ($imgBlur, $img, 0, 0, 0, 0, $w, $h, 50); // center 
	            imagecopy ($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h); 
	
	            imagecopymerge ($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333 ); // up 
	            imagecopymerge ($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25); // down 
	        } 
	    } 
	
	    if($threshold>0){ 
	        // Calculate the difference between the blurred pixels and the original 
	        // and set the pixels 
	        for ($x = 0; $x < $w-1; $x++)    { // each row
	            for ($y = 0; $y < $h; $y++)    { // each pixel 
	                     
	                $rgbOrig = ImageColorAt($img, $x, $y); 
	                $rOrig = (($rgbOrig >> 16) & 0xFF); 
	                $gOrig = (($rgbOrig >> 8) & 0xFF); 
	                $bOrig = ($rgbOrig & 0xFF); 
	                 
	                $rgbBlur = ImageColorAt($imgBlur, $x, $y); 
	                 
	                $rBlur = (($rgbBlur >> 16) & 0xFF); 
	                $gBlur = (($rgbBlur >> 8) & 0xFF); 
	                $bBlur = ($rgbBlur & 0xFF); 
	                 
	                // When the masked pixels differ less from the original 
	                // than the threshold specifies, they are set to their original value. 
	                $rNew = (abs($rOrig - $rBlur) >= $threshold)  
	                    ? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig))  
	                    : $rOrig; 
	                $gNew = (abs($gOrig - $gBlur) >= $threshold)  
	                    ? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig))  
	                    : $gOrig; 
	                $bNew = (abs($bOrig - $bBlur) >= $threshold)  
	                    ? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig))  
	                    : $bOrig; 
	                 
	                 
	                             
	                if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) { 
	                        $pixCol = ImageColorAllocate($img, $rNew, $gNew, $bNew); 
	                        ImageSetPixel($img, $x, $y, $pixCol); 
	                    } 
	            } 
	        } 
	    } 
	    else{ 
	        for ($x = 0; $x < $w; $x++)    { // each row 
	            for ($y = 0; $y < $h; $y++)    { // each pixel 
	                $rgbOrig = ImageColorAt($img, $x, $y); 
	                $rOrig = (($rgbOrig >> 16) & 0xFF); 
	                $gOrig = (($rgbOrig >> 8) & 0xFF); 
	                $bOrig = ($rgbOrig & 0xFF); 
	                 
	                $rgbBlur = ImageColorAt($imgBlur, $x, $y); 
	                 
	                $rBlur = (($rgbBlur >> 16) & 0xFF); 
	                $gBlur = (($rgbBlur >> 8) & 0xFF); 
	                $bBlur = ($rgbBlur & 0xFF); 
	                 
	                $rNew = ($amount * ($rOrig - $rBlur)) + $rOrig; 
	                    if($rNew>255){$rNew=255;} 
	                    elseif($rNew<0){$rNew=0;} 
	                $gNew = ($amount * ($gOrig - $gBlur)) + $gOrig; 
	                    if($gNew>255){$gNew=255;} 
	                    elseif($gNew<0){$gNew=0;} 
	                $bNew = ($amount * ($bOrig - $bBlur)) + $bOrig; 
	                    if($bNew>255){$bNew=255;} 
	                    elseif($bNew<0){$bNew=0;} 
	                $rgbNew = ($rNew << 16) + ($gNew <<8) + $bNew; 
	                    ImageSetPixel($img, $x, $y, $rgbNew); 
	            } 
	        } 
	    } 
	    imagedestroy($imgCanvas); 
	    imagedestroy($imgBlur); 
	     
	    return $img; 
	
	}
	
	function processThumb($file, $newpath, $filename, $w, $h, $bw=0) {
			$imgSrc = $file; 
			//getting the image dimensions
			list($width, $height) = getimagesize($imgSrc);
			
			if($width > $w) {
				if($h == 0) {
					$rm = $width/$height;
					$h = ($w / $rm);
				}
			} else {
				$w = $width;
				$h = $height;
			}
	
			//saving the image into memory (for manipulation with GD Library)
			$myImage = imagecreatefromjpeg($imgSrc); 
			$_ratio=array($width/$height,$w/$h); 
	    if ($_ratio[0] != $_ratio[1]) { // crop image
	      // find the right scale to use
	      $_scale=min((float)($width/$w),(float)($height/$h));
	
	      // coords to crop
	      $cropX=(float)($width-($_scale*$w));
	      $cropY=(float)($height-($_scale*$h));   
	     
	      // cropped image size
	      $cropW=(float)($width-$cropX);
	      $cropH=(float)($height-$cropY);
	     
	      $crop=ImageCreateTrueColor($cropW,$cropH);
	      // crop the middle part of the image to fit proportions
	      ImageCopy($crop,$myImage,0,0,(int)($cropX/2),(int)($cropY/2),$cropW,$cropH);
	    }
	   
	    // do the thumbnail
	    $NewThumb=ImageCreateTrueColor($w,$h);
	    if (isset($crop)) { // been cropped
	        ImageCopyResampled($NewThumb,$crop,0,0,0,0,$w,$h,$cropW,$cropH);
	        ImageDestroy($crop);
	    } else { // ratio match, regular resize
	        ImageCopyResampled($NewThumb,$myImage,0,0,0,0,$w,$h,$width,$height);
	    }
	
			if($bw == 1) {
				imagefilter($NewThumb, IMG_FILTER_GRAYSCALE);
			}
		
			//sharpen image
			UnsharpMask($NewThumb, 30, 0.2, 2);
			imagejpeg($NewThumb,  $_SERVER['DOCUMENT_ROOT'] . '/_tmp/' . $filename, 100);
			
			// include the API
			require($_SERVER['DOCUMENT_ROOT'] . '/_inc/_cloudfiles/cloudfiles.php');
	
			// cloud info
			$username = "saviio"; // username
			$key = "5049efd30426567d6cc5c25fb20354b3"; // api key
			 
			// Connect to Rackspace
			$auth = new CF_Authentication($username, $key);
			$auth->authenticate();
			$conn = new CF_Connection($auth);
			 
			// Get the container we want to use
			$container = $conn->get_container('_saviioAvatars');

			// store file information

			// upload file to Rackspace
			$object = $container->create_object($filename);
			$object->load_from_filename($_SERVER['DOCUMENT_ROOT'] . '/_tmp/' . $filename);

			//imagejpeg($thumb);
			imagedestroy($NewThumb); 	
			
	}
	
	//UPLOAD AVATAR SCRIPT
	if(isset($_POST['uploadAvatar'])) {
		
		$cdn_path = 'http://c538929.r29.cf2.rackcdn.com';
		$localfile = $_FILES['upload']['tmp_name'];
		$curr_fn = $_FILES['upload']['name'];
		//resize and crop it baby!
		$usxObj = new User();
		$randname = $usxObj->randPass(18);
		$fileExt = pathinfo($curr_fn, PATHINFO_EXTENSION);
		$filename = $_REQUEST['userid'] . '_' . $randname . '.' . $fileExt;
			
		$w = 50;
		$h = 0;
		processThumb($localfile, $newpath, $filename, $w, $h, $bw=0);

		$avaType = 'adminUpload';
		$ava_url = $cdn_path . '/' . $filename;
		if(!$adminobj->updateAvatar($avaType, $ava_url, $_REQUEST['userid'])) {
			$message = _('Avatar update was not successful, please try again');
		} else {
			$message = _('Avatar successfully updated.');
		}
		echo $ava_url . '---' . $message;
	}

	if(isset($_GET['addUser'])) {
		try {
			if($adminobj->addUser($_GET['username'], $_GET['firstname'], $_GET['surname'], $_GET['companyid'], $_GET['username'], $_GET['countryid'], $_GET['eid'], $_GET['level'], $_GET['ulang'])) {
				printf(_("User %s %s was added successfully"), $_GET['firstname'], $_GET['surname']);
				$adminobj->runTracker('Added User', $_GET['firstname'] . ' ' . $_GET['surname'], $_SESSION['adduid'], 'USER');
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	if(isset($_GET['getUser'])) {
		$userobj = new User();
		$userobj->getUser($_GET['userid']);
		echo $userobj->username . '---' . $userobj->firstname . '---' . $userobj->surname . '---' . $userobj->email . '---' . $userobj->countryid . '---' . $userobj->companyid . '---' . $userobj->level . '---' . $userobj->avatarURL;
	}
	
	if(isset($_GET['editUser'])) {
		$feataccess = $adminobj->getLevelAccess('siteaccess',$_GET['level']);
		$superAccess = $adminobj->getLevelAccess('admin');
		//admin users are allowed one free MAP regardless of the MAP Storage limit to prevent issues with adding new admin users when MAP limit has been reached. However adding more MAPs to an admin account is tagged as a new MAP and adds towards the MAP storage total
		$allowUpdate = false;
		if($feataccess['ANALYTICS'] == 'TRUE' || $feataccess['ADMIN'] == 'TRUE') {
			$modAdmin = true;
		} else {
			$modAdmin = false;
			$allowUpdate = true;
		}
		if($modAdmin == true) {
			if($limitsArr['users'] == 'NOLIMIT') {
				$allowUpdate = true;
			} else {
				$limit = ($limitsArr['users'] - TOTAL_USERS);
				if($limit >= 1) {
					$allowUpdate = true;
				} else {
					$allowUpdate = false;
				}
			}
		}
		
		//only allow if update is true or you are a superAdmin to override.
		if($allowUpdate == true || $superAccess['GODADMIN'] == 'TRUE') {
			if(!$adminobj->updateUser($_GET['userid'], $_GET['firstname'], $_GET['surname'], $_GET['companyid'], $_GET['email'], $_GET['countryid'], $_GET['level'])) {
				printf(_("An error has occured updating the user %s %s"), $_GET['firstname'], $_GET['surname']);
			} else {
				$adminobj->runTracker('Updated User', $_GET['firstname'] . ' ' . $_GET['surname'], $_GET['userid'], 'USER');
				printf(_("User %s %s was updated successfully"), $_GET['firstname'], $_GET['surname']);
			}
		} else {
			printf(_("User %s %s was not updated. You have reached your limit for the maximum number of administrators."), $_GET['firstname'], $_GET['surname']);
		}
	}

  //***********************************************************************
  //															RETAKE CODE															//
  //***********************************************************************
	    
	if(isset($_GET['retakeSurvey'])) {
		$userdetails = $adminobj->getUserVID($_GET['userid']);
		if($limitsArr['maps'] == 'NOLIMIT') {
			$limit = 'NOLIMIT';
		} else {
			$limit = ($limitsArr['maps'] - TOTAL_MAPS);
		}
		
		if($limit >= 1 || $superAccess['GODADMIN'] == 'TRUE' || $limit == 'NOLIMIT') {
			if(!$adminobj->increaseRetake($_GET['userid'])) {
				printf(_("An error has occured updating %s %s's retake count"), $userdetails->firstname, $userdetails->surname);
			} else {
				//no need to increment total MAPs here as when the user visits the MAP page again it will automatically add one for them and increment it then. 
				//$adminobj->incrementTotals('map');
				$adminobj->runTracker('Increase Retake', $userdetails->firstname . ' ' . $userdetails->surname, $_GET['userid'], 'USER');

			  /****************************************************************************
			  //															SEND OUT MAIL																*
			  //**************************************************************************/
				
				$mail = new PHPMailer();
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = MAIL_HOSTNAME; // SMTP server
				//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = MAIL_USERNAME; // SMTP account username
				$mail->Password   = MAIL_PASSWORD;        // SMTP account password
				
				$body .= sprintf(_("Hi %s,"), $userdetails->firstname) . '<br /><br />';
				$body .= sprintf(_("You have been invited to retake your survey on %s! If you have forgotten your details then don't worry, just type your email into the forgotten password box and a new password will be mailed out to you!"), ACC_NAME) . '<br /><br />';
				$body .= 'Survey Site: <a href="' . $adminobj->surveyURL . '">' . $adminobj->surveyURL . '</a><br /><br />';
				$body .= _('Thank You') . '<br /><br />';
				$body .= 'The Saviio Team<br />';
				
				//now get the template file!
				$email_template = file_get_contents(CONTENT_PATH . '/email_template.php');
				$tags_original   = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
				$tags_replaced = array(_('You have been invited to retake the Saviio Survey!'), $body);
	
				$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);

				$mail->From = NR_EMAIL;
				$mail->FromName = ACC_NAME;
				$mail->CharSet = "UTF-8";
				$mail->Subject = _("Retake Survey Request");
				$mail->AltBody = _("To view the message, please use an HTML compatible email viewer."); // optional, comment out and test
				$mail->MsgHTML($emailHTML);
				$mail->AddAddress($userdetails->email, $userdetails->firstname . ' ' . $userdetails->surname);

				if(!$mail->Send()) {
				 	printf(_("User %s %s's retake count has been increased successfully. However the e-mail informing them of this was not sent!"), $userdetails->firstname, $userdetails->surname);
				} else {
				  printf(_("User %s %s's retake count has been increased successfully"), $userdetails->firstname, $userdetails->surname);
				}
		
			}//end check increase OK
		} else {
			echo _("I'm sorry you have reached your limit for MAP Storage");
		}
	}
	
	//***********************************************************************
  //															RESEND CODE															//
  //***********************************************************************
	    
	if(isset($_GET['reSendEmail'])) {
		$userdetails = $adminobj->getUserVID($_GET['userid']);
		$randpass = $adminobj->randPass(14);
		$hashstr = md5($userdetails->firstname . $userdetails->surname . $randpass);
		$password = md5($randpass);
		
		//builds email from db!
		$emailobj = $adminobj->getEmail($_GET['eid']);
	
		$tags_original   = array("[USERNAME]", "[PASSWORD]", "[ADMIN_EMAIL]", "[SITEURL]", "[FIRSTNAME]", "[LASTNAME]", "[ADMIN_FN]", "[ADMIN_SN]", "[SURVEYNAME]");
		$tags_replaced = array($username, $password, $adminobj->email, $adminobj->surveyURL . 'saviio/auth/' . $hashstr, $userdetails->firstname, $userdetails->surname, $adminobj->firstname, $adminobj->surname, ACC_NAME);

		$emailHTML = str_replace($tags_original, $tags_replaced, $emailobj->email_content);
		
		//now get the template file!
		$email_template = file_get_contents(CONTENT_PATH . '/email_template.php');
		$tags_original   = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
		$tags_replaced = array(_('Welcome to Saviio!'), $emailHTML);

		$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);
		
		//send mail to admin... with details for now! :)
		$mail = new PHPMailer();

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = MAIL_HOSTNAME; // SMTP server
		//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Username   = MAIL_USERNAME; // SMTP account username
		$mail->Password   = MAIL_PASSWORD;        // SMTP account password
				
		$mail->From  = NR_EMAIL;
		$mail->FromName = ACC_NAME;
		$mail->CharSet = "UTF-8";
		//$mail->Subject = _("Login Details");
		$mail->Subject = "Login Details";
		$mail->AltBody = _("To view the message, please use an HTML compatible email viewer"); // optional, comment out and test
		$mail->MsgHTML($emailHTML);
		//$mail->AddAddress($adminobj->email, $adminobj->firstname . ' ' . $adminobj->surname);
		$mail->AddAddress($userdetails->email, $userdetails->firstname . ' ' . $userdetails->surname);

		if(!$mail->Send()) {
		  //RETURN FALSE;
		  printf(_("%s %s's invitation was not sent successfully"), $userdetails->firstname, $userdetails->surname);
		} else {
			$adminobj->runTracker('Resend Email Invitation', $userdetails->firstname . ' ' . $userdetails->surname, $_GET['userid'], 'USER');
			printf(_("%s %s's invitation has been sent successfully"), $userdetails->firstname, $userdetails->surname);
			$adminobj->updateDetails($_GET['userid'], $userdetails->firstname, $userdetails->surname, $userdetails->email, $randpass, true, $hashstr);
		 // RETURN TRUE;
		}
	}
	
	if(isset($_GET['updateDetails'])) {
		if(!$adminobj->updateDetails($_GET['uid'], $_GET['firstname'], $_GET['surname'], $_GET['email'], $_GET['password'])) {
			echo _('There was an error updating your details');
		} else {
			$adminobj->runTracker('Updated their details', $_GET['firstname'] . ' ' . $_GET['surname'], $_GET['uid'], 'USER');
			echo _('Your details were updated successfully');
		}
	}
	
	if(isset($_GET['resetPass'])) {
		$newpass = $adminobj->randPass(8);
		$password = md5($newpass);
		$mail = new PHPMailer();

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = MAIL_HOSTNAME; // SMTP server
		//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Username   = MAIL_USERNAME; // SMTP account username
		$mail->Password   = MAIL_PASSWORD;        // SMTP account password
		
		$body .= sprintf(_("Hi %s,"), $_GET['firstname']) . '<br /><br />';
		$body .= _("Thank you for your request to reset your password. If you haven't requested this password reset please contact an administrator immediately") . '<br /><br />';
		$body .= '<strong>' . _('Your password has now been reset and can be seen below') . '</strong><br /><br />';
		
		$body .= _('Username') . ': ' . $_GET['username'] . '<br />'  . "\n";
		$body .= _('Password') . ': ' . $newpass . '<br /><br />'  . "\n";
        $body .= _('Link') . ': ' . $adminobj->surveyURL . '<br /><br />'  . "\n";
		
		$body .= _('Thank You') . '<br /><br />';
		$body .= 'The Saviio Team<br />';
		
		//now get the template file!
		$email_template = file_get_contents(CONTENT_PATH . '/email_template.php');
		$tags_original   = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
		$tags_replaced = array(_('Your new Saviio password'), $body);

		$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);
		
		$mail->From = NR_EMAIL;
		$mail->FromName = ACC_NAME;
		$mail->CharSet = "UTF-8";
		$mail->Subject = _("Reset Password Request");
		$mail->AltBody = _("To view the message, please use an HTML compatible email viewer."); // optional, comment out and test
		$mail->MsgHTML($body);
		$mail->AddAddress($_GET['email'], $_GET['firstname'] . ' ' . $_GET['surname']);
		if(!$adminobj->resetPassword($_GET['userid'], $password)) {
			echo _('An error has occurred resetting the password');
		} else {
			$adminobj->runTracker('Reset User Password', $_GET['firstname'] . ' ' . $_GET['surname'], $_GET['userid'], 'USER');
			if(!$mail->Send()) {
			  RETURN FALSE;
			} else {
			  printf(_("%s %s's password was successfully reset"), $_GET['firstname'], $_GET['surname']);
			}
		}
	}
	
	if(isset($_FILES['usercsv']['name'])) {
		$i = 0;
		$errorstr = '';
		$success_str = '';
		require_once 'excel_reader2.php';
		$reader = new Spreadsheet_Excel_Reader($_FILES['usercsv']['tmp_name']);
		foreach($reader->sheets as $k => $data) {
	 		$i=0;
	    foreach($data['cells'] as $row) {
			  if($i == 0) {
			  	$i = 1;
					if($row[1] == 'Email' && $row[2] == 'Firstname') {
		  			continue;
		  		} else {
		  			echo _('The CSV file is formatted incorrectly. Make sure the structure is exactly the same as the template file!');
		  			break;
		  		}
				}
	    
	      $fn = utf8_encode($row[2]);
	      $sn = utf8_encode($row[3]);
	      
				if(preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $row[1])) {
					try {
						if($adminobj->addUser($row[1], $fn, $sn, $_POST['UUCompanyid'], $row[1], $_POST['UUCountryid'], $_POST['eid'], $_POST['UULevel'], $_POST['ulang'])) {
							$success_str .= sprintf(_("User %s %s was added successfully"), $fn, $sn) . '<br />' . "\n";
							$adminobj->runTracker('Added User', $_GET['firstname'] . ' ' . $_GET['surname'], $_SESSION['adduid'], 'USER');
						}
					} catch (Exception $e) {
						$errorstr .= sprintf(_("User %s %s failed to add"), $fn, $sn) . ' - <span style="color:red;">' . $e->getMessage() . '</span><br />';
					}
				} else {
					$errorstr .= sprintf(_("User %s %s failed to add"), $fn, $sn) . ' - <span style="color:red;">' . _("Invalid Email Address") . '</span><br />';
				}
	    }
	 	}
	  if($errorstr == '') {
	  	echo _('All users added successfully, an email has been sent to you confirming the users added') . "\n";
	  } else {
	 	  echo _('Some or all users were not added - please check your email for a detailed overview.');
	 	}
	  //send mail to admin... with details for now! :)
		$mail = new PHPMailer();
		$emailHTML = _('Here is your CSV User Upload report') . '<br /><br />';
		$emailHTML .= '<strong>' . _("Users that added successfully:") . '</strong><br />';
		$emailHTML .= $success_str . '<br />';
	  $emailHTML .= '<strong>' . _('Users that were not added:') . '</strong><br />';
	  if($errorstr == '') {
	  	$emailHTML .= _('No error occurred!');
	  } else {
	  	$emailHTML .= $errorstr;
	  }
		$mail->From  = NR_EMAIL;
		$mail->FromName = ACC_NAME;
		$mail->CharSet = "UTF-8";
		$mail->Subject = _("CSV User Upload Report");
		$mail->AltBody = _("To view the message, please use an HTML compatible email viewer"); // optional, comment out and test
		$mail->MsgHTML($emailHTML);
		$mail->AddAddress($adminobj->email, $adminobj->firstname . ' ' . $adminobj->surname);
	
		if(!$mail->Send()) {
		  RETURN FALSE;
		} else {
		  RETURN TRUE;
		}

	}
	
	/****************************************************************************
	*																																						*
	*																		EMAIL																		*
	*																																						*
	****************************************************************************/
	
	if(isset($_GET['getEMail'])) {
		$emailobj = $adminobj->getEmail($_GET['eid']);
		echo $emailobj->name . '---' . $emailobj->email_content . '---' . $emailobj->lang;
	}
	
	if(isset($_POST['addEmail'])) {
		if($limitsArr['emails'] == 'NOLIMIT') {
			$limit = 'NOLIMIT';
		} else {
			$limit = ($limitsArr['emails'] - TOTAL_EMAILS);
		}
		if($limit >= 1 || $superAccess['GODADMIN'] == 'TRUE' || $limit == 'NOLIMIT') {
			try {
				if($adminobj->addEmail($_POST['emailname'], $_POST['contents'], $_POST['elang'])) {
					$eid = mysql_insert_id();			
					$adminstring = '<div class="makerelative"><a href="javascript:void(0);" class="editEmail" id="mailedit_' . $eid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditMail.png" alt="Edit Email" title="Edit Email" /></a><a href="javascript:void(0);" class="deleteEmail" id="emaildel_' . $eid . '">&nbsp;&nbsp;<img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_DeleteMail.png" alt="Delete EMail" title="Delete Email" /></a>&nbsp;</div>';
					$result_str = _('Adding Email Template Successful');
					$emailArr = array("message" => $result_str, "error" => false, "createdOn" =>  date('d/m/Y', time()), "adminString" => $adminstring);
					
					$adminobj->runTracker('Added new Email Template', $_POST['emailname'], $eid, 'EMAIL');
					echo json_encode($emailArr);
				}
			} catch (Exception $e) {
				$emailArr = array("message" => $e->getMessage(), "error" => true);
				echo json_encode($emailArr);
			}
		} else {
			$result_str = _('Email Template not added. You have reached your limit for adding new Email Templates');
			$emailArr = array("message" => $result_str, "error" => true);
			echo json_encode($emailArr);
		}
	}
	
	if(isset($_POST['editEmail'])) {
		if(!$adminobj->updateEmail($_POST['eid'], $_POST['emailname'], $_POST['contents'], $_POST['elang'])) {
			echo _('Updating Email Template Failed');
		} else {
			$adminobj->runTracker('Updated Email Template', $_POST['emailname'], $_POST['eid'], 'EMAIL');
			echo _('Updating Email Template Successful');
		}
	}
	
		if(isset($_GET['deleteEmail'])) {
		if(!$adminobj->deleteEmail($_GET['emailid'])) {
			echo _('Deleting Email Template Failed');
		} else {
			$adminobj->runTracker('Deleted Email Template', $_GET['emailname'], $_GET['emailid'], 'EMAIL');
			echo _('Deleting Email Template Successful');
		}
	}
	
	if(isset($_GET['active'])) {
		$userdetails = $adminobj->getUserVID($_GET['uid']);
		if(!$adminobj->userActive($_GET['uid'], $_GET['active'])) {
			echo 'fail';
		} else {
			if($_GET['active'] == 1) {
				$str = _("Activated User");
			} else {
				$str = _("Deactivated User");
			}
			$adminobj->runTracker($str, $userdetails->firstname . ' ' . $userdetails->surname, $_GET['uid'], 'USER');
			echo 'success';
		}
	}
	
	if(isset($_GET['updateTotal'])) {
		if($_GET['incDec'] == 'increment') {
			$decrease = 0;
		} else {
			$decrease = 1;
		}
		
		if($adminobj->incrementTotals($_GET['updateKey'], $_GET['accid'], $decrease)) {
			$totals_arr = array("message" => _('Totals successfully amended'));
		} else {
			$totals_arr = array("message" => _('Error changing totals'));
		}
		
		echo json_encode($totals_arr);
	}
	
	if(isset($_GET['getEmailList'])) {
		$emails_arr = $adminobj->getEmailList('all', $_GET['langval']);
		echo json_encode($emails_arr);
	}

	/****************************************************************************
	*																																						*
	*																	SHARING																		*
	*																																						*
	****************************************************************************/
	
	if(isset($_POST['shareCompany'])) {
		if(!$adminobj->addCompanyShare($_POST['companyid'], $_POST['accshareObj'])) {
			echo _("Account access has failed to be added. Please return to the first stage and try again!");
		} else {
			if(isset($_POST['reqApprove'])) {
				//status = 2 is approved
				$adminobj->updateShareRequest($_POST['companyid'], $adminobj->uid, $_POST['requid'], 2);
			}
			echo _("Access has successfully been given to the selected users.");
		}
	}
	
	if(isset($_GET['deleteShare'])) {
		if(!$adminobj->deleteCompanyShare($_GET['companyid'], $_GET['uid'])) {
			echo _("Access to the account has not been removed. Please try again!");
		} else {
			echo _("Access to the account has been removed successfully!");
		}
	}
	
	if(isset($_GET['checkShareUsers'])) {
		$SHUsers = $adminobj->getAccountShareUsers($_GET['cid']);
		echo json_encode($SHUsers);
	}

	if(isset($_GET['getShareAccess'])) {
		$SHAccess = $adminobj->getShareAccessUID($_GET['uid'], $_GET['cid']);
		echo json_encode($SHAccess);
	}
	
	if(isset($_POST['updateShareAccess'])) {
		if(!$adminobj->updateCompanyShare($_POST['companyid'], $_POST['accshareObj'])) {
			echo _("I'm sorry, there was an error whilst updating the access rights. Please try again!");
		} else {
			echo _("Access Rights updated successfully!");
		}
	}
		
	
			
	/****************************************************************************
	*																																						*
	*																	LANGUAGES																	*
	*																																						*
	****************************************************************************/
	
	if(isset($_GET['updateLangs'])) {
		//if you want to update the preferences for another account, simply set the accid to that accid here instead of the current adminobj accid (i.e. who you are logged in as)
		$accid = $adminobj->accid;
		if($adminobj->updateLangPrefs($_GET['langList'], $accid)) {
			$result_str = _('Language choices updated successfully');
			$langResult = array("message" => $result_str, "error" => false);
		} else {
			$result_str = _('Error: Languages not update successfully');
			$langResult = array("message" => $result_str, "error" => true);
		}
		echo json_encode($langResult);
	}
	
	/****************************************************************************
	*																																						*
	*																	ADD LEVEL																	*
	*																																						*
	****************************************************************************/
	
	if(isset($_POST['addLevel'])) {
		if(!$adminobj->addLevel($_POST['levelName'], $_POST['minAccess'], $_POST['levelObj'], $_POST['modids'])) {
			echo 'not added!';
		} else {
			echo 'added';
		}
	}
	
	/****************************************************************************
	*																																						*
	*																ADD/EDIT ACCOUNT														*
	*																																						*
	****************************************************************************/
	
	if(isset($_POST['addAccount'])) {
		try {
			$result = $accobj->addAccount($_POST['accName'], $_POST['adminEmail'], $_POST['serviceLevel'], $_POST['salutation'], $_POST['firstname'], $_POST['surname'], $_POST['countryid'], $acctype);
			if(!$result) {
				$acctype = true;
				echo 'There was an error adding the account. Please try again.';
			} else {
				$acctype = false;
				echo 'The account has been added successfully!';
			}
		} catch(Exception $e) {
			echo 'There was an error adding the account. Please try again.';
		}
	}
	
	if(isset($_POST['getAccount'])) {
		//1 is to tell its from Admin so it doesn't refine fixed vars for current account
		$accInfo = $accobj->getAccount($_POST['accid'], 1);
		echo json_encode($accInfo);
	}
	
	if(isset($_POST['getLimits'])) {
		//1 is to tell its from Admin so it doesn't refine fixed vars for current account
		$limitInfo = $adminobj->getLimits($_POST['accid'], $_POST['useLimits'], $_POST['serviceid']);
		echo json_encode($limitInfo);
	}
	
	if(isset($_POST['updateCustomLimits'])) {
		if($_POST['useLimits'] == 0) {
			$clMsg = _("No");
			$clUpdate = _("Custom Limits Deactivated");
		} else {
			$clMsg = _("Yes");
			$clUpdate = _("Custom Limits Activated");
		}
		if($accobj->updateCustomLimit($_POST['accid'], $_POST['useLimits'])) {
			$updateResult = array("message" => $clUpdate, "clMsg" => $clMsg);
		} else {
			$updateResult = array("message" => _("There was an error toggling the custom Limits"), "clMsg" => $clMsg);
		}
		echo json_encode($updateResult);
	}
	
	
	if(isset($_POST['updateAccount'])) {
		if($accobj->updateAccount($_POST['accid'], $_POST['accName'], $_POST['adminEmail'], $_POST['serviceid'])) {
			$updateResult = array("message" => _("Account successfully updated"));
		} else {
			$updateResult = array("message" => _("Account details were not updated, please try again."));
		}
		echo json_encode($updateResult);
	}
	
	if(isset($_POST['updateLimits'])) {
		if($adminobj->updateLimits($_POST['accid'], $_POST['useLimits'], $_POST['limitsObj'])) {
			$updateResult = array("message" => _("Account limits successfully updated"));
		} else {
			$updateResult = array("message" => _("Account limits were not updated, please try again."));
		}
		echo json_encode($updateResult);
	}
	
?>