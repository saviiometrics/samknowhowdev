<?php

	session_start();
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");
	
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';

	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	
	$adminobj = new Admin($_SESSION['uid']);
	$limitsArr = $adminobj->getLimits();
	$superAccess = $adminobj->getLevelAccess('admin');
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	function get_months($date1, $date2) {
	   $time1  = $date1;
	   $time2  = $date2;
	   $my     = date('mY', $time2);
		
		 $months = array();
		 $i = 0;
	   $months[$i] = date('F', $time1);
		 
	   while($time1 < $time2) {
	   	$i++;
	      $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
	      if(date('mY', $time1) != $my && ($time1 < $time2)) {
	         $months[$i] = date('F', $time1);
	      }
	   }
	
	   $months[] = date('F', $time2);
	   return $months;
	}
	
	//this gets survey usage stats!
	if(isset($_GET['givemejson'])) {	
		$start_time = mktime(0,0,0,$_GET['ms'], 1, $_GET['ys']);
		$end_time = mktime(0,0,0,$_GET['me'], 1, $_GET['ye']);
		$monthsarr = get_months($start_time, $end_time);
		$selector_arr = array();
		$startmonth = date('m', $start_time);
		$startyear = date('Y', $start_time);

		$results = $adminobj->getUsage($monthsarr, $startmonth, $startyear, $_GET['maptype'], $_GET['uidstring'], $_GET['cidstring']);
		echo json_encode($results);
	}
	
	if(isset($_GET['pieTMAP'])) {	
		$results = $adminobj->getTMAPUsage($_GET['companyid']);
		echo json_encode($results);
	}
	
	if(isset($_GET['pieMAP'])) {	
		$results = $adminobj->getSurveyUsage($_GET['companyid']);
		echo json_encode($results);
	}
	
	$results = $adminobj->getSurveyUsage($_GET['companyid']);

	if(isset($_GET['editCompany'])) {
		if(!$adminobj->updateCompany($_GET['companyid'], $_GET['cname'], $_GET['cdesc'], $_GET['cid'])) {
			printf (_('Updating Account %s failed!'), $_GET['cname']);
		} else {
			$adminobj->runTracker('Updated Account', $_GET['cname'], $_GET['companyid'], 'COMPANY');
			printf (_('Updated Account %s successfully!'), $_GET['cname']);
		}
	}
	
	if(isset($_GET['getCompany'])) {
		$comobj = $adminobj->getCompany($_GET['companyid']);
		echo $comobj->companyname . '---' . $comobj->description . '---' . $comobj->countryid;
	}
	
	if(isset($_GET['addCompany'])) {
		//check type being added and see if the associated limit has been reached or not
		$addme = false;
		if($limitsArr['accounts'] == 'NOLIMIT') {
			$limit_head = 'NOLIMIT';
		} else {
			$limit_head = ($limitsArr['accounts'] - TOTAL_ACCOUNTS);
		}
		
		if($limitsArr['subaccounts'] == 'NOLIMIT') {
			$limit_sub = 'NOLIMIT';
		} else {
			$limit_sub = ($limitsArr['subaccounts'] - TOTAL_SUBACCOUNTS);
		}

		if($_GET['type'] == 'head' && ($limit_head >= 1 || $limit_head == 'NOLIMIT')) {
			$addme = true;
			$typeShow = 'Main';
		} else if ($_GET['type'] == 'sub' && ($limit_sub >= 1 || $limit_sub == 'NOLIMIT')) {
			$addme = true;
			$typeShow = 'Sub';
		}
		
		if($addme == true || $superAccess['GODADMIN'] == 'TRUE') {
			//this is so only god admins can send a custom accid through so stop POST manipulation etc. If not a god admin then just users current users one when set to 0 in addCompany in admin obj
			if($superAccess['GODADMIN'] == 'TRUE') {
				$curraccid = $_GET['accid'];
			} else {
				$curraccid = 0;
			}
			if(!$adminobj->addCompany($_GET['headid'], $_GET['cname'], $_GET['cdesc'], $_GET['cid'], $_GET['type'], $curraccid)) {
				$result_str = sprintf (_('Account %s failed to add!'), $_GET['cname']);
				$compArr = array("message" => $result_str, "error" => true);
				echo json_encode($compArr);
			} else {
				$result_str = sprintf (_('Account %s successfully Added!'), $_GET['cname']);
				if($_GET['type'] == 'head') {
					$compstr = 'Main Account';
				} else if($_GET['type'] == 'sub') {
					$compstr = 'Sub Account';
				}
				if($_GET['headid'] == 'new') {
					$mainacc = $_GET['cname'];
				} else {
					$compinfo = $adminobj->getCompany($_GET['headid']);
					$mainacc = $compinfo->companyname;
				}
				
				$adminstring = '';
				$adminstring .= '<a href="javascript:void(0);" class="editcompany" id="editcom_' . $_SESSION['newcompid'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EditApplication.png" /></a>&nbsp;&nbsp;' . "\n";
				$adminstring .= '<a href="javascript:void(0);" class="sharecompany" id="sharecom_' . $_SESSION['newcompid'] . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ShareAccount.png" /></a>&nbsp;&nbsp;' . "\n";
				
				$compArr = array("message" => $result_str, "comptype" => $compstr, "error" => false, "createdby" => $adminobj->firstname . ' ' . $adminobj->surname, "mainaccount" => $mainacc, "enterpriseAccount"=>$adminobj->getAccountName($curraccid), "adminString" => $adminstring, "newcompid" => $_SESSION['newcompid']);
				$cid = mysql_insert_id();
				$adminobj->runTracker('Added Account', $_GET['cname'], $cid, 'COMPANY');
				echo json_encode($compArr);
			}
		} else {
			$result_str = sprintf (_('Account not added. You have reached your limit for adding %s accounts'), $typeShow);
			$compArr = array("message" => $result_str, "error" => true);
			echo json_encode($compArr);
		}
	}
	
	if(isset($_GET['shareRequest'])) {
		if(!$adminobj->addShareRequest($_GET['compids'], $_GET['commts'])) {
			echo _('Account Access request failed to add!');
		} else {
			echo _('Account Access Request successfully Added!');
			//$cid = mysql_insert_id();
			//$adminobj->runTracker('Added Account', $_GET['cname'], $cid, 'COMPANY');
		}
	}

	if(isset($_GET['declineShare'])) {
		$status = 3;
		$display = 2;
		if(!$adminobj->declineShareRequest($_GET['srid'], $status, $display)) {
			echo _('There was an error declining access to this account. Please try again!');
		} else {
			echo _('Account share declined successfully');
			//$cid = mysql_insert_id();
			//$adminobj->runTracker('Added Account', $_GET['cname'], $cid, 'COMPANY');
		}
	}
	
	
	?>