<?php

	session_start();
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");
	
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	$adminobj = new Admin($_SESSION['uid']);
	
	function timeDiff($timediff) {
		//there are 86400 seconds in a day therefore...
		$timediff = ($timediff / 86400);
		if($timediff < 1) {
			$timediff = ($timediff * 24);
			if($timediff < 1) {
				$timediff = round(($timediff * 60),0);
				$timediff = $timediff . ' minutes ago';
			} else {
				$timediff = round($timediff,2) . ' hours ago';
			}
		} else {
			$timediff = round($timediff,1) . ' days ago';
		}
		return $timediff;
	}

	$countries = $adminobj->getCountries();
	$companies = $adminobj->getCompanies();
	 
	$sLimit = "";
	if (isset($_GET['iDisplayStart'])) {
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ". mysql_real_escape_string( $_GET['iDisplayLength'] );
	}

	if ( $_GET['sSearch'] != "" ) {
		$searchstr = mysql_real_escape_string($_GET['sSearch']);
	} else {
		$searchstr = '';
	}
	
	if(isset( $_GET['iSortCol_0'])) {
		$sOrder = "ORDER BY ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ ) {
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "ut.firstname";
		else if ( $i == 1 )
			return "ut.surname";
		else if ( $i == 4 )
			return "ct.companyname";
	}
	
	$userlist = $adminobj->getUsers(99, false, false, $searchstr, $sLimit, $sOrder);
	if($searchstr == '') {
		$iTotal = $adminobj->getTotalUsers();
		$iFilteredTotal = $iTotal;
	} else {
		$iTotal = $adminobj->getTotalUsers();
		$iFilteredTotal = count($userlist);
	}

	$sOutput = '{';
	$sOutput .= '"sEcho": '.intval($_GET['sEcho']).', ';
	$sOutput .= '"iTotalRecords": '.$iTotal.', ';
	$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	$sOutput .= '"aaData": [ ';
	
	/****************** START USER LOOP ********************/
	
	$timenow = time();
	$access = $adminobj->getLevelAccess('user');
	$shareaccess = $adminobj->getAccountShareAccess('user');
	function checkAccess($acctype, $html, $userARR, $adminid, $headid, $companyid, $checktype, $sharerights) {
		switch($acctype) {
			case 'ALL':
			$adminstring = $html;
			break;
			case 'CREATED':
			if($userARR['adminid'] == $adminid || $userARR['owneruid'] == $adminid) {
				$adminstring = $html;
			} else {
				if($sharerights[$userARR['companyid']][$checktype] == 'TRUE') {
					$adminstring = $html;
				}
			}
			break;
			case 'MA':
			if($userARR['headid'] == $headid) {
				$adminstring = $html;
			} else {
				if($sharerights[$userARR['companyid']][$checktype] == 'TRUE') {
					$adminstring = $html;
				}
			}
			break;
			case 'SA':
			if($userARR['companyid'] == $companyid) {
				$adminstring = $html;
			} else {
				if($sharerights[$userARR['companyid']][$checktype] == 'TRUE') {
					$adminstring = $html;
				}
			}
			break;
		}
		return $adminstring;
	}
	if(!empty($userlist)) {
		foreach($userlist as $uid => $userARR) {
			$adminstring = '';
			$editaccessHTML = '<a href="javascript:void(0);" class="editaccess" id="user_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_Access.png" class="manageaccess" alt="' . _("Manage Access") . '" /></a>&nbsp;&nbsp;';
			$retakeHTML = '<a href="javascript:void(0);" class="incretake" id="retake_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_Retake.png" alt="' . _("Allow Retake") . '" title="' . _("Allow Retake") . '" /></a>&nbsp;&nbsp;';
			$resendHTML = '<a href="javascript:void(0);" class="reSend" id="resend_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ResendEmail.png" alt="' . _("Resend Email Invitation") . '" title="' . _("Resend Email Invitation") . '" /></a>&nbsp;&nbsp;';
			$edituserHTML = '<a href="javascript:void(0);" class="editUser" id="edituser_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ManageUsers.png" alt="' . _("Edit User") . '" title="' . _("Edit User") . '" /></a>&nbsp;&nbsp;';
			$adminstring .= checkAccess($access['ACCESS'], $editaccessHTML, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'ACCESS', $shareaccess);
			$adminstring .= checkAccess($access['EDIT'], $edituserHTML, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'EDIT', $shareaccess);
			if($userARR['status'] == 1) {
				$typeshow = 'Active';
				$usertype = 'useractive';
				$activelink = '<a href="javascript:void(0);" class="deacUser" id="deactivate_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_DeactivateUser.png" alt="' . _("Deactivate User") . '" title="' . _("Deactivate User") . '" /></a>&nbsp;&nbsp;';
				$adminstring .= checkAccess($access['ACTIVATION'], $activelink, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'DEACTIVATE', $shareaccess);
			} elseif($userARR['status'] == 2) {
				$typeshow = 'Authenticated';
				$usertype = 'userauthed';
				$activelink = '<a href="javascript:void(0);" class="deacUser" id="deactivate_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_DeactivateUser.png" alt="' . _("Deactivate User") . '" title="' . _("Deactivate User") . '" /></a>&nbsp;&nbsp;';
				$adminstring .= checkAccess($access['ACTIVATION'], $activelink, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'DEACTIVATE', $shareaccess);
			} else {
				$typeshow = 'Deactivated';
				$usertype = 'userdeactive';
				$activelink = '<a href="javascript:void(0);" class="acUser" id="activate_' . $uid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_ActivateUser.png" alt="' . _("Activate User") . '" title="' . _("Activate User") . '" /></a>&nbsp;&nbsp;';
				$adminstring .= checkAccess($access['ACTIVATION'], $activelink, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'ACTIVATE', $shareaccess);
			}
			$adminstring .= checkAccess($access['RETAKE'], $retakeHTML, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'RETAKE', $shareaccess);
			$adminstring .= checkAccess($access['RESEND'], $resendHTML, $userARR, $adminobj->uid, $adminobj->headid, $adminobj->companyid, 'RESEND', $shareaccess);
			
			if($adminstring == '') {
				$adminstring = _('Read Only');
			}
			
			if($userARR['level'] == 1 && $adminobj->level != 1) {
				$adminstring = _('Read Only');
			}

			$timeadded = $userARR['timefinish'];
			$timediffINT = ($timenow - $timeadded);
			$timeissueINT = ($timenow - $userARR['timeissue']);
			$timediff = timeDiff($timediffINT);
			$timediffIssue = timeDiff($timeissueINT);
			
			$sOutput .= "[";
			$sOutput .= json_encode($userARR['firstname']).',';
			$sOutput .= json_encode($userARR['surname']).',';
			//$sOutput .= '"'.addslashes('<img src="../' . ADMIN_ACCOUNT_PATH . '_images/_flags/' . $userARR['countryid'] . '.gif" alt="' . $countries[$userARR['countryid']] . '" title="' . $countries[$userARR['countryid']] . '" />').'",';
			$sOutput .= json_encode('<img src="..' . ACCOUNT_PATH . '_images/_flags/' . $userARR['countryid'] . '.gif" alt="' . $countries[$userARR['countryid']] . '" title="' . $countries[$userARR['countryid']] . '" />').',';
			if($timeadded == 0) {
				$sOutput .= json_encode('<div class="nevertaken">' . _("Date Issued") . ' - ' . date("d/m/Y", $userARR['timeissue']) . ' (' . $timediffIssue . ')</div>').',';
			} else {
				switch($timediffINT) {
					case $timediffINT <= 15724800: //26 weeks
					$decayClass = 'decaygreen';
					break;
					case $timediffINT <= 18144000: //30 weeks
					$decayClass = 'decayorange';
					break;
					case $timediffINT >= 18144000: //more than 4 weeks
					$decayClass = 'decayred';
					break;
				}
				$sOutput .= json_encode('<div class="' . $decayClass . '">' . date('d/m/Y', $userARR['timefinish']) . ' (' . $timediff . ')</div>').',';
			}
			$sOutput .= json_encode($userARR['company']).',';
			$sOutput .= json_encode($typeshow).',';
			$sOutput .= json_encode('<div class="makerelative">' . $adminstring . '</div>');
			$sOutput .= "],";
		}
	}
			
	$sOutput = substr_replace( $sOutput, "", -1 );
	$sOutput .= '] }';
	
	echo $sOutput;

?>