<?php
		/**
		* MSajaxUser.php
		*
		* The results returned by this script matches the format found in data.txt,
		* ie: "key=value", one per line.
		*
		* Arguments :
		*
		* q the query string to search. The provided string will be
		* tested against the "value" part of the data (case insensitive)
		* Ex: q=en will match any data containing *en*
		* limit (optional) specify a limit of results to return. If not
		* specified, the default value is DEFAULT_LIMIT
		* Ex: limit=20 will not return more than 20 results even
		* if more matches
		*
		*
		*/
		session_start();
		 
 		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");
		
		$adminobj = new Admin($_SESSION['uid']);
		$MAPobj = new MAP();
		
		// the default limit if the parameter is not specified
		define('DEFAULT_LIMIT', 25);

		header('Content-type: text/plain; charset=utf-8');
		 
		$limit = isset($_GET['limit']) && !empty($_GET['limit']) ? $_GET['limit'] : DEFAULT_LIMIT;
		$minlevel = 4;
		$companies = $adminobj->getCompanies("all", true);
		if ( isset($_GET['q']) && !empty($_GET['q']) ) {
			$count = 0;
			foreach($companies as $cid => $companyarr) {
				$accname = $companyarr['companyname'] . ' <strong>(Owner - ' . $companyarr['fullname'] . ')</strong>';
				if ( false !== stripos($accname, urldecode($_GET['q'])) ) {
			  	echo $cid . '-' . $companyarr['uid'] . '=' . $accname . "\n";
			  	if ( ++$count >= $limit ) break;
			  }
			}
		} else { 
			$count = 0;
			foreach($companies as $cid => $companyarr) {
				$accname = $companyarr['companyname'] . ' <strong>(Owner - ' . $companyarr['fullname'] . ')</strong>';
		  	echo $cid . '-' . $companyarr['uid'] . '=' . $accname . "\n";
		  	if ( ++$count >= $limit ) break;
			}
		}