<?php

	session_start();
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");

	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);

	//include other classes
	include FULL_PATH . '/_inc/_classes/user-class.php';
	include FULL_PATH . '/_inc/_classes/admin-class.php';
	
	$adminobj = new Admin($_SESSION['uid']);
		
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");	

	
	//$adminobj->updateFA();
	if(isset($_GET['newOrderAS'])) {
		$userinfo = $adminobj->getUserVID($_GET['ASUid']);
		if(!$adminobj->updateAdminAccess($_GET['newOrderAS'], $_GET['ASUid'])) {
			printf (_('Updating access levels for %s %s failed!'), $userinfo->firstname, $userinfo->surname);
		} else {
			$adminobj->runTracker('Updated Access', $userinfo->firstname . ' ' . $userinfo->surname, $_GET['ASUid'], 'ACCESS');
			printf (_('Updating access levels for %s %s was successful!'), $userinfo->firstname, $userinfo->surname);
		}
	}
	
	if(isset($_GET['getAUID'])) {
		//this is an array of modules only master admins are allowed access too!
		$mastadmin = array(4,5,13);
		$allmodules = $adminobj->getALLModules();
		$MODAccessIDs = $adminobj->getModuleAccess($_GET['getAUID']);
		foreach($allmodules as $modid => $infoarr) {
			if($adminobj->level > 1) {
				if(in_array($modid, $mastadmin)) {
					continue;
				}
			}
			if(!in_array($modid, $MODAccessIDs)) {
				echo '<li class="ui-state-default" id="modid-' . $modid . '">' . utf8_encode(_($infoarr['module_name'])) . '</li>' . "\n";
			}
		}
		echo '---';
		foreach($allmodules as $modid => $infoarr) {
			if(in_array($modid, $MODAccessIDs)) {
				echo '<li class="ui-state-default" id="modid-' . $modid . '">' . utf8_encode(_($infoarr['module_name'])) . '</li>' . "\n";
			}
		}
		$userobj = new User();
		$userobj->getUser($_GET['getAUID']);
		echo '---';
		echo $userobj->firstname . ' ' . $userobj->surname;
	}
	?>