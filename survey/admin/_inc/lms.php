
<script type="text/javascript">

	$(function() {
			jQuery.fn.fadeToggle = function(speed, easing, callback) {
 				return this.animate({opacity: 'toggle'}, speed, easing, callback);
			}; 
			
			$("#ctype").change(function() {
				if($(this).val() == 'sub') {
					$(".compheadlist").show();
				} else {
					$(".compheadlist").hide();
				}
			});
			
			$(".lmscat").hover(
				 function() {$(".lmsmodules", this).fadeToggle(200);}, 
	       function() {$(".lmsmodules", this).fadeToggle(200);} 
			);

			
			$(".modlink").click(function() {
				var lmid = this.id.substr(5);
				$.get("AjaXLMS.php", {lmid: lmid, getLMSModule: 'true' },
				  function(j){
				    $("#modulename").html(j.moduleinfo.name);
				    $("#lmsummary").html(j.moduleinfo.summary);
				    $("#lmcontent").html(j.moduleinfo.content);
				    var dlstring = '';
				    if(j.filesinfo != undefined) {
					    $.each(j.filesinfo, function(fid, fileobj) {
					    	dlstring += '<img src="' + $("#accpath").val() + '_images/_icons/_files/' + fileobj.type + '.jpg" /> <a href="download.php?fn=' + fileobj.filename + '&xfid=' + fid + '" target="_blank">' + fileobj.name + '</a> (' + fileobj.size + ' bytes)<br />';
					    });
					  } else {
					  	dlstring = $("#dlstring").val();
					  }
				    $("#dlfiles").html(dlstring);
				    if(j.moduleinfo.video == 'novideo') {
				    	if($("#lmsvidholder").is(':visible')) {
				    		$("#lmsvidholder").slideToggle(500, function() {
				    			$("#vidme").html('');
					    		$("#lms_summary").animate({ 
						        marginLeft: "0px"
						      }, 500, "easein");
				    		});
				    		$("#lms_summary").show(0);
				    	}
				    } else {
				    	if($("#lmsvidholder").is(':hidden')) {
		    					flashvars.videoURL = $("#accpath").val() + '_uploads/_videos/' + j.moduleinfo.video;
			   					swfobject.embedSWF("MD_AS3VideoPlayer_FlashVar.swf", "vidme", "400", "300", "9.0.0", false, flashvars, params, attributes);
				    			$("#lms_summary").animate({ 
						        marginLeft: "420px"
						      }, 500, "easein", function() {
						      	$("#lmsvidholder").slideToggle(500);
						      	$("#lms_summary").show(0);
						      });
				    	} else {
				    		$("#lms_summary").show(0);
			    			flashvars.videoURL = $("#accpath").val() + '_uploads/_videos/' + j.moduleinfo.video;
		   					swfobject.embedSWF("MD_AS3VideoPlayer_FlashVar.swf", "vidme", "400", "300", "9.0.0", false, flashvars, params, attributes);
				    	}
				    }
				  }, "json");
			});

	    
	});//end doc rdy
	
				
	//vid player options
	var flashvars = {};
	flashvars.autoplay 			= "false";
	flashvars.playerW 			= "400";
	flashvars.playerH 			= "300";
	//flashvars.videoURL 			= "";
	flashvars.rtmpPath			= "";
	flashvars.controlsW 		= "360";
	flashvars.controlsAuto 		= "true";
	flashvars.showFullscreen	= "true";
	flashvars.controlOpacity 	= "50";
	flashvars.border 			= "0";
	flashvars.borderColor 		= "0x444444";
	flashvars.scrubberColor 	= "0x993d8f";
	
	var params = {};
	params.wmode =  "transparent";
	params.salign				= "tl";
	params.allowFullScreen 		= "true";
	params.allowScriptAccess	= "sameDomain";
	
	var attributes = {};

</script>
<input type="hidden" name="xRTG" id="xRTG" value="<?php if($_SESSION['level'] == 1 || $_SESSION['level'] == 2) { echo 'Y'; } else { echo 'N'; }?>" />
<input type="hidden" value="<?=ADMIN_ACCOUNT_PATH;?>" name="accpath" id="accpath" />
<input type="hidden" value="<?=_("No downloadable content available for this module");?>" name="dlstring" id="dlstring" />


<div id="userbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Learning Resources")?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
	<h1 class="MWxHeader"><?=_("Learning Resources")?></h1>
    <p><?=_("Welcome to Learning Resources where you can continue to develop your knowledge and understanding of things that you consider important. We will constantly update this section with new resources and let you know when this happens.");?></p>
    <p><?=_("We have created four major categories of resources");?>:</p>
    <ul>
	    <li><?=_("Business Development");?> - <?=_("Learn more about the SAM business model and how to engage with new and existing clients");?></li>
	    <li><?=_("Management Models");?> - <?=_("Management theories and models related to motivation, leadership, teams and culture.");?></li>
	    <li><?=ACC_NAME;?> - <?php printf(_("A collection of short Video-based tutorials to help you make the most of %s."), ACC_NAME);?></li>
	    <li><?=_("FAQs");?> - <?=_("A collection of responses to your questions and where to get the answers.");?></li>
    </ul>
    <p><?=_("Use the Submit Feedback form at the top of this page to request additional resources and we will respond accordingly!");?></p>
	<div style="height:36px; margin:8px 0px 12px 0px;">
		<div id="lmsmenu">
			<ul>
				<?php
				$lmscats = array(1 => _("Business Development"),2 => _("Management Models"),3 => ACC_NAME);
				foreach($lmscats as $lmscatid => $catname) {
					echo '<li class="lmscat">' . "\n";
					echo '<a href="javascript:void(0);">' . $catname . '</a>' . "\n";
					$lmsmodules = $adminobj->getLMSModules($lmscatid);
					if(count($lmsmodules) != 0) {	
						echo '<div class="lmsmodules">' . "\n";
						echo '<div class="modulemenu">' . "\n";
						echo '<ul>' . "\n";
						foreach($lmsmodules as $lmid => $lmname) {
							echo '<li class="modlink" id="lmid_' . $lmid . '"><a href="javascript:void(0);">' . $lmname . '</a></li>' . "\n";
						}
						echo '</ul>' . "\n";
						echo '</div>' . "\n";
						echo '</div>' . "\n";
					}
					echo '</li>' . "\n";
				}
				?>
			</ul>
		</div>
	</div>
	<div id="lmsvidholder">
		<div id="vidme"></div>
	</div>
	<div id="lms_summary">
		<strong><?=_("Summary");?></strong><br />
		<div id="lmsummary">
		</div>
		<p><strong><?=_("Downloadable Content");?></strong></p>
		<div id="dlfiles"></div>
	</div>
	<div style="float:left; width:98%; min-height:100px; margin:4px 0px 20px 0px;">
		<span style="font-weight:bold; font-size:14px;" id="modulename"></span><br />
		<div id="lmcontent">
		</div>
	</div>
	<div class="cleaner"></div>
</div>


</div>

