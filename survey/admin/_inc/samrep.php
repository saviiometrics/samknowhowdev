<?php
$month = isset($_GET["month"]) ? $_GET["month"] : null;
if(is_null($month)) {
?>
  <form action="samrep.php" method="get">
      <table cellpadding="0" cellspacing="0">
          <tr>
              <td><? Month ?></td>
              <td><? Year ?></td>
          </tr>
          <tr>
              <td>
                  <select name="month">
                      <option value="1">January</option>
                      <option value="2">February</option>
                      <option value="3">March</option>
                      <option value="4">April</option>
                      <option value="5">May</option>
                      <option value="6">June</option>
                      <option value="7">July</option>
                      <option value="8">August</option>
                      <option value="9">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                  </select>
              </td>
              <td>
                  <select name="year">
                      <?
                        $year = date("Y");
                        for($i = 2011; $i <= $year; $i++){
                            echo "<option value='$i'>$i</option>";
                        }?>
                  </select>
              </td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="Generate" name="generate">
              </td>
          </tr>
      </table>
  </form>
<?php
} else {
    $year = $_GET["year"];
    if($month == 12) {
        $endmonth = 1;
        $endyear = $year + 1;
    } else {
        $endmonth = $month + 1;
        $endyear = $year;
    }

    $start = mktime(0,0,1,$month,1,$year);
    $end = mktime(0,0,1,$endmonth,1,$endyear) - 1;
    $out = array();
    $mySQLConnection = new mysqli('localhost', 'saviiomaps', 'Ch7g$#%G23', 'savmaps', 3306);
    $stmt = $mySQLConnection->prepare('SELECT uid, email, firstname, surname FROM sav_users WHERE accid = 17 AND username like "%sam-int.com" AND email like "%sam-int.com"');
    //$stmt = $mySQLConnection->prepare('SELECT uid, email, firstname, surname FROM sav_users WHERE accid = 9');
    $stmt->execute();
    $stmt->bind_result($uid, $email, $firstname, $surname);
    while($stmt->fetch()) {
        $out[] = array('uid'=>$uid, 'email'=>$email, 'firstname'=>utf8_decode($firstname), 'lastname'=>utf8_decode($surname), 'Activity'=>array());
    }
    $stmt->close();
    $stmt = $mySQLConnection->prepare('SELECT action, result, timestamp FROM sav_log WHERE uid = ? AND timestamp > ' . $start .' AND timestamp < ' . $end);
    foreach($out as $id=>$user) {
        $uid = $user['uid'];
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->bind_result($action, $result, $time);
        while($stmt->fetch()) {
            $out[$id]['Activity'][] = array('action'=>utf8_decode($action), 'result'=>utf8_decode($result), 'time'=>date("H:i:s d M y",$time));
        }
    }
    $stmt->close();
    $pages = array();
    $noPages = array();
    $maps = array();
    $noMaps = array();
    foreach ($out as $uid=>$user) {
        $count = 0;
        $pvs = 0;
        foreach ($user["Activity"] as $aid=>$activity) {
            if($activity['action'] == "Added User") {
                $count ++;
            } elseif($activity['action'] == "Viewed Page") {
                $pvs ++;
            }
        }
        if($count > 0) {
            $maps[] = $user;
        } else {
            $noMaps[] = $user;
        }
        if($pvs > 0) {
            $pages[] = $user;
        } else {
            $noPages[] = $user;
        }
    }

        ?>
<h1>Usage report for <?= $month ?>/<?= $year ?></h1>
    <table border="1">
  <tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Email</th>
    <th>MAPs Issued</th>
  </tr>

    <?php
    foreach ($maps as $user) {
        $count = 0;
        foreach ($user["Activity"] as $activity) {
            if($activity['action'] == "Added User") {
                $count ++;
            }
        }
        ?>
         <tr>
            <td><?= $user['firstname']; ?></td>
            <td><?= $user['lastname']; ?></td>
               <td><?= $user['email']; ?></td>
               <td><?= $count; ?></td>
          </tr>
        <?php

    }
    foreach ($noMaps as $user) {
        $count = 0;
        foreach ($user["Activity"] as $activity) {
            if($activity['action'] == "Added User") {
                $count ++;
            }
        }
        ?>
         <tr>
            <td><?= $user['firstname']; ?></td>
            <td><?= $user['lastname']; ?></td>
               <td><?= $user['email']; ?></td>
               <td><?= $count; ?></td>
          </tr>
        <?php

    }
    ?>
    </table>
    <?php

    ?>

    <table border="1">
  <tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Email</th>
    <th>Page viewed</th>
    <th>Date</th>
  </tr>

    <?php
    foreach ($pages as $user) {
        ?>
         <tr>
            <td><?= $user['firstname']; ?></td>
            <td><?= $user['lastname']; ?></td>
               <td><?= $user['email']; ?></td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
          </tr>
        <?php
        foreach ($user["Activity"] as $activity) {
            if($activity['action'] == "Viewed Page") {
        ?>
         <tr>
             <td>&nbsp;</td>
               <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= $activity['result']; ?></td>
               <td><?= $activity['time']; ?></td>
          </tr>
        <?php
            }
    }
    }
    foreach ($noPages as $user) {
        ?>
         <tr>
            <td><?= $user['firstname']; ?></td>
            <td><?= $user['lastname']; ?></td>
               <td><?= $user['email']; ?></td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
          </tr>
        <?php
        foreach ($user["Activity"] as $activity) {
            if($activity['action'] == "Viewed Page") {
        ?>
         <tr>
             <td>&nbsp;</td>
               <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= $activity['result']; ?></td>
               <td><?= $activity['time']; ?></td>
          </tr>
        <?php
            }
    }
    }
    ?>
    </table>
    <?php
}
