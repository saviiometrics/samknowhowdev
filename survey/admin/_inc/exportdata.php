
<?php
session_start();
$phpsessid = session_id();
//ini_set("display_errors", 1);
//include localization and site config files
require_once("../../site.config.php");

include CONTENT_PATH . '/_classes/db-class.php';
include CONTENT_PATH . '/_classes/account-class.php';
if(!isset($_SESSION['accid'])) {
	if($_SERVER['SERVER_NAME'] == 'www.saviio.com') {
		$_SESSION['accid'] = 1;
	} else {
		$_SESSION['accid'] = 1;
	}
}
$accobj = new Account($_SESSION['accid']);

//include other classes
include CONTENT_PATH . '/_classes/user-class.php';
include CONTENT_PATH . '/_classes/admin-class.php';
include CONTENT_PATH . '/_classes/MAP-class.php';

header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=saviio_export_".date("d-m-y").".csv");
header("Pragma: no-cache");
header("Expires: 0");
function sputcsv($row, $delimiter = ',', $enclosure = '"', $eol = "\n")
{
    static $fp = false;
    if ($fp === false)
    {
        $fp = fopen('php://temp', 'r+'); // see http://php.net/manual/en/wrappers.php.php - yes there are 2 '.php's on the end.
        // NB: anything you read/write to/from 'php://temp' is specific to this filehandle
    }
    else
    {
        rewind($fp);
    }
   
    if (fputcsv($fp, $row, $delimiter, $enclosure) === false)
    {
        return false;
    }
   
    rewind($fp);
    $csv = fgets($fp);
   
    if ($eol != PHP_EOL)
    {
        $csv = substr($csv, 0, (0 - strlen(PHP_EOL))) . $eol;
    }
   
    return $csv;
}

// test
$userobj = new Admin($_SESSION['uid']);
$MAPobj = new MAP();
$access = $userobj->getLevelAccess('map');
if($access['VIEW'] == 'MA') {
	$compid = $userobj->headid;
} else {
	$compid = $userobj->companyid;
}
$userlist = $userobj->exportAll();

if (PHP_EOL == "\r\n")
{
    $eol = "\n";
}
else
{
    $eol = "\r\n";
}

foreach($userlist as $row)
{
	 	$newrow = implode(',', $row);
	 	$newarr = explode(',',$newrow);
    echo sputcsv($newarr, ',', '"', $eol);
}


?>
