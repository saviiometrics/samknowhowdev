
<div id="userbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Administrator Access Log");?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?=_("Administrator Access Log")?></h1>
<p><?=_("This page enables you to view all of the activity on the administration site. It includes, but not limited too, pageview activity, adding / updating of users and TalentMAP / TeamMAP activity.");?></p>
<br />

<div class="floatleft" style="padding:4px 4px 0px 0px;"><strong><?=_("Start Date");?></strong></div><div class="floatleft"><input type="text" id="timeStart"/></div> <div class="floatleft" style="padding:4px 4px 0px 8px;"><strong><?=_("End Date");?></strong></div><div class="floatleft"><input type="text" id="timeEnd"/></div><div class="floatleft" style="padding:4px 4px 0px 8px;"><a href="javascript:void(0);" class="testclick"><?=_("Apply Filter");?></a></div>
<div id="tableHolder">
	<table cellspacing="1" class="tablesorter">
		<thead>
			<tr>
				<th><?=_("Administrator's Action");?></th>
				<th><?=_("Timestamp");?></th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="7" class="dataTables_empty"><?=_("Loading data from server");?></td>
				</tr>
			</tbody>
			<tfoot>
			<tr>
				<th><?=_("Administrator's Action");?></th>
				<th><?=_("Timestamp");?></th>
			</tr>
		</tfoot>
		<tbody>
		</tbody>
	</table>
</div>
<div class="cleaner"></div>
</div>