<script type="text/javascript">

	$(function() {
		//   
	});//end doc rdy

</script>

<div id="mapbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Manage Feedback");?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?=_("Export Data")?></h1>
<p>From here you can export all of the data from the system to a CSV file. Later on you will be able to drag and drop which accounts / companies you want to include / exclude in the export process so you can avoid "junk" data being included.</p>
<p><a href="_inc/exportdata.php" target="_blank">Export All Data</a></p>
<br />
<h1 class="MWxHeader"><?=_("Monthly report")?></h1>
<p>From here you can generate the monthly usage report</p>
      <form action="_inc/samrep.php" method="get" target="_blank">
      <table cellpadding="0" cellspacing="0">
          <tr>
              <td><? Month ?></td>
              <td><? Year ?></td>
          </tr>
          <tr>
              <td>
                  <select name="month">
                      <option value="1">January</option>
                      <option value="2">February</option>
                      <option value="3">March</option>
                      <option value="4">April</option>
                      <option value="5">May</option>
                      <option value="6">June</option>
                      <option value="7">July</option>
                      <option value="8">August</option>
                      <option value="9">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                  </select>
              </td>
              <td>
                  <select name="year">
                      <?
                        $year = date("Y");
                        for($i = 2011; $i <= $year; $i++){
                            echo "<option value='$i'>$i</option>";
                        }?>
                  </select>
              </td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="Generate" name="generate">
              </td>
          </tr>
      </table>
  </form>
	<div class="cleaner"></div>
</div>

