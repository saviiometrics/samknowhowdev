
	<?php
	$MAPobj = new MAP();
	if($_GET['tmap'] == 'talentmap') {
		$tmapheader = 'Talent MAP';
		$tmaptype = 'talent';
	} else {
		$tmapheader = 'Team MAP';
		$tmaptype = 'team';
	}
	?>
	
	<script type="text/javascript">

	$(function() {
			$('.tablesorter').dataTable({
				//"bProcessing": true,
				"iDisplayStart": 0,
				"aaSorting": [],
				"iDisplayLength": 25,
				"sPaginationType": "full_numbers",
				"sDom": '<"filter_container"fl><"top"ip>rt<"bottom"ip<"clear">',
				"bAutoWidth": false
			});
			
			$('.deleteMAP').live("click", function(){
				var clicked = $(this);
				$(this).closest('tr').removeClass('odd even').addClass('red');
				var delid = this.id.substr(8);
	  		$.get("AjaXMAP.php", { tmapid:delid, deleteTMAP: 'yes' },
				  function(data){
						//alert(data);
						clicked.closest('td').html('<strong>Removed</strong>');
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:$('#tmapdeletecopy').val() + '? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});
				
			$('.unarchiveMAP').live("click", function(){
				var clicked = $(this);
				$(this).closest('tr').removeClass('odd even').addClass('green');
				var arcid = this.id.substr(8);
	  		$.get("AjaXMAP.php", { tmapid:arcid, archiveTMAP: 'reverse' },
				  function(data){
						//alert(data);
						clicked.closest('td').html('<strong>Un Archived</strong>');
				  });
			}).confirm({dialogShow:'fadeIn', wrapper:'<div class="confirmdialog"></span>', timeout:10000, msg:$('#tmaprestorecopy').val() + '? &nbsp;', buttons: {ok:'Yes',cancel:'No', separator:' / '}});

		  //this is needed.. because.. thats right.. if you change the view from the default one on the table sorter the tips dont work as it only applies it to the first ones seen.
		  //there is a trigger in the tablesorter.js file which triggers this function when the page it sorted or an update is done from the filter plugin, thus making the tooltip still work! imba
		  $(".userlist").live("toggleTips", function(e){
			  $(".userlist").tooltip({
					 effect:'slide',
					 bounce:true,
					 offset: [34, 0], 
					 position: 'top left',
					 relative: true, 
					 predelay:100
				 });
	    });

	    $(".userlist").trigger("toggleTips");
	    $("#tmapform").validationEngine('attach');
	    
	});//end doc rdy

</script>
<input type="hidden" name="tmapdeletecopy" id="tmapdeletecopy" value="<?php printf(_("Are you sure you want to delete this %s"), $tmapheader);?>" />
<input type="hidden" name="tmaprestorecopy" id="tmaprestorecopy" value="<?php printf(_("Are you sure you want to restore this %s"), $tmapheader);?>" />
<script type="text/javascript" src="_js/graph_controls.js"></script>
<input type="hidden" name="tmapheader" id="tmapheader" value="<?=$tmapheader;?>" />
<input type="hidden" name="tmaptype" id="tmaptype" value="<?=$tmaptype;?>" />
<input type="hidden" name="compid" id="compid" value="<?=$adminobj->companyid;?>" />
<input type="hidden" name="editTMAPID" id="editTMAPID" />
<input type="hidden" name="xRTG" id="xRTG" value="<?php if($_SESSION['level'] == 1 || $_SESSION['level'] == 2) { echo 'Y'; } else { echo 'N'; }?>" />

<div id="mapbar"><div class="iconfloat"><img src="<?=ADMIN_ACCOUNT_PATH;?>_images/_icons/IC_Structure.png" /></div> <div class="floatleft"><?=_("Archived Talent / Team MAPs");?></div></div>
<div class="titlebar_shadow"></div>
<div id="content_pad">
<h1 class="MWxHeader"><?=_("Manage Team / Talent MAP Archive")?></h1>
<p><?=_("This page enables you to manage all of your archived Talent & Team MAPs");?></p>
<br />

<?php
$strMAP = 'both';
$access = $adminobj->getLevelAccess('tmap');
if($access['VIEW'] == 'MA') {
	$compid = $adminobj->headid;
} else {
	$compid = $adminobj->companyid;
}
$tmaplist = $MAPobj->getTMAPList($strMAP, $compid, $access, 'admin', 1);
?>
<div id="tableHolder">
	<table cellspacing="1" class="tablesorter">
		<thead>
			<tr>
				<th><?=_("Talent / Team MAP Name");?></th>
				<th><?=_("MAP Type");?></th>
				<th><?=_("Contains Users");?></th>
				<th><?=_("Created By");?></th>
				<th><?=_("Created On");?></th>
				<th><?=_("Admin Panel");?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th><?=_("Talent / Team MAP Name");?></th>
				<th><?=_("MAP Type");?></th>
				<th><?=_("Contains Users");?></th>
				<th><?=_("Created By");?></th>
				<th><?=_("Created On");?></th>
				<th><?=_("Admin Panel");?></th>
			</tr>
		</tfoot>
		<tbody>
			<?php 
				foreach($tmaplist as $tmapid => $tmaparr) {
					$userstring = '<strong>User List for this ' . $tmapheader . '</strong><br /><ul class="mapuserlist">';
					$usernames = $MAPobj->getNamesVMAPids($tmaparr['mapids']);
					$usercount = count($usernames);
					foreach($usernames as $uid => $username) {
							$userstring .= '<li>'.$username['fullname'] . '</li>';
					}
					echo '</ul>' . "\n";
					echo '<tr>' . "\n";
					echo '<td>' . $tmaparr['tmapname'] . '</td>' . "\n";
					echo '<td>' . $tmaparr['type'] . '</td>' . "\n";
					echo '<td><a href="javascript:void(0);" class="userlist">User List</a><div class="tooltip">' . $userstring . '</div></td>' . "\n";
					echo '<td>' . $tmaparr['creator'] . '</td>' . "\n";
					echo '<td>' . date("d/m/Y", $tmaparr['created']) . '</td>' . "\n";
					echo '<td><div class="makerelative"><a href="javascript:void(0);" class="deleteMAP" id="tmapdel_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_EraserMinus.png" alt="Delete ' . ucwords($tmaparr['type']) . ' MAP" title="Delete ' . ucwords($tmaparr['type']) . ' MAP" /></a>&nbsp;
					<a href="javascript:void(0);" class="unarchiveMAP" id="tmaparc_' . $tmapid . '"><img src="' . ADMIN_ACCOUNT_PATH . '_images/_icons/IC_UnArchive.png" alt="UnArchive ' . ucwords($tmaparr['type']) . ' MAP" title="UnArchive ' . ucwords($tmaparr['type']) . ' MAP" /></a></div></td>' . "\n";
					echo '</tr>' . "\n";
				} 
			?>
		</tbody>
	</table>
</div>
	<div class="cleaner"></div>
</div>

