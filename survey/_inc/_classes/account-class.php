<?php

class Account {
		public $accid;
		public $accname;
		public $folderpath;
		public $dbprefix;
		public $nr_email;
		public $email;
		public $pagetitle;
		public $serviceid;
		public $useAccLimits;
		public $Acctable;
		public $serviceLevelTable;
		
		public function __construct($accid=0) {
			$this->Acctable = 'system_accounts';
			$this->serviceLevelTable = 'system_servicelevels';
			if($accid == 0) {
				$accid = 1;
			}
			$this->getAccount($accid);
		}
		
		public function genHash($length = 10, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890') {
		   $charLength = strlen($chars)-1;  
		   for($i = 0; $i < $length; $i++)  {  
		   	$randomString .= $chars[mt_rand(0,$charLength)];  
		   }  
		   return $randomString;
		}
		
		public function getAccount($accid, $fromAdmin=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->Acctable WHERE accid = '$accid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());
			
			$row = mysql_fetch_object($result);
			
			if($fromAdmin == 0) {
				$this->accid = $accid;
				$this->accname = $row->accountname;
				$this->dbprefix = $row->dbprefix;
				$this->folderpath = $row->folderpath;
				$this->nr_email = $row->nr_email;
				$this->email = $row->email;
				$this->time_added = $row->time_added;
				$this->serviceid = $row->serviceid;
				$this->useAccLimits = $row->useAccLimits;
				$this->pagetitle = _('Welcome to') . ' Saviio';
				if(is_null($this->dbprefix) || $this->dbprefix == '') {
					$this->dbprefix = "sav";
				}
				define ("ACCOUNT_PATH", '/_accounts/' . $this->folderpath . '/');
				define ("ADMIN_ACCOUNT_PATH", '/admin/_accounts/' . $this->folderpath . '/');
				define ("DB_PREFIX", $this->dbprefix);
				define ("NR_EMAIL", $this->nr_email);
				define ("ACC_EMAIL", $this->email);
				define ("ACC_NAME", 'Saviio');
				define ("SERVICE_ID", $this->serviceid);
				define ("USE_ACC_LIMITS", $this->useAccLimits);
				define ("ACC_TIME_ADDED", $this->time_added);
				
				//set Current limit values
				define ("TOTAL_MAPS", $row->ToT_maps);
				define ("TOTAL_ACCOUNTS", $row->ToT_accounts);
				define ("TOTAL_SUBACCOUNTS", $row->ToT_subaccs);
				define ("TOTAL_TALENTMAPS", $row->ToT_talent);
				define ("TOTAL_TEAMMAPS", $row->ToT_team);
				define ("TOTAL_LANGUAGES", $row->ToT_languages);
				define ("TOTAL_USERS", $row->ToT_users);
				define ("TOTAL_IDEAL", $row->ToT_ideal);
				define ("TOTAL_EMAILS", $row->ToT_emails);
			} else {
				$accArr = array("accid" => $accid, "accname" => $row->accountname, "accEmail" => $row->email, "serviceid" => $row->serviceid, "useAccLimits" => $row->useAccLimits, "maps" => $row->ToT_maps, "accounts" => $row->ToT_accounts, "subaccounts" => $row->ToT_subaccs, "talentMAPs" => $row->ToT_talent, "teamMAPs" => $row->ToT_team, "languages" => $row->ToT_languages, "users" => $row->ToT_users, "ideal" => $row->ToT_ideal, "emails" => $row->ToT_emails);
				return $accArr;
			}

			$db->mysqlclose();
			
		}//end getAccount
		
		
		public function updateAccount($accid, $accname, $email, $serviceid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "UPDATE $this->Acctable SET accountname = '$accname', serviceid = '$serviceid', email = '$email' WHERE accid = '$accid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());

			$db->mysqlclose();
			
			return TRUE;
	
		}//end getAccount
		
		public function updateCustomLimit($accid, $customLimit) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "UPDATE $this->Acctable SET useAccLimits = '$customLimit' WHERE accid = '$accid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());

			$db->mysqlclose();
			
			return TRUE;
	
		}//end updateCustomLimit
		
		public function getAccounts() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->Acctable";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());
			
			$accounts = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$accounts[$row->accid] = array("accountname" => $row->accountname, "useAccLimits" => $row->useAccLimits);
			}//end while

			return $accounts;
			
			$db->mysqlclose();
			
		}//end getAccount
		
		public function addAccount($accountname, $email, $serviceid, $firstname, $surname, $countryid, $freetrial=false) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$accountname = $db->EscapeMe($accountname);
			$email = $db->EscapeMe($email);
			//generate the account hash. This is another unique identifier for each account, maybe use it with mass invites or fancy support tickets for each account in the future.
			$randstr = $this->genHash(14);
			$hashstr = md5($this->accid . '_' . $randstr);
			$timenow = time();
		
			$sql = "INSERT INTO $this->Acctable (accounthash,accountname,email,nr_email,serviceid,status,dbprefix,folderpath,useAccLimits,time_added,countryid) VALUES ('$hashstr', '$accountname', '$email', 'no-reply@saviio.com', '$serviceid', 1, 'sav', '_saviio', 0, '$timenow', '$countryid')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());
			
			//get the new id of the account that has just been created.
			$newaccid = mysql_insert_id();
			//create a new admin obj for adding company/user/maps etc etc :)
			if($freetrial == true) {
				//178 is a user called Saviio Global so the creator is that (as no-one is actually logged in when adding it for a free trial :P)
				$xadmObj = new Admin(155);
				$eid = $xadmObj->getEIDviaType('freetrial');
			} else {
				$xadmObj = new Admin($_SESSION['uid']);
				$eid = $xadmObj->getEIDviaType('newaccount');		
			}
			$type = 'head';
			if(!$xadmObj->addCompany("new", $accountname, $accountname, $countryid, $type, $newaccid)) {
				return FALSE;
			}
			$newcid = $_SESSION['newcompid'];
			$ulang = 'en_EN';
			$username = $email;
			//the default level is 2 which is an account "Master Admin"
			$level = 2;
			//0,1 here are the admin is 0 so it adds the god admin ID of who added them. Also 1 is just the retake count of the new user. As a master admin they bypass this anyway

			try {
				$xadmObj->addUser($username, $firstname, $surname, $newcid, $email, $countryid, $eid, $level, $ulang, 0, 1, $newaccid);
			} catch (Exception $e) {
				echo $e->getMessage();
			}
			
			//this adds a default lang of english/ger/fr into a new account
			if(!$xadmObj->updateLangPrefs('1,3,7', $newaccid)) {
				return FALSE;
			}

			$xadmObj->incrementTotals('user', $newaccid);

			return TRUE;
			
			$db->mysqlclose();
			
		}//end addAccount
		
		
		public function getServiceLevels() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->serviceLevelTable";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error());
			
			$services = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$services[$row->serviceid] = $row->serviceName;
			}//end while

			return $services;
			
			$db->mysqlclose();
			
		}//end getServiceLevels

		
}//end class

?>
