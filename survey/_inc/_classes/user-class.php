<?php

class User {
		public $uid;
		public $companyid;
		public $countryid;
		public $username;
		public $firstname;
		public $surname;
		public $password;
		public $email;
		public $adminid;
		public $level;
		public $terms;
		public $langid;
		public $status;
		public $retake;
		public $headid;
		public $accid;
		public $avatar;
		public $avatarURL;
		public $API_Access;
		public $API_Token;
		public $surveyURL;
		public $staticURL;
		public $mainURL;

		public function __construct(){
			$this->usertable = DB_PREFIX . '_users';
			$this->companiestable = DB_PREFIX . '_companies';
			$this->compshare = DB_PREFIX . '_sharecomp';
			$this->compshareaccess = DB_PREFIX . '_sharecompaccess';
			$this->levelstable = DB_PREFIX . '_userlevels';
			$this->levelaccesstable = DB_PREFIX . '_levelaccess';
			$this->MAPtable = DB_PREFIX . '_maps';
			
			$this->clustertable = 'system_clusters';
			$this->factortable = 'system_factors';
			$this->facDescTable = 'system_factordesc';
			$this->langTable = 'system_languages';
			$this->langSelectTable = 'system_langselect';
			$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'] : "http://".$_SERVER['SERVER_NAME'];
            if($url != "http://samknowhow.saviio.com" && $url != "http://samknowhow.com") {
                $this->surveyURL = "http://samknowhow.saviio.com/";
            } else {
                $this->surveyURL = $url . "/";
            }
			$this->staticURL = 'http://static.saviio.com/';
			$this->mainURL = 'http://www.saviio.com/';
		}
		
		//this checks for survey expiration via the DB.. if it finds surveys that have reached their expiration date it simply sends out a reminder.
		public function checkExpiration() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$currtime = time();
			$month = date('m', $currtime);
			$day = date('d', $currtime);
			$year = date('Y', $currtime);
			
			$timeVal = mktime ( 0, 0, 0, ( $month - 6 ), $day, $year );

			$sql = "SELECT ut.uid, ct.uid as owneruid, MAX(mt.mapid) as mapid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, MAX(mt.timefinish) as timefinish FROM $this->usertable ut, $this->companiestable ct, $this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND mt.timefinish < '$timeVal' AND mt.timefinish > 0 GROUP BY mt.uid";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$row = mysql_fetch_object($result);

			//Need to do a foreach and check the admin id then concatenate all the uid's for each admin id and send an email to each of the admins with a list of the users whose survey is about to expire!
			/*			
			$emailHTML = str_replace($tags_original, $tags_replaced, $emailobj->email_content);

			//now get the template file!
			$email_template = file_get_contents('/var/www/saviio.com/survey/_inc/email_template.php');
			$tags_original = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
			$tags_replaced = array(_('Welcome to Saviio!'), $emailHTML);

			$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);

			//start up the mail class
			$mail = new PHPMailer();
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = MAIL_HOSTNAME;
			//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->Username   = MAIL_USERNAME;
			$mail->Password   = MAIL_PASSWORD;
				
			$mail->From  = NR_EMAIL;
			$mail->FromName = ACC_NAME;
			$mail->CharSet = "UTF-8";
			//$mail->Subject = _("Login Details");
			$mail->Subject = _("Login Details");
			$mail->AltBody = _("To view the message, please use an HTML compatible email viewer"); // optional, comment out and test
			$mail->MsgHTML($emailHTML);
			$mail->AddBCC($this->email, $this->firstname . ' ' . $this->surname);
			$mail->AddAddress($email, $firstname . ' ' . $surname);

			if(!$mail->Send()) {
				throw new Exception(_('Error sending email. Please try the resend email invitation function as the user has now been added successfully.')); 
			} else {
			  RETURN TRUE;
			}
			*/

			$db->mysqlclose();
		}//end checkExpiration

		public function cidviaLocale($locale) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT countryid FROM $this->langTable WHERE locale = '$locale'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$row = mysql_fetch_object($result);
			
			RETURN $row->countryid;

			$db->mysqlclose();
		}//end cidviaLocale

		
		public function langviaLocale($locale) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->langTable WHERE locale = '$locale'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$row = mysql_fetch_object($result);
			
			RETURN $row;

			$db->mysqlclose();
		}//end getQids
		
		
		public function getLanguages($accid=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($accid == 0) {
				$sql = "SELECT * FROM $this->langTable";
			} else {
				$sql = "SELECT * FROM $this->langSelectTable lst LEFT JOIN $this->langTable lt ON lt.langid = lst.langid WHERE lst.accid = '$accid' ORDER BY lang_name ASC";
			}
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$langs = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$langs[$row->langid] = array("lang_name" => $row->lang_name, "countryid" => $row->countryid, "locale" => $row->locale);
			}//end while
			
			RETURN $langs;

			$db->mysqlclose();
		}//end getLanguages
		
		public function updateLangPrefs($langArr, $accid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$langArr = explode(',', $langArr);
			$currLangs = $this->getLanguages($accid);
			foreach($langArr as $langid) {
				if(!array_key_exists($langid, $currLangs)) {
					$sql = "INSERT INTO $this->langSelectTable (langid, accid) VALUES ('$langid', '$accid')";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error(). " user Class " . __LINE__);
				}
			}
			foreach($currLangs as $langid => $infoArr) {
				if(!in_array($langid, $langArr)) {
					$sql = "DELETE FROM $this->langSelectTable WHERE langid = '$langid' AND accid = '$accid'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error(). " user Class " . __LINE__);
				}
			}
			
			//get count of total langs used and update the account DB with the value.
			$totalLangs = count($langArr);
			$sql = "UPDATE $this->accTable SET ToT_languages = '$totalLangs' WHERE accid = '$accid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			RETURN TRUE;

			$db->mysqlclose();
		}//end getQids
			
		
		public function getLevelAccess($accessgroup, $level=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			if($level == 0) {
				$sql = "SELECT fat.accesstype, fat.accesslevel FROM $this->levelaccesstable fat WHERE levelid = '$this->level' AND accessgroup = '$accessgroup'";
			} else {
				$sql = "SELECT fat.accesstype, fat.accesslevel FROM $this->levelaccesstable fat WHERE levelid = '$level' AND accessgroup = '$accessgroup'";
			}
			
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$FeaAccess = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$FeaAccess[$row->accesstype] = $row->accesslevel;
			}//end while
			
			RETURN $FeaAccess;

			$db->mysqlclose();
		}//end getQids
		
		public function getShareAccessUID($uid, $companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT accesstype, accessgroup, allowaccess FROM $this->compshareaccess WHERE uid = '$uid' AND companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$ShareAccess = array();
			$i = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$ShareAccess[$i] = array("accesstype" => $row->accesstype, "accessgroup" => $row->accessgroup, "allowaccess" => $row->allowaccess);
				$i++;
			}//end while
		
			RETURN $ShareAccess;

			$db->mysqlclose();
		}//end getQids
		
		
		public function getAccountShareAccess($accessgroup) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$this->uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			$compids = array();
			$k = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$compids[$k] = $row->companyid;
				$k++;
			}
		
			//check to see whether any companyids have been returned!
			if(sizeof($compids) != 0) { 
				foreach($compids as $compid) {
					$sql = "SELECT sa.accesstype, sa.allowaccess FROM $this->compshareaccess sa WHERE uid = '$this->uid' AND accessgroup = '$accessgroup' AND companyid = '$compid'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error(). " user Class " . __LINE__);
						
					$shareAccess = array();
					while( $row = mysql_fetch_object( $result ) ) {
						$shareAccess[$compid][$row->accesstype] = $row->allowaccess;
					}//end while
				}
			}
			
			RETURN $shareAccess;

			$db->mysqlclose();
		}//end getQids
		
		public function getAccountShareUsers($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "SELECT ut.firstname, ut.surname, cs.uid FROM $this->compshare cs LEFT JOIN $this->usertable ut ON ut.uid=cs.uid WHERE cs.companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			$uids = array();
			$k = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$uids[$row->uid] = array("firstname" => $row->firstname, "surname" => $row->surname);
				$k++;
			}
	
			RETURN $uids;

			$db->mysqlclose();
		}//end getQids
		
		public function randPass($length = 10, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890') {
		   $charLength = strlen($chars)-1;  
		   for($i = 0; $i < $length; $i++)  {  
		   	$randomString .= $chars[mt_rand(0,$charLength)];  
		   }  
		   return $randomString;
		}
		
		public function getUserObject($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->usertable WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			
			$row = mysql_fetch_object( $result );
			$out = $row;
			
			$db->mysqlclose();
			return $row;
		}//end getUser
		
		
		public function getUser($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->usertable WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			if(mysql_num_rows($result) > 0) {
				$row = mysql_fetch_object( $result );
	
				$this->uid = $uid;
				$this->companyid = $row->companyid;
				$this->countryid = $row->countryid;
				$this->username = $row->username;
				$this->firstname = $row->firstname;
				$this->surname = $row->surname;
				$this->password = $row->password;
				$this->email = $row->email;
				$this->adminid = $row->adminid;
				$this->level = $row->level;
				$this->terms = $row->terms;
				$this->langid = $row->langid;
				$this->status = $row->status;
				$this->retake = $row->retake;
				$this->accid = $row->accid;
				$this->avatar = $row->avatarType;
				$this->avatarURL = $row->avatarURL;
			 	$this->API_Access = $row->API_Access;
				$this->API_Token = $row->API_Token;
				
				$db->mysqlclose();
			return true;
			} else {
				return false;
			}
			
		}//end getUser
		
		
		public function getUserVID($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT firstname, surname, email, username, salutation FROM $this->usertable WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			
			$row = mysql_fetch_object( $result );
			
			return $row;
			
			$db->mysqlclose();
			
		}//end getUser
		
		
		public function getUservUN($username) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->usertable WHERE username = '$username'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			
			$row = mysql_fetch_object( $result );

			$this->uid = $row->uid;
			$this->companyid = $row->companyid;
			$this->countryid = $row->countryid;
			$this->username = $row->username;
			$this->firstname = $row->firstname;
			$this->surname = $row->surname;
			$this->password = $row->password;
			$this->email = $row->email;
			$this->level = $row->level;
			$this->terms = $row->terms;
			$this->adminid = $row->adminid;
			$this->langid = $row->langid;
			$this->status = $row->status;
			$this->retake = $row->retake;
			$this->accid = $row->accid;
			$this->avatar = $row->avatarType;
			$this->avatarURL = $row->avatarURL;
			$this->API_Access = $row->API_Access;
			$this->API_Token = $row->API_Token;
			
			$db->mysqlclose();
			
		}//end getUser
		

	  public function updateDetails($userid, $firstname, $surname, $email, $password, $adminChange=false, $authcode=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$timenow = time();
			if($password == '') {
				$sql = "UPDATE $this->usertable SET firstname = '$firstname', surname = '$surname', email = '$email' WHERE uid = '$userid'";
			} else {

				$md5pass = md5($password);
				if($authcode == 0 || $authcode==0) {			
					$sql = "UPDATE $this->usertable SET firstname = '$firstname', surname = '$surname', email = '$email', password = '$md5pass' WHERE uid = '$userid'";
				} else {
					$sql = "UPDATE $this->usertable SET firstname = '$firstname', surname = '$surname', email = '$email', password = '$md5pass', userhash = '$authcode', authed='0', authedSent = '$timenow', authed = 0 WHERE uid = '$userid'";
				}
				if($adminChange == false) {
					$_SESSION['passw'] = $password;
					$_SESSION['dashpass'] = $password;
				}
			}
			
			//echo $sql;

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateDetails
		
		public function getCompany($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * FROM $this->companiestable left join system_accounts as acc on acc.accid = " . $this->companiestable . ".accid WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$row = mysql_fetch_object( $result );
	
			RETURN $row;

			$db->mysqlclose();
		}//end getQids
		
	 public function updateAvatar($avaType, $avaURL, $uid=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($uid == 0) {
				$newuid = $this->uid;
			} else {
				$newuid = $uid;
			}

			$sql = "UPDATE $this->usertable SET avatarType = '$avaType', avatarURL = '$avaURL' WHERE uid = '$newuid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateUser
		
		
		public function setLastLogin($timenow) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "UPDATE $this->usertable SET lastlogin = '$timenow' WHERE uid = '$this->uid'";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			RETURN true;
			
			$db->mysqlclose();
			
		}//end setLastLogin

		public function getLastLogin() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT lastlogin FROM $this->usertable WHERE uid = '$this->uid'";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
				
			$row = mysql_fetch_object( $result );

			RETURN $row->lastlogin;
			
			$db->mysqlclose();
			
		}//end setLastLogin
				
		public function resetPassword($userid, $password) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "UPDATE $this->usertable SET password = '$password' WHERE uid = '$userid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			
			RETURN true;
			
			$db->mysqlclose();
		}//end resetPassword
		
		public function resetPasswordVUN($username) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$username = $db->EscapeMe($username);
			
			//its classed as false because checkuser function checks if the username exists for adding a new user. If it does then it returns false. Hence in this instance a returning false value means the user actually exists!
			if(!$this->checkUser($username)) {
		
				$this->getUservUN($username);
				$newpass = $this->randPass(14);
				$hashstr = md5($username . $newpass);
				$timenow = time();
				$sql = "UPDATE $this->usertable SET authedSent = '$timenow', userhash = '$hashstr', resetRequest = 1 WHERE username = '$username'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error(). " user Class " . __LINE__);
				$mail = new PHPMailer();

				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = MAIL_HOSTNAME; // SMTP server
				//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = MAIL_USERNAME; // SMTP account username
				$mail->Password   = MAIL_PASSWORD;        // SMTP account password
		
				$body .= _("Thank you for your request to reset your password. If you haven't requested this then please ignore this email. Your password will not be changed.") . '<br /><br />';
				$body .= '<strong>' . _('If you have requested the password reset then please follow the link below to reset your password and choose a new one.') . '</strong><br />';
				
				$body .= _('Reset Password Link') . ': <a href="' . $this->surveyURL . 'saviio/auth/' . $hashstr . '">' . _("Reset Password Link") . '</a><br /><br />'  . "\n";
				$body .= _('Thanks, Team Saviio') . '<br />'  . "\n";
				
				$mail->From  = NR_EMAIL;
				$mail->FromName = ACC_NAME;
				$mail->Subject = _("Reset Password Request");
				$mail->AltBody = _("To view the message, please use an HTML compatible email viewer."); // optional, comment out and test
				$mail->MsgHTML($body);
				$mail->AddAddress($this->email, $this->firstname . ' ' . $this->surname);

				if(!$mail->Send()) {
				  RETURN FALSE;
				} else {
				 RETURN true;
				}

			} else {
				RETURN false;
			}
			
			$db->mysqlclose();
		}//end resetPassword
	
		
		public function getHeadID($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT headid FROM $this->companiestable WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			
			$row = mysql_fetch_object( $result );

			$this->headid = $row->headid;

			$db->mysqlclose();
			
		}//end getUser
		
		public function getUserLevel($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT level FROM $this->usertable WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);
			
			$row = mysql_fetch_object( $result );

			return $row->level;

			$db->mysqlclose();
			
		}//end getUser
		
		public function updateRetake( $retake ) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "UPDATE $this->usertable set retake = '$retake' WHERE uid = '$this->uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die( "Invalid query Line 85" );
			
			RETURN TRUE;

			$db->mysqlclose();
		}//end getLeftMenuIDs
		
		public function checkLogin( $username, $pass ) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$username = $db->EscapeMe($username);
			$sql = "SELECT * from $this->usertable WHERE username = '$username' AND password = '$pass' AND status = 1";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die( "Invalid query Line 460" );
				
			$row = mysql_fetch_object( $result );
			$count = mysql_num_rows($result);
			
			if($count == 1) {
				$this->uid = $row->uid;
				RETURN TRUE;
			} else {
				RETURN FALSE;
			}

			$db->mysqlclose();
		}//end getLeftMenuIDs
		
		
		public function checkUser($username) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$username = $db->EscapeMe($username);
			
			$sql = "SELECT firstname FROM $this->usertable WHERE username = '$username'";
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die( "Invalid query Line 268" );
				
			$row = mysql_fetch_object( $result );
			$count = mysql_num_rows($result);
			
			if($count == 1) {
				RETURN FALSE;
			} else {
				RETURN TRUE;
			}

			$db->mysqlclose();
		}//end checkUser
		
		public function checkReadUpdate($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT terms FROM $this->usertable WHERE uid = '$uid'";
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die( "Invalid query Line 422" );
				
			$row = mysql_fetch_object( $result );
			
			if($row->terms == 1 || $this->level > 5 ) {
				RETURN FALSE;
			} else {
				RETURN TRUE;
			}

			$db->mysqlclose();
		}//end checkReadUpdate
		
		public function setReadUpdate($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "UPDATE $this->usertable SET terms = 1 WHERE uid = '$uid'";
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die( "Invalid query Line 445" );
			
			RETURN TRUE;

			$db->mysqlclose();
		}//end setReadUpdate
		
		
		/********************************************************************************************
		*																																														*
		*																					API FUNCTIONS																			*
		*																																														*
		********************************************************************************************/
		
		public function regenToken($uid=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($uid == 0) {
				$curr_uid = $this->uid;
			} else {
				$curr_uid = $uid;
			}

			$newToken = sha1(microtime());
			$sql = "UPDATE $this->usertable SET API_Token = '$newToken' WHERE uid = '$curr_uid'";
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error(). " user Class " . __LINE__);

			return $newToken;

			$db->mysqlclose();
			
		}

}//end class

?>
