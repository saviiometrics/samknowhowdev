<?php

class Question {
		public $qid;
		public $question;
		public $live;
		public $timecode;

		public function __construct(){
			$this->questiontable = 'system_questions';
			$this->responsetable = 'system_responses';
			$this->factortable = 'system_factors';
		}
		
		public function getQids( $live = 1 ) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT qid from $this->questiontable WHERE live = '$live'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " question Class " . __LINE__);
				
			$i = 0;
			$qids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$qids[$i] = $row->qid;
				$i = $i + 1;
			}//end while
			
			RETURN $qids;

			$db->mysqlclose();
		}//end getQids
		
		
	 public function getSTResponses( $live = 1 ) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * FROM $this->responsetable";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " question Class " . __LINE__);
				
			$i = 0;
			$qids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$qids[$row->qid][$row->alignment] = $row->response;
				$i = $i + 1;
			}//end while
			
			RETURN $qids;

			$db->mysqlclose();
		}//end getQids
		
		public function getResponses($qid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT response, alignment, fid FROM $this->responsetable WHERE qid = '$qid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " question Class " . __LINE__);
				
			$i = 0;
			$resids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$resids[$row->alignment] = $row->response;
				//$resids['fid'][$row->alignment] = $row->fid;
			}//end while
			
			RETURN $resids;
			
			$db->mysqlclose();
			
		}//end getResponses
		
		
		public function getFactorGroup($qid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT $this->factortable.factor_group FROM $this->responsetable LEFT JOIN $this->factortable ON $this->responsetable.fid=$this->factortable.fid WHERE $this->responsetable.qid = '$qid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " question Class " . __LINE__);
			
			$row = mysql_fetch_object($result);
			$this->factor_group = $row->factor_group;
			
			return $this->factor_group;
			
			$db->mysqlclose();
			
		}//end getFactorGroup
	
	
}//end class

?>
