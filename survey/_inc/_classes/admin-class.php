<?php

class Admin extends User {
		public $moduleid;
		public $modulename;
		public $mod_catid;
		public $module_description;
		public $link;
		public $maccessid;
		public $limitstable;

		public function __construct($uid){
			parent::__construct();
			$this->adminaccess = DB_PREFIX . '_moduleaccess';
			$this->companiestable = DB_PREFIX . '_companies';
			$this->TMAPtable = DB_PREFIX . '_TMAPs';
			$this->emailtable = DB_PREFIX . '_emails';
			$this->logtable = DB_PREFIX . '_log';
			$this->filestable = DB_PREFIX . '_files';
			$this->bugtable = DB_PREFIX . '_bugs';
			$this->sharerequest = DB_PREFIX . '_sharerequest';
			$this->surveytable = DB_PREFIX . '_survey';
			
			//non customisable
			$this->adminmmoduletable = 'system_adminmodules';
			$this->admincats = 'system_admincats';
			$this->faqtable = 'system_faq';
			$this->countriestable = 'system_countries';
			$this->servicetable = 'system_servicelevels';
			$this->getUser($uid);
			$this->getHeadID($this->companyid);
			$this->accTable = 'system_accounts';
		}
	
		public function getLimits($accid=0, $useLimits=0, $service=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($accid == 0) {
				$useAccid = $this->accid;
			} else {
				$useAccid = $accid;
			}
			
			if($useLimits == 0) {
				$limits = USE_ACC_LIMITS;
			} else {
				$limits = $useLimits;
			}
			
			if($service == 0) {
				$serviceid = SERVICE_ID;
			} else {
				$serviceid = $service;
			}

			if($limits == 1) {
				$sql = "SELECT * FROM system_accountlimits WHERE accid = '$useAccid'";
			} else {
				$sql = "SELECT * FROM system_servicelimits WHERE serviceid = '$serviceid'";
			}

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$num_rows = mysql_num_rows($result);
			if($num_rows == 0) {
				return false;
			}
			$LimitsArr = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$LimitsArr[$row->limitIdent] = $row->currLimit;
			}//end while
	
			return $LimitsArr;
					
			$db->mysqlclose();
			
		}//end getLevels
		
		public function updateLimits($accid, $useLimits, $limitsArr) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
	
			if($useLimits == 1) {
				$currLimits = $this->getLimits($accid, $useLimits);
				$currLimits == false ? $editLimits = false : $editLimits = true;
			}

			if($editLimits == true) {
				foreach($limitsArr as $limitKey => $limitval) {
					$sql = "UPDATE system_accountlimits sa SET sa.currLimit = '$limitval' WHERE sa.limitIdent = '$limitKey' AND sa.accid = '$accid'";
					$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
				}
			} else {
				if ($useLimits == 1) {
					foreach($limitsArr as $limitKey => $limitval) {
						$sql = "INSERT INTO system_accountlimits (accid, limitIdent, currLimit) VALUES ('$accid', '$limitKey', '$limitval')";
						$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " Admin Class " . __LINE__) ;
					}
				}
			}

			return TRUE;
					
			$db->mysqlclose();
			
		}//end getLevels
		
		public function incrementTotals($type, $accid=0, $decrease=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($accid == 0) {
				$accid = $this->accid;
			}
			
			switch($type) {
				case 'user':
				$dbstring = 'ToT_users';
				break;
				case 'map':
				$dbstring = 'ToT_maps';
				break;
				case 'account':
				$dbstring = 'ToT_accounts';
				break;
				case 'subaccount':
				$dbstring = 'ToT_subaccs';
				break;
				case 'team':
				$dbstring = 'ToT_team';
				break;
				case 'talent':
				$dbstring = 'ToT_talent';
				break;
				case 'language':
				$dbstring = 'ToT_languages';
				break;
				case 'ideal':
				$dbstring = 'ToT_ideal';
				break;
				case 'email':
				$dbstring = 'ToT_emails';
				break;
				case 'users':
				$dbstring = 'ToT_users';
				break;
				case 'maps':
				$dbstring = 'ToT_maps';
				break;
				case 'accounts':
				$dbstring = 'ToT_accounts';
				break;
				case 'subaccounts':
				$dbstring = 'ToT_subaccs';
				break;
				case 'teamMAPs':
				$dbstring = 'ToT_team';
				break;
				case 'talentMAPs':
				$dbstring = 'ToT_talent';
				break;
				case 'languages':
				$dbstring = 'ToT_languages';
				break;
				case 'idealMAPs':
				$dbstring = 'ToT_ideal';
				break;
				case 'emails':
				$dbstring = 'ToT_emails';
				break;
			}
			
			if($decrease == 1) {
				$sql = "UPDATE $this->accTable SET $dbstring = $dbstring -1 WHERE accid = '$accid'";
			} else {
				$sql = "UPDATE $this->accTable SET $dbstring = $dbstring +1 WHERE accid = '$accid'";
			}

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
	
			return TRUE;
					
			$db->mysqlclose();
			
		}//end getLevels

		
		/*************************************************************
		************************* LEVELS *****************************
		*************************************************************/
		
		public function addLevel($levelname, $minAccess, $levelObj, $modids) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$title = $db->EscapeMe($title);
			$comments = $db->EscapeMe($comments);
			
			$timenow = time();
			$sql = "INSERT INTO $this->levelstable (levelname, accid, levelAccess) VALUES ('$levelname', 'ALL', '$minAccess')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$levelid = mysql_insert_id();
			$uid = $this->uid;
			
			$sql = "INSERT INTO $this->levelaccesstable (levelid, uid, accesstype, accessgroup, accesslevel) VALUES ";
			foreach($levelObj as $infoObj) {
				foreach($infoObj as $accessgroup => $levelarr) {
					if(is_array($levelarr)) {
						foreach($levelarr as $accesstype => $accesslevel) {
							//this is because minifying JS doesn't like delete as a key, so changed it to deleteMe so have to change it back to delete here before adding it into the database!
							if($accesstype == 'deleteMe') {
								$accesstype = 'delete';
							}
							$accesstype = strtoupper($accesstype);
							$sql .= "('$levelid', '$uid', '$accesstype', '$accessgroup', '$accesslevel'),";
						}
					}
				}
			}

			
			$sql = substr($sql, 0, -1);

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
		  $modids = explode(',', $modids);
		  $sql = "INSERT INTO $this->adminaccess (moduleid, levelid) VALUES ";
			foreach($modids as $modid) {
				$sql .= "('$modid', '$levelid'),";
			}
			
			$sql = substr($sql, 0, -1);
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			return TRUE;

			$db->mysqlclose();
		}//end addLevel
		
		public function getLevels($accid=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$acctype = 'ALL';
			$sql = "SELECT * FROM $this->levelstable WHERE accid = '$acctype' ORDER BY levelAccess";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$levelsArr = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$levelsArr[$row->levelid] = array("levelname" => $row->levelname, "levelAccess" => $row->levelAccess);
			}//end while
	
			return $levelsArr;		
					
			$db->mysqlclose();
		}//end getLevels
				
		/************************** IDEAL MAP ************************/
		
		public function getClusterResponses($clusterid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "SELECT sf.clusterid, sr.response, sf.factor_group, sf.factor_text, sr.qid, sr.alignment, sr.fid FROM $this->factortable sf LEFT JOIN system_responses sr ON sf.fid=sr.fid WHERE sf.clusterid = '$clusterid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;		

			$response_arr = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$response_arr[$row->factor_group][$row->qid][$row->alignment] = array("fid" => $row->fid, "response" => $row->response, "factor_text" => $row->factor_text);
			}//end while
	
			return $response_arr;
			
			$db->mysqlclose();
		}//end getLocale
		
		
		/*************************************************************
		************************* LOCALE *****************************
		*************************************************************/
		
		public function getLocale($uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "SELECT langid FROM $this->usertable WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );
			return $row->langid;

			$db->mysqlclose();
		}//end getLocale
		
		
		public function setUserLocale($uid, $locale) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "UPDATE $this->usertable SET langid = '$locale' WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$db->mysqlclose();
		}//end addCompany
		
		public function addShareRequest($companyids, $comments) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$compids = explode(',',$companyids);
			//get owner id from companyid here (or have it sent through?
			$timenow = time();
			$status = 1;
			
			//SEND EMAIL
			$mail = new PHPMailer();

			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = MAIL_HOSTNAME; // SMTP server
			//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->Username   = MAIL_USERNAME; // SMTP account username
			$mail->Password   = MAIL_PASSWORD;        // SMTP account password
			
			foreach($compids as $compid) {
				$idarr = explode('-',$compid);
				$sql = "INSERT INTO $this->sharerequest (companyid, request_uid, owner_uid, comments, status, request_time, display) VALUES ('$idarr[0]', '$this->uid', '$idarr[1]', '$comments', '$status', '$timenow', 1)";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
				
				$ownerobj = $this->getUserVID($idarr[1]);
				$compobj = $this->getCompany($compid);
				
				//send Email
				$emailHTML = '=====================================================================<br /><strong>This Email is sent from an unmanned email account.<br /> ******** Please do not respond directly to this email *******</strong><br />=====================================================================<br /><br />';
				$emailHTML .= 'You Have a new request from ' . $this->firstname . ' ' . $this->surname . ' to share your account <strong>' . $compobj->companyname . '</strong>:<br /><br />';
				$emailHTML .= '<strong>Requester Comments:</strong><br />' . $comments . '<br /><br />';
				$emailHTML .= 'To approve or deny the request please login to ' . ACC_NAME . ' and go to the accounts page in the admin section. You can then click on show requests to see all of your outstanding requests that need actioning.<br />';
			  $emailHTML .= '<br />Thank you<br /><br />' . ACC_NAME . ' Team';
				$mail->From  = NR_EMAIL;
				$mail->FromName = ACC_NAME;
				$mail->CharSet = "UTF-8";
				$mail->Subject = 'Account Share Request';
				$mail->AltBody = _("To view the message, please use an HTML compatible email viewer"); // optional, comment out and test
				$mail->MsgHTML($emailHTML);
				$mail->AddAddress($ownerobj->email, $ownerobj->firstname . ' ' . $ownerobj->surname);
	
				$mail->Send();
			}
			
			return TRUE;

			$db->mysqlclose();
		}//end addShareRequest
		
		
		public function getShareRequests($reqtype) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($reqtype == 'owner') {
				$sql = "SELECT sr.srid, sr.request_uid, ut.firstname, ut.surname, ct.companyname, ct.companyid, sr.status, sr.request_time, sr.comments FROM $this->sharerequest sr LEFT JOIN $this->usertable ut ON sr.request_uid=ut.uid LEFT JOIN $this->companiestable ct ON sr.companyid=ct.companyid WHERE sr.owner_uid = '$this->uid' AND sr.display = 1";
			} else if($reqtype == 'requested') {
				$sql = "SELECT sr.srid, sr.request_uid, ut.firstname, ut.surname, ct.companyname, ct.companyid, sr.status, sr.request_time, sr.comments FROM $this->sharerequest sr LEFT JOIN $this->usertable ut ON sr.owner_uid=ut.uid LEFT JOIN $this->companiestable ct ON sr.companyid=ct.companyid WHERE sr.request_uid = '$this->uid' AND (sr.display = 1 OR sr.display = 2)";
			}
		
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			$requests_arr = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$requests_arr[$row->srid] = array('firstname' => $row->firstname, 'surname' => $row->surname, 'companyname' => $row->companyname, 'status' => $row->status, 'request_time' => $row->request_time, 'comments' => $row->comments, 'requid' => $row->request_uid, 'reqcid' => $row->companyid);
			}//end while
			
			return $requests_arr;

			$db->mysqlclose();
		}//end getShareRequests
		

		public function updateShareRequest($compid, $ownerid, $requid, $status) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "UPDATE $this->sharerequest SET status = '$status', display = 2 WHERE owner_uid = '$ownerid' AND request_uid = '$requid' AND companyid = '$compid'";
		
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			return TRUE;

			$db->mysqlclose();
		}//end updateShareRequest
		
		
		public function declineShareRequest($srid, $status, $display) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "UPDATE $this->sharerequest SET status = '$status', display = '$display' WHERE srid = '$srid'";
		
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			return TRUE;

			$db->mysqlclose();
		}//end declineShareRequest
		
		public function addCompanyShare($companyid, $rightsArray) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$timenow = time();
			foreach($rightsArray as $rights) {
				$sql = "INSERT INTO $this->compshare (companyid, uid, dateshared) VALUES ('$companyid', '$rights[userid]', '$timenow')";
					$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
				
				$i=0;
				$sql = '';
				foreach($rights as $itemident => $itemarr) {
					if($itemident == 'userid') {
						continue;
					}
					foreach($itemarr as $itemkey => $itemvalue) {
						$itemkey = strtoupper($itemkey);
						$sql = "INSERT INTO $this->compshareaccess (uid, companyid, accesstype, accessgroup, allowaccess) VALUES ('$rights[userid]', '$companyid', '$itemkey', '$itemident', '$itemvalue')";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " Admin Class " . __LINE__) ;
					}
					$i++;	
				}
			}
			
			return TRUE;

			$db->mysqlclose();
		}//end addCompanyShare
		
		public function deleteCompanyShare($companyid, $uid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "DELETE FROM $this->compshare WHERE companyid = '$companyid' AND uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$sql = "DELETE FROM $this->compshareaccess WHERE companyid = '$companyid' AND uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			return TRUE;

			$db->mysqlclose();
		}//end addCompany
		
		public function updateCompanyShare($companyid, $rightsArray) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$timenow = time();
			foreach($rightsArray as $rights) {
				foreach($rights as $itemident => $itemarr) {
					if($itemident == 'userid') {
						continue;
					}
					foreach($itemarr as $itemkey => $itemvalue) {
						$itemkey = strtoupper($itemkey);
						
						$sql = "UPDATE $this->compshareaccess SET allowaccess = '$itemvalue' WHERE uid = '$rights[userid]' AND companyid = '$companyid' AND accesstype = '$itemkey' AND accessgroup = '$itemident'";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " Admin Class " . __LINE__) ;

					}
				}
			}
			
			return TRUE;

			$db->mysqlclose();
		}//end addCompany
		
		public function submitFeedback($title, $comments, $priority) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$title = $db->EscapeMe($title);
			$comments = $db->EscapeMe($comments);
			
			$timenow = time();
			$sql = "INSERT INTO $this->bugtable (uid, title, comments, priority, dateadded, status) VALUES ('$this->uid', '$title', '$comments', '$priority', '$timenow', 'new')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			return TRUE;

			$db->mysqlclose();
		}//end addCompany
		
		public function getFeedback() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$sql = "SELECT bugid, title, uid, comments, priority, dateadded, adminid, status FROM $this->bugtable ORDER BY bugid DESC";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$feedbackarr = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$feedbackarr[$row->bugid]['title'] = $row->title;
				$feedbackarr[$row->bugid]['uid'] = $row->uid;
				$feedbackarr[$row->bugid]['comments'] = $row->comments;
				$feedbackarr[$row->bugid]['priority'] = $row->priority;
				$feedbackarr[$row->bugid]['dateadded'] = $row->dateadded;
				$feedbackarr[$row->bugid]['adminid'] = $row->adminid;
				$feedbackarr[$row->bugid]['status'] = $row->status;
			}//end while
			
			return $feedbackarr;

			$db->mysqlclose();
		}//end addCompany

		public function increaseRetake($userid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "UPDATE $this->usertable SET retake = 1 WHERE uid = '$userid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			RETURN true;
			
			$db->mysqlclose();
			
		}//end increaseRetake
		
		
	  public function updateUser($userid, $firstname, $surname, $companyid, $email, $countryid, $level) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$currlevel = $this->getUserLevel($userid);
			
			$firstname = $db->EscapeMe($firstname);
			$surname = $db->EscapeMe($surname);
			$email = $db->EscapeMe($email);
			
			//update companyid on MAPTable too!
			$sql = "UPDATE $this->MAPtable SET companyid = '$companyid' WHERE uid = '$userid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$sql = "UPDATE $this->usertable SET firstname = '$firstname', surname = '$surname', countryid = '$countryid', companyid = '$companyid', email = '$email', level = '$level' WHERE uid = '$userid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateUser
		
		public function getAdminCats() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
		
			$sql = "SELECT * from $this->admincats";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$mod_catids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$mod_catids[$row->mod_catid] = $row->categoryname;
				$i = $i + 1;
			}//end while
			
			RETURN $mod_catids;

			$db->mysqlclose();
		}//end getQids
		
		
		public function getModuleAccess($level = 0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			if($level == 0) {
				$levelnow = $this->level;
			} else {
				$levelnow = $level;
			}

			$sql = "SELECT moduleid FROM $this->adminaccess WHERE levelid = '$levelnow'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			$i = 0;
			$modids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$modids[$i] = $row->moduleid;
				$i = $i + 1;
			}//end while
			
			RETURN $modids;

			$db->mysqlclose();
		}//end getQids


		public function getModules($ACSmodules) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$ACSmoduleSTR = implode(',', $ACSmodules);
			$sql = "SELECT * from $this->adminmmoduletable WHERE moduleid IN ($ACSmoduleSTR)";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$modids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$modids[$row->mod_catid][$row->moduleid]['modulename'] = $row->module_name;
				$modids[$row->mod_catid][$row->moduleid]['link'] = $row->link;
				$modids[$row->mod_catid][$row->moduleid]['desc'] = $row->module_description;
				$i = $i + 1;
			}//end while
			
			RETURN $modids;

			$db->mysqlclose();
		}//end getQids
		
		public function getMid($linkstr) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$sql = "SELECT moduleid FROM $this->adminmmoduletable WHERE link = '$linkstr'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$row = mysql_fetch_object( $result );
			RETURN $row->moduleid;

			$db->mysqlclose();
		}//end getQids


		public function getALLModules() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * from $this->adminmmoduletable";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$modids = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$modids[$row->moduleid] = array('module_name' => $row->module_name, 'link' => $row->link, 'desc' => $row->module_description);
				$i = $i + 1;
			}//end while
			
			RETURN $modids;

			$db->mysqlclose();
		}//end getQids

				
		public function updateAdminAccess($modids, $adminuid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$currmodids = $this->getModuleAccess($adminuid);
			$modids = explode(',', $modids);
			foreach($modids as $key => $modid) {
				$modid = substr($modid, 6);
				$modids[$key] = $modid;
				if(!in_array($modid, $currmodids)) {
					$sql = "INSERT INTO $this->adminaccess (moduleid, uid) VALUES ('$modid', '$adminuid')";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " Admin Class " . __LINE__) ;
				}
			}

			foreach($currmodids as $currmodid) {
				if(!in_array($currmodid, $modids)) {
					$sql = "DELETE FROM $this->adminaccess WHERE moduleid = '$currmodid' AND uid = '$adminuid'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " Admin Class " . __LINE__) ;
				}
			}

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateAdminAccess
		
		
		/*********************************************************************************************/
		/*************************************** COMPANIES *******************************************/
		/*********************************************************************************************/
				
		public function getCompanies($type="all", $showall=false) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$superAccess = $this->getLevelAccess('admin');
			//god admin tag means you can see all the companies not limited to a particular account id! Everyone else sees only those companies associated with their account (even well viewing all for share)
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid ORDER BY ct.companyname ASC";
			} else {
				if($showall == true) {
					$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
				} else {
					$access = $this->getLevelAccess('company');
					switch($access['VIEW']) {
						case 'ALL':
						if($type == 'head') {
							$sql = "SELECT ct.companyid, ct.companyname, ct.description, ct.countryid, ct.type, ct.uid, ct.headid, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.type = '$type' AND ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						} else {
							$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						}
						break;
						case 'CREATED':
						if($type == 'head') {
							$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.uid = '$this->uid' AND ct.type = '$type' AND ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						} else {
							$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.uid = '$this->uid' AND ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						}
						break;
						case 'MA':
						if($type == 'head') {
							$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.headid = '$this->headid' AND ct.type = '$type'  AND ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						} else {
							$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.headid = '$this->headid' AND ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						}
						break;
						case 'SA':
						$sql = "SELECT ct.*, ut.firstname, ut.surname from $this->companiestable ct JOIN $this->usertable ut ON ct.uid=ut.uid WHERE ct.companyid = '$this->companyid' AND ct.accid = '$this->accid' ORDER BY ct.companyname ASC";
						break;
					}
				}
			}

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$companies = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$companies[$row->companyid] = array('companyname' => $row->companyname, 'description' => $row->description, 'countryid' => $row->countryid, 'type' => $row->type, 'uid' => $row->uid, 'headid' => $row->headid, 'fullname' => $row->firstname . ' ' . $row->surname, 'shared' => false);
				$i = $i + 1;
			}//end while
			
			/************************************************************************
			*																																				*
			*												ACCOUNT SHARED ACCOUNTS HERE										*
			*																																				*
			************************************************************************/
			
			//same as for maps, but this time it just checks for viewing rights (here anyway) it checks for editing and adding and shit from the adminobj / website as that is controlled there this is just the list!
			
			if($showall == false) {
				$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$this->uid'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
	
				$compids = array();
				$k = 0;
				while( $row = mysql_fetch_object( $result ) ) {
					$compids[$k] = $row->companyid;
					$k++;
				}
				
				//check to see whether anyw companyids have been returned!
				if(sizeof($compids) != 0) { 
					foreach($compids as $compid) {
					$sql = "SELECT allowaccess FROM $this->compshareaccess WHERE uid = '$this->uid' AND companyid = '$compid' AND accesstype = 'VIEW' AND accessgroup = 'comp'";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " Admin Class " . __LINE__) ;
							
						$row = mysql_fetch_object( $result );
						//if they are allowed to view the MAPs add em to the list!
						if($row->allowaccess == 'TRUE') {		
							$sql = "SELECT * from $this->companiestable WHERE companyid = '$compid' ORDER BY companyname ASC";
							$result = mysql_query( $sql, $db->mySQLConnection )
								or die(mysql_error() . " Admin Class " . __LINE__) ;
		
							while( $row = mysql_fetch_object( $result ) ) {
								if(!array_key_exists($row->companyid, $companies)) {
									$companies[$row->companyid] = array('companyname' => $row->companyname, 'description' => $row->description, 'countryid' => $row->countryid, 'type' => $row->type, 'uid' => $row->uid, 'headid' => $row->headid, 'shared' => true);
								}
							}//end while
						}
					}
				}
			}//only do this if showall isnt set
			
			RETURN $companies;

			$db->mysqlclose();
		}//end getQids

		public function getAccountName($accountID) {
			if($accountID == 0) {
				$accountID = $this->accid;
			}
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			$sql = sprintf("SELECT accountname FROM system_accounts WHERE accid = %s", $accountID);
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$row = mysql_fetch_object( $result );
			return $row->accountname;
		}

		public function addCompany($headid, $cname, $cdesc, $cid, $type, $accid=0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$cname = $db->EscapeMe($cname);
			$cdesc = $db->EscapeMe($cdesc);
			$cid = $db->EscapeMe($cid);
			
			if($accid == 0) {
				$curraccid = $this->accid;
			} else {
				$curraccid = $accid;
			}
			
			if($headid == "new") {
				$sql = "INSERT INTO $this->companiestable (headid, accid, uid, companyname, description, countryid, type) VALUES (0, '$curraccid', '$this->uid', '$cname', '$cdesc', '$cid', '$type')";
			} else {
				$sql = "INSERT INTO $this->companiestable (headid, accid, uid, companyname, description, countryid, type) VALUES ('$headid', '$curraccid', '$this->uid', '$cname', '$cdesc', '$cid', '$type')";
			}
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$newcompid = mysql_insert_id();
			$_SESSION['newcompid'] = $newcompid;
			if($headid == "new") {
				//increment totals
				if($accid == 0) {
					$this->incrementTotals('account');
				} else {
					$this->incrementTotals('account', $accid);
				}
				$sql = "UPDATE $this->companiestable SET headid = '$newcompid' WHERE companyid = '$newcompid'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
			} else {
				//increment subaccount totals
				if($accid == 0) {
					$this->incrementTotals('subaccount');
				} else {
					$this->incrementTotals('subaccount', $accid);
				}
			}
			
			return TRUE;

			$db->mysqlclose();
		}//end addCompany
		
		
		public function getCountries() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT * from $this->countriestable ORDER BY Name ASC";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$countries = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$countries[$row->countryid] = $row->Name;
				$i = $i + 1;
			}//end while
			
			RETURN $countries;

			$db->mysqlclose();
		}//end getQids
		

		public function getTotalUsers($active=false) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$level = 9999;
			if($active == false) {
				$activeme = '';
			} elseif ($active == 1 || $active == 0) {
				$activeme = "AND status = '$active'";
			}
			
			$superAccess = $this->getLevelAccess('admin');
			$access = $this->getLevelAccess('user');
			
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT uid FROM $this->usertable WHERE level < $level $activeme";
			} else {
				switch($access['VIEW']) {
					case 'ALL':
					$sql = "SELECT uid FROM $this->usertable ut WHERE level < $level AND ut.accid = '$this->accid' $activeme";
					break;
					case 'CREATED':
					$sql = "SELECT ut.uid, ct.uid as owneruid FROM $this->usertable ut LEFT JOIN $this->companiestable ct ON ut.companyid=ct.companyid WHERE ut.adminid = '$this->uid' OR ct.uid = '$this->uid' AND ut.accid = '$this->accid' $activeme";
					break;
					case 'MA':
					$sql = "SELECT ct.headid, ut.uid FROM $this->usertable ut LEFT JOIN $this->companiestable ct ON ut.companyid=ct.companyid WHERE ct.headid = '$this->headid' AND ut.accid = '$this->accid' $activeme";
					break;
					case 'SA':
					$sql = "SELECT uid FROM $this->usertable ut WHERE ut.companyid = '$this->companyid' AND ut.accid = '$this->accid' $activeme";
					break;
				}
			}

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$users = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$users[$row->uid] = $row->uid;
				$i = $i + 1;
			}//end while
			
			
			if($superAccess['GODADMIN'] != 'TRUE' && $access['VIEW'] != 'ALL') {
				//NOW COUNT ONES WHICH ARE SHARED!
				$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$this->uid'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
	
				$compids = array();
				$k = 0;
				while( $row = mysql_fetch_object( $result ) ) {
					$compids[$k] = $row->companyid;
					$k++;
				}
				
				//check to see whether any companyids have been returned!
				if(sizeof($compids) != 0) { 
					foreach($compids as $compid) {
						$sql = "SELECT allowaccess FROM $this->compshareaccess WHERE uid = '$this->uid' AND companyid = '$compid' AND accesstype = 'VIEW' AND accessgroup = 'user' GROUP BY allowaccess";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " Admin Class " . __LINE__) ;
	
						$row = mysql_fetch_object( $result );
						//if they are allowed to view the MAPs add em to the list!
						if($row->allowaccess == 'TRUE') {		
							$sql = "SELECT ut.uid FROM $this->usertable ut WHERE ut.companyid = '$compid' AND ut.level <= '99'";
							$result = mysql_query( $sql, $db->mySQLConnection )
								or die(mysql_error() . " Admin Class " . __LINE__) ;
				
							while( $row = mysql_fetch_object( $result ) ) {
								$users[$row->uid] = $row->uid;
								$i = $i + 1;
							}//end while
						}
					}
				}
			}
			//count companies!
			$count = count($users);
			return $count;
		}
		
		public function exportAll() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}			
			
			$level = 99;
			$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, MAX(mt.mapid) AS mapid, mt.timefinish, mt.timeissue FROM $this->usertable ut, $this->companiestable ct, $this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.accid = '$this->accid' AND ut.level <= '$level' AND mt.active = 0 GROUP BY mt.uid";

			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$users = Array();
			$users[0]  = Array('Firstname', 'Surname', 'Email', 'Company', 'Date Completed');
			for($i = 1; $i<=55; $i++) {
				array_push($users[0], 'Q' . $i, 'Timer Q' . $i);
			}
			while( $row = mysql_fetch_object( $result ) ) {
				$rowuid = $row->uid;
				$users[$row->uid] = array('firstname' => $row->firstname, 'surname' => $row->surname, 'email' => $row->email, 'company' => $row->companyname, 'timefinish' => date('d/m/Y', $row->timefinish));
				$i = $i + 1;
				$altsql = "SELECT * FROM $this->surveytable WHERE mapid = '$row->mapid' ORDER BY qid ASC";
				$resultmap = mysql_query( $altsql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
					
				while( $sur_Row = mysql_fetch_object($resultmap ) ) {
					array_push($users[$rowuid], $sur_Row->score, $sur_Row->timer); 
				}

			}//end while
						
			return $users;
			
		}

		public function getUsers($level=9999, $active=false, $adminlist=false, $searchstring='', $limit='', $orderby='') {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			if($active == false) {
				$activeme = '';
			} elseif ($active == 1 || $active == 0) {
				$activeme = "AND ut.status = '$active'";
			}
			
			if($searchstring != '') {
				$searching = "AND ";
				$searchArr = explode(' ', $searchstring);
				$i = 1;
				$scount = count($searchArr);
				foreach($searchArr as $Sstr) {
					if($scount == 1) {
						$operator = '';
					} else {
						if($i != $scount) {
							$operator = ' AND ';
						} else {
							$operator = '';
						}
					}
					$searching .= "(ut.firstname LIKE \"%" . $Sstr . "%\" OR ut.surname LIKE \"%" . $Sstr . "%\" OR ct.companyname LIKE \"%" . $Sstr . "%\")" . $operator;
					$i++;
				}
			} else {
				$searching = '';
			}
			
			$access = $this->getLevelAccess('user');
			$superAccess = $this->getLevelAccess('admin');
			if($adminlist == true) {
				$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ut.authed, ct.companyname, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.accid = '$this->accid' $searching $activeme GROUP BY mt.uid $orderby $limit";
			} else {
				if($superAccess['GODADMIN'] == 'TRUE') {
					$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, ut.authed, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid $searching $activeme GROUP BY mt.uid $orderby $limit";
				} else {
					switch($access['VIEW']) {
						case 'ALL':
						$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, ut.authed, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.accid = '$this->accid' $searching $activeme GROUP BY mt.uid $orderby $limit";
						break;
						case 'CREATED':
						$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, ut.authed, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.accid = '$this->accid' AND (ut.adminid = '$this->uid' OR ct.uid = '$this->uid') $searching $activeme GROUP BY mt.uid $orderby $limit";
						break;
						case 'MA':
						$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, ut.authed, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.accid = '$this->accid' AND ct.headid = '$this->headid' $searching $activeme GROUP BY mt.uid $orderby $limit";
						break;
						case 'SA':
						$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, ut.authed, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.accid = '$this->accid' AND ut.companyid = '$this->companyid' $searching $activeme GROUP BY mt.uid $orderby $limit";
						break;
					}		
				}
			}

			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$users = array();

			while( $row = mysql_fetch_object( $result ) ) {
				$rowuid = $row->uid;
				switch ($row->status) {
					case 1:
						$status = $row->authed == 0 ? 1 : 2;
						
						break;
					
					default:
						$status = $row->status;
						break;
				}
				$users[$row->uid] = array('firstname' => $row->firstname, 'surname' => $row->surname, 'email' => $row->email, 'company' => $row->companyname, 'timefinish' => $row->timefinish, 'timeissue' => $row->timeissue, 'countryid' => $row->countryid, 'level' => $row->level, 'status' => $status, 'companyid' => $row->companyid, 'headid' => $row->headid, 'adminid' => $row->adminid, "owneruid" => $row->owneruid);
				$i = $i + 1;
				/*
				$altsql = "SELECT * FROM $this->surveytable WHERE mapid = '$row->mapid'";
				$resultmap = mysql_query( $altsql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
					
				while( $row = mysql_fetch_object($resultmap ) ) {
					array_push($users[$rowuid], $row->score); 
				}
				*/

			}//end while

			/************************************************************************
			*																																				*
			*												ACCOUNT SHARED USERS HERE												*
			*																																				*
			************************************************************************/
			
			//same as for maps, but this time it just checks for viewing rights (here anyway) it checks for editing and adding and shit from the adminobj / website as that is controlled there this is just the list!

			$sql = "SELECT companyid FROM $this->compshare WHERE uid = '$this->uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			$compids = array();
			$k = 0;
			while( $row = mysql_fetch_object( $result ) ) {
				$compids[$k] = $row->companyid;
				$k++;
			}
			
			//check to see whether anyw companyids have been returned!
			if(sizeof($compids) != 0) { 
				foreach($compids as $compid) {
					$sql = "SELECT allowaccess FROM $this->compshareaccess WHERE uid = '$this->uid' AND companyid = '$compid' AND accesstype = 'VIEW' AND accessgroup = 'user'";
					$result = mysql_query( $sql, $db->mySQLConnection )
						or die(mysql_error() . " Admin Class " . __LINE__) ;
						
					$row = mysql_fetch_object( $result );
					//if they are allowed to view the MAPs add em to the list!
					if($row->allowaccess == 'TRUE') {		
						$sql = "SELECT ut.uid, ct.uid as owneruid, ut.adminid, ut.companyid, ct.headid, ut.firstname, ut.surname, ut.status, ut.email, ut.level, ut.countryid, ct.companyname, MAX(mt.timefinish) as timefinish, mt.timeissue FROM $this->usertable ut,$this->companiestable ct,$this->MAPtable mt WHERE ut.companyid=ct.companyid AND ut.uid=mt.uid AND ut.companyid = '$compid' AND ut.level <= '$level' $searching GROUP BY mt.uid";
						$result = mysql_query( $sql, $db->mySQLConnection )
							or die(mysql_error() . " Admin Class " . __LINE__) ;
						//echo $sql;
						while( $row = mysql_fetch_object( $result ) ) {
							if(!array_key_exists($row->uid, $users)) {
								$users[$row->uid] = array('firstname' => $row->firstname, 'surname' => $row->surname, 'email' => $row->email, 'company' => $row->companyname, 'timefinish' => $row->timefinish, 'countryid' => $row->countryid, 'level' => $row->level, 'status' => $row->status, 'companyid' => $row->companyid, 'headid' => $row->headid, 'adminid' => $row->adminid, "owneruid" => $row->owneruid);
							}
						}//end while
					}
				}
			}
			
			RETURN $users;

			$db->mysqlclose();
		}//end getUsers
		
		public function userActive($uid, $active) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "UPDATE $this->usertable SET status = '$active' WHERE uid = '$uid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			RETURN true;
			
			$db->mysqlclose();
			
		}//end userActive

		public function getAccidVCid($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT accid FROM $this->companiestable WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );

			return $row->accid;
			
			$db->mysqlclose();
			
		}//end updateCompany

		
		public function addUser($username, $firstname, $surname, $companyid, $email, $countryid, $eid, $level, $ulang, $adminid = 0, $retake = 1, $accid = 0) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$allowAdd = false;
			$addAdmin = false;
			$limitAllow = false;
			//checks the access levels to see if user being added is an admin or just a survey taker
			$limitsArr = $this->getLimits();
			$feataccess = $this->getLevelAccess('siteaccess', $level);
			$superAccess = $this->getLevelAccess('admin');
			//admin users are allowed one free MAP regardless of the MAP Storage limit to prevent issues with adding new admin users when MAP limit has been reached. However adding more MAPs to an admin account is tagged as a new MAP and adds towards the MAP storage total
			if($feataccess['ANALYTICS'] == 'TRUE' || $feataccess['ADMIN'] == 'TRUE') {
				$addAdmin = true;
			} else {
				$addAdmin = false;
			}
			
			if($addAdmin == true) {
				if($limitsArr['users'] == 'NOLIMIT') {
					$limit = 'NOLIMIT';
				} else {
					$limit = ($limitsArr['users'] - TOTAL_USERS);
				}
			} else {
				if($limitsArr['maps'] == 'NOLIMIT') {
					$limit = 'NOLIMIT';
				} else {
					$limit = ($limitsArr['maps'] - TOTAL_MAPS);
				}
			}
			
			//this is because you can't do an if on same var if one is a string and another time it is a integer.
			if(is_numeric($limit)) {
				if($limit > 0) {
					$limitAllow = true;
				} else {
					$limitAllow = false;
				}
			} else {
				if($limit == 'NOLIMIT') {
					$limitAllow = true;
				} else {
					$limitAllow = false;
				}
			}

			//set allowAdd to true based on limits etc. If it's false it throws an exception which is caught by the add user function
			if($limitAllow == true || $superAccess['GODADMIN'] == 'TRUE') {
				$allowAdd = true;
			}
			
			//this checks to see what account id the company is under
			if($superAccess['GODADMIN'] == 'TRUE') {
				$accid = $this->getAccidVCid($companyid);
			}

			if($allowAdd == false) {
				if($addAdmin == true) {
					throw new Exception(_('User not added. You have already added the maximum number of admin users to your account.'));
				} else {
					throw new Exception(_('User not added. You have reached your limit for adding MAPs')); 
				}
			}
			
			if($adminid == 0) {
				$adminid = $this->uid;
			}
			
			if($accid == 0) {
				$accid = $this->accid;
			}
			
			$username = $db->EscapeMe(trim($username));
			$firstname = $db->EscapeMe($firstname);
			$surname = $db->EscapeMe($surname);
			$email = $db->EscapeMe(trim($email));
			$randpass = $this->randPass(14);
			$hashstr = md5($firstname . $surname . $randpass);
			$password = md5($randpass);
			$timenow = time();
			$authed = 0;

			//add user to db
			$sql = "INSERT INTO $this->usertable (accid, companyid, countryid, username, firstname, surname, password, email, level, terms, langid, status, retake, optin, adminid, userhash, authed, createdon, authedSent) VALUES('$accid', '$companyid', '$countryid', '$username', '$firstname', '$surname', '$password', '$email', '$level', 0, '$ulang', 1, '$retake', 0, '$adminid', '$hashstr', '$authed', '$timenow', '$timenow')";
			
			if (!$result = @ mysql_query( $sql, $db->mySQLConnection )) {
			 throw new Exception(_('Error adding user to Database. Username/Email Address already in use.')); 
      } else {
      	$uid = mysql_insert_id();
      	$_SESSION['adduid'] = $uid;
      	//if adding an admin user then increment the totals for that admin (MAP total is incremented in the "addMAP" function in MAP-class.php
	      if($addAdmin == true) {
					$this->incrementTotals('user');
				}
      }

			//create a new question obj
			$questobj = new Question();
			$QMAPobj = new MAP();
			$qids = $questobj->getQids();
			shuffle($qids);
			$qidstring = implode(',',$qids);
			//this adds a MAP associated with the new user. All users must have a MAP associated with them in the system!
			if(!$QMAPobj->addMAP($uid, $qidstring, $companyid, 0, $accid)) {
				throw new Exception(_('Error adding MAP to the user account. Please try again')); 
			}
			
			//builds email from db!
			$emailobj = $this->getEmail($eid);

			$tags_original   = array("[USERNAME]", "[PASSWORD]", "[ADMIN_EMAIL]", "[SITEURL]", "[FIRSTNAME]", "[LASTNAME]", "[ADMIN_FN]", "[ADMIN_SN]", "[SURVEYNAME]");
			$tags_replaced = array($username, $password, $this->email, $this->surveyURL . 'saviio/auth/' . $hashstr, $firstname, $surname, $this->firstname, $this->surname, 'Saviio');

			$emailHTML = str_replace($tags_original, $tags_replaced, $emailobj->email_content);

			//now get the template file!
			$email_template = file_get_contents(CONTENT_PATH .'/email_template.php');
			$tags_original = array("[EMAIL_HEADER]", "[EMAIL_COPY]");
			$tags_replaced = array($emailobj->subject, $emailHTML);
			$AdminemailHTML = $emailHTML;
			$emailHTML = str_replace($tags_original, $tags_replaced, $email_template);
			
			//start up the mail class
			$mail = new PHPMailer();
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = MAIL_HOSTNAME;
			//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->Username   = MAIL_USERNAME;
			$mail->Password   = MAIL_PASSWORD;
				
			$mail->From  = NR_EMAIL;
			$mail->FromName = ACC_NAME;
			$mail->CharSet = "UTF-8";
			//$mail->Subject = _("Login Details");
			$mail->Subject = $emailobj->subject;
			$mail->AltBody = _("To view the message, please use an HTML compatible email viewer"); // optional, comment out and test
			$mail->MsgHTML($emailHTML);
			//$mail->AddBCC($this->email, $this->firstname . ' ' . $this->surname);
			$mail->AddAddress($email, $firstname . ' ' . $surname);
			
			
			//Email to Admin
			$adminTags_replaced = array(sprintf(_('New user %s was added'), $email), "<br /><strong>" . sprintf(_("%s %s has been added to the system and was sent the following email."), $firstname, $surname) . "</strong><br/>" . _("No further action is required on your part") . "<hr />" . $AdminemailHTML);
			$AdminemailHTML = str_replace($tags_original, $adminTags_replaced, $email_template);
			
			$adminMail = new PHPMailer();
			$adminMail->IsSMTP(); // telling the class to use SMTP
			$adminMail->Host       = MAIL_HOSTNAME;
			//$adminMail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
			$adminMail->SMTPAuth   = true;                  // enable SMTP authentication
			$adminMail->Username   = MAIL_USERNAME;
			$adminMail->Password   = MAIL_PASSWORD;
				
			$adminMail->From  = NR_EMAIL;
			$adminMail->FromName = ACC_NAME;
			$adminMail->CharSet = "UTF-8";
			//$adminMail->Subject = _("Login Details");
			$adminMail->Subject = $emailobj->admin_subject;
			$adminMail->AltBody = _("To view the message, please use an HTML compatible email viewer"); // optional, comment out and test
			$adminMail->MsgHTML($AdminemailHTML);
			//$adminMail->AddBCC($this->email, $this->firstname . ' ' . $this->surname);
			$adminMail->AddAddress($this->email, $this->firstname . ' ' . $this->surname);
			
			

			if(!$mail->Send()) {
				throw new Exception(_('Error sending email. Please try the resend email invitation function as the user has now been added successfully.')); 
			} else {
				$adminMail->Send();
				RETURN TRUE;
			}

			$db->mysqlclose();

		}//end addUser
	
		public function getSurveyUsage($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT COUNT(mt.timebegin = 0) as issued, COUNT(mt.active = 0) as complete, COUNT(mt.timebegin > 0 AND mt.active = 1) as incomplete, ct.companyname FROM $this->usertable ut LEFT JOIN $this->companiestable ct ON ut.companyid=ct.companyid LEFT JOIN $this->MAPtable mt ON ut.uid=mt.uid WHERE ut.companyid = '$companyid' GROUP BY ut.companyid";
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$users = array();
			$row = mysql_fetch_object( $result );
			$results = array('complete' => $row->complete, 'incomplete' => $row->incomplete, 'issued' => $row->issued);
			
			RETURN $results;

			$db->mysqlclose();
		}//end getQids
		
		public function getTMAPUsage($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$sql = "SELECT SUM(mt.type = 'talent') as talent, SUM(mt.type = 'team') as team FROM $this->TMAPtable mt WHERE mt.companyid = '$companyid'";
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$users = array();
			$row = mysql_fetch_object( $result );
			$results = array('talentMAP' => $row->talent, 'teamMAP' => $row->team);
			
			RETURN $results;

			$db->mysqlclose();
		}//end getQids
		
		
		public function getCompanyDetails($companyid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$compinfo = array();
			//get company name
			$sql = "SELECT companyname FROM $this->companiestable WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );
			$compinfo['name'] = $row->companyname;
			
			//get company map usage
			$sql = "SELECT COUNT(uid) as users FROM $this->usertable WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );
			$compinfo['users'] = $row->users;
			
			//get company map usage
			$currtime = time();
			$day = date('d',$currtime);
			$month = date('m', $currtime);
			$year = date('Y', $currtime);
			$endtime = $currtime;
			$weektime = mktime(0,0,0,($day-7),$month,$year);
			$monthtime = mktime(0,0,0,1,$month,$year);
			$sql = "SELECT SUM(CASE WHEN timefinish BETWEEN '$weektime' AND '$endtime' THEN 1 else 0 end) as week, SUM(CASE WHEN timefinish BETWEEN '$monthtime' AND '$endtime' THEN 1 else 0 end) as month, COUNT(mapid) as total FROM $this->MAPtable WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );
			$compinfo['week'] = $row->week;
			$compinfo['month'] = $row->month;
			$compinfo['total'] = $row->total;
			
			RETURN $compinfo;

			$db->mysqlclose();
		}//end getQids
		
		//this is for the survey usage page on the line graph!
		public function getUsage($monthsarr, $startmonth, $startyear, $mapXtype, $uidstring, $cidstring) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$results = array();
			global $results;

			function getResults($idarr, $maptype, $delim, $reType, $monthsarr, $startmonth, $startyear) {
				if( !isset( $db->mySQLConnection ) ) {
					$db = new DbaseMySQL();
					$db->mySQLConnect();
				}
				global $results;
				$selector_arr = array();
				$k = 0;
				$amobj = new admin($_SESSION['uid']);
				
				if($maptype == "MAP") {
					$createdelim = 'mt.timeissue';
				} else {
					$createdelim = 'mt.created';
				}
				
				foreach($idarr as $identid) {
					$datesql = "SELECT ";
					$i = 0;
					foreach($monthsarr as $nummonth => $currmonth) {
						$currMS = mktime(0, 0, 0, $startmonth+$nummonth, 1, $startyear);
						$numdays = date('t', $currMS);
						$currME = mktime(23, 59, 59, $startmonth+$nummonth, (1+($numdays-1)), $startyear);
						$month = date('M', $currMS);
						$year = date('y', $currMS);
						//$sqlval = $month . $year;
						$sqlval = 'date_' . $currMS;
						if($k == 0) {
							$selector_arr[$i] = $sqlval; 
						}
						//concatanate this SQL string for this user/company data
						if($i == 0) {
							$comma = '';
						} else {
							$comma = ',';
						}
						if($maptype == "MAP") {
							$datesql .= $comma . " SUM(CASE WHEN $createdelim BETWEEN '$currMS' AND '$currME' THEN 1 else 0 end) AS $sqlval";
						} else {
							$datesql .= $comma . " SUM(CASE WHEN $createdelim BETWEEN '$currMS' AND '$currME' AND mt.type = '$maptype' THEN 1 else 0 end) AS $sqlval";
						}
						$i++;
					}
					if($maptype == "MAP") {
						$datesql .= " FROM $amobj->MAPtable mt, $amobj->usertable ut WHERE mt.uid=ut.uid AND $delim = '$identid' ORDER BY mt.mapid";
					} else {
						$datesql .= " FROM $amobj->TMAPtable mt WHERE $delim = '$identid' ORDER BY mt.tmapid";
					}
					//echo $datesql;
					
					$result = mysql_query( $datesql, $db->mySQLConnection )
						or die(mysql_error() . " Admin Class " . __LINE__) ;
	
					while( $row = mysql_fetch_array( $result ) ) {
						foreach($selector_arr as $dbresult) {
							$results[$reType . '_' . $identid]['data'][] = array((substr($dbresult, 5) * 1000), (int) $row[$dbresult]);
						}
					}//end while
					if($reType == 'admin') {
						$nUserObj = new User($identid);
						$nUserObj->getUser($identid);
						$results[$reType . '_' . $identid]['gname'] = $nUserObj->firstname . ' ' . $nUserObj->surname;
					} else if ($reType == 'company') {
						$adminme = new Admin($_SESSION['uid']);
						$compinfo = $adminme->getCompany($identid);
						$results[$reType . '_' . $identid]['gname'] = $compinfo->companyname;
					}
					$k++;
				}
			}	

			if($uidstring != 0) {
				$uidarr = explode(',', $uidstring);
				$reType = 'admin';
				if($mapXtype == "MAP") {
					$delim = 'ut.adminid';
				} else {
					$delim = 'mt.uid';
				}
				getResults($uidarr, $mapXtype, $delim, $reType, $monthsarr, $startmonth, $startyear);
			} 
			if($cidstring != 0) {
				$cidarr = explode(',', $cidstring);
				$reType = 'company';
				$delim = 'mt.companyid';
				if($mapXtype == "MAP") {
					$delim = 'mt.companyid';
				} else {
					$delim = 'mt.companyid';
				}
				getResults($cidarr, $mapXtype, $delim, $reType, $monthsarr, $startmonth, $startyear);
			} 

			RETURN $results;

			$db->mysqlclose();
		}//end getQids
	
		
		public function updateCompany($companyid, $companyname, $compdesc, $countryid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$companyname = $db->EscapeMe($companyname);
			$compdesc = $db->EscapeMe($compdesc);
			
			$sql = "UPDATE $this->companiestable SET companyname = '$companyname', description = '$compdesc', countryid = '$countryid' WHERE companyid = '$companyid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			RETURN true;
			
			$db->mysqlclose();
			
		}//end updateCompany
		
		/***********************************************************************/
		/************************** EMAIL SHIZZLE ******************************/
		/***********************************************************************/
		
		public function getEmail($eid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			//the characters are not displayed correctly when retrieved from the database if this is not set!
			mysql_query("SET character_set_results=utf8", $db->mySQLConnection);

			$sql = "SELECT name, email_content, lang, subject, admin_subject FROM $this->emailtable WHERE eid = '$eid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );
	
			RETURN $row;

			$db->mysqlclose();
		}//end getEmail
		
		public function getEIDviaType($emailType) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			//the characters are not displayed correctly when retrieved from the database if this is not set!
			mysql_query("SET character_set_results=utf8", $db->mySQLConnection);

			$sql = "SELECT eid FROM $this->emailtable WHERE type = '$emailType'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$row = mysql_fetch_object( $result );
	
			RETURN $row->eid;

			$db->mysqlclose();
		}//end getEmail
		
		public function updateEmail($eid, $name, $contents, $lang) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}

			$name = $db->EscapeMe($name);
			$contents = $db->EscapeMe($contents);
			
			$sql = "UPDATE $this->emailtable SET name = '$name', email_content = '$contents', lang = '$lang' WHERE eid = '$eid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
	
			RETURN true;

			$db->mysqlclose();
		}//end getEmail
		
		public function addEmail($name, $contents, $lang) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$name = $db->EscapeMe($name);
			$contents = $db->EscapeMe($contents);
			

			$mandatoryTags = array('[SITEURL]');
			foreach($mandatoryTags as $tag) {
				$pos = strpos($contents, $tag);
				if($pos === false) {
					throw new Exception(_("I'm sorry you must include at least the [SITEURL] field when adding a new email template."));
				}
			}

 			$timenow = time();
			$sql = "INSERT INTO $this->emailtable (uid, name, email_content, type, lang, createdon) VALUES ('$this->uid', '$name', '$contents', 'welcome', '$lang', '$timenow')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
	
			$this->incrementTotals('email');
			
			RETURN true;

			$db->mysqlclose();
		}//end getEmail
		
		public function deleteEmail($eid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
	
			$sql = "DELETE FROM $this->emailtable WHERE eid = '$eid'";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			//email, accid = 0 (use current one), 1 = decrease total)
			$this->incrementTotals('email', 0, 1);
			
			RETURN true;

			$db->mysqlclose();
		}//end deleteEmail
		
		public function getEmailList($show, $langval='en_EN') {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			if($show == 'all') {
				$sql = "SELECT eid, name, lang, createdon FROM $this->emailtable WHERE (uid = '$this->uid' AND lang='$langval') OR (type='default' AND lang='$langval') ORDER BY eid ASC";
			} else {
				$sql = "SELECT eid, name, lang, createdon FROM $this->emailtable WHERE uid = '$this->uid' ORDER BY eid ASC";
			}
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			$i = 0;
			$emails = array();
			while( $row = mysql_fetch_object( $result ) ) {
				$emails[$row->eid] = array("name" => $row->name, "createdon" => $row->createdon, "lang" => $row->lang);
				$i = $i + 1;
			}//end while
			
			RETURN $emails;

			$db->mysqlclose();
		}//end getEmailList
		
		/****************************************************************************/
		/***************************** RUN TRACKER **********************************/
		/****************************************************************************/
		
		public function runTracker($action, $result, $identid, $type) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$result = $db->EscapeMe($result);
			
			$time = time();
			$sql = "INSERT INTO $this->logtable (uid, action, result, identid, type, timestamp, accid) VALUES ('$this->uid', '$action', '$result', '$identid', '$type', '$time', '$this->accid')";
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
				
			return TRUE;
			
			$db->mysqlclose();
		}
		
		public function TotalAccessLog() {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$superAccess = $this->getLevelAccess('admin');
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT lt.action FROM $this->logtable lt";
			} else {
				$sql = "SELECT lt.action FROM $this->logtable lt, $this->usertable ut WHERE ut.uid = lt.uid AND ut.accid = '$this->accid'";
			}
			
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;
			
			$num_rows = mysql_num_rows($result);
			
			return $num_rows;
			
			$db->mysqlclose();
		}
		
		public function getAccessLog($searchstring='',$limit='', $orderby='', $timeStart='', $timeEnd='') {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			$feataccess = $this->getLevelAccess('siteaccess', $level);
			$superAccess = $this->getLevelAccess('admin');
			//admin users are allowed one free MAP regardless of the MAP Storage limit to prevent issues with adding new admin users when MAP limit has been reached. However adding more MAPs to an admin account is tagged as a new MAP and adds towards the MAP storage total
			
			if($orderby != '') {
				$orderby = $db->EscapeMe($orderby);
			}
			
			if($limit != '') {
				$limit = $db->EscapeMe($limit);
			}
			
			//echo $orderby;
			
			if($timeStart != '' && $timeEnd != '') {
				$timeS = explode ('/',$timeStart);
				$timeE = explode ('/',$timeEnd);
				
				$sDay = $timeS[0];
				$sMonth = $timeS[1];
				$sYear = $timeS[2];
				
				$eDay = $timeE[0];
				$eMonth = $timeE[1];
				$eYear = $timeE[2];
				
				$startMK = mktime(0,0,0,$sMonth, $sDay, $sYear);
				$endMK = mktime(23,59,59,$eMonth, $eDay, $eYear);
				$timeStr = "AND lt.timestamp BETWEEN $startMK AND $endMK";
			} else {
				$timeStr = '';
			} 
			
			if($searchstring != '') {
				$searching = "AND ";
				$searchArr = explode(' ', $searchstring);
				$i = 1;
				$scount = count($searchArr);
				foreach($searchArr as $Sstr) {
					if($scount == 1) {
						$operator = '';
					} else {
						if($i != $scount) {
							$operator = ' AND ';
						} else {
							$operator = '';
						}
					}
					$searching .= "(ut.firstname LIKE \"%" . $Sstr . "%\" OR ut.surname LIKE \"%" . $Sstr . "%\" OR lt.action LIKE \"%" . $Sstr . "%\" OR lt.result LIKE \"%" . $Sstr . "%\")" . $operator;
					$i++;
				}
			} else {
				$searching = '';
			}
			
			$superAccess = $this->getLevelAccess('admin');
			if($superAccess['GODADMIN'] == 'TRUE') {
				$sql = "SELECT lt.action, lt.result, lt.timestamp, ut.firstname, ut.surname FROM $this->logtable lt, $this->usertable ut WHERE ut.uid=lt.uid $searching $timeStr $orderby $limit";
			} else {
				$sql = "SELECT lt.action, lt.result, lt.timestamp, ut.firstname, ut.surname FROM $this->logtable lt, $this->usertable ut WHERE ut.uid=lt.uid AND lt.accid = '$this->accid' $searching $timeStr $orderby $limit";
			}
			//echo $sql;
			$result = mysql_query( $sql, $db->mySQLConnection )
				or die(mysql_error() . " Admin Class " . __LINE__) ;

			$accesslog = array();
			$i=0;
			while( $row = mysql_fetch_object( $result ) ) {
				$accesslog[$i] = array("name" => $row->firstname . ' ' . $row->surname, "timestamp" => $row->timestamp, "action" => $row->action, "result" => $row->result);
				$i++;
			}//end while
			
			return $accesslog;
			
			$db->mysqlclose();
		}
	
		
		public function updateCOMP($userarr, $compid) {
			if( !isset( $db->mySQLConnection ) ) {
				$db = new DbaseMySQL();
				$db->mySQLConnect();
			}
			
			foreach($userarr as $userid) {
				$sql = "UPDATE $this->usertable SET companyid = '$compid' WHERE uid = '$userid'";
				$result = mysql_query( $sql, $db->mySQLConnection )
					or die(mysql_error() . " Admin Class " . __LINE__) ;
			}

			$db->mysqlclose();
		}//end getUsers

	
}//end class

?>