	  <?php
	  session_start();
		$phpsessid = session_id();
		$start_time=microtime(true);

		//include localization and site config files
		require_once("../site.config.php");
	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);
	
	//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");

		$userobj = new User();
		$MAPobj = new MAP();
		$qobj = new Question();
		
		$fdescs = $MAPobj->getFactDescs();
	  $stquestions = $qobj->getSTResponses();
	 $db = new DbaseMySQL();
		foreach($stquestions as $qid => $qarr) {
			echo '_("' . $qarr['l'] . '")' . "\n";
			echo '_("' . $qarr['r'] . '")' . "\n";
		}

		foreach($fdescs as $fid => $typearr) {
			foreach($typearr as $content) {
				foreach($content as $text) {
					echo '_("' . $text . '")' . "\n";
				}
			}
		}

    ?>