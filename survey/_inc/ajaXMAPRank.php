	  <?php
	  session_start();
		$phpsessid = session_id();
		$start_time=microtime(true);
		//set initial vars
		//ini_set("display_errors",1);
		
		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
		
		$userobj = new Admin($_SESSION['uid']);
		$MAPobj = new MAP();
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");
		

		$fids = $MAPobj->getfids();
		$fgs = $MAPobj->getFactorGroups();
		$cids = $MAPobj->getClusters();
		$stquestions = $_SESSION['stquestions'];
		
		$access = $userobj->getLevelAccess('tmap');
		if($access['EDIT'] == 'MA') {
			$compid = $userobj->headid;
		} else {
			$compid = $userobj->companyid;
		}
		
		//get factor scores set from AjaXGen Script!
		$arrFactorScores = $_SESSION['arrFactorScores'];
		$arrDeviations = $_SESSION['arrDeviations'];
		if(!isset($_SESSION['fdescs'])) {
			$fdescs = $MAPobj->getFactDescs();
			$_SESSION['fdescs'] = $fdescs;
		} else {
			$fdescs = $_SESSION['fdescs'];
		}
		$rankid = $_GET['rankID'];
		$_SESSION['rankID'] = $_GET['rankID'];
		
		//generate HTML for the select menus (if they are MAP's) if not show edit link!
		if($_SESSION['leftType'] == 'MAP') {
			$leftswitchHTML = GenSelectHTML($arrFactorScores[$rankid]['leftmapUID'], 'left', $MAPobj, $arrFactorScores[$rankid]['l_mapids'], $arrFactorScores[$rankid]['leftname']);
		} else {
			if(!$MAPobj->checkTMAPAccess($arrFactorScores[$rankid]['leftmapid'], $compid, $userobj->uid, $access)) {
				$leftswitchHTML = sprintf(_('This %s is read only'), $_SESSION['leftType']);
			} else {
				$leftswitchHTML = '<strong style="font-size: 11px;">' . _("Edit this") . ' ' . $_SESSION['leftType'] . '</strong><br /><div class="fl marginpad"><img src="' . ACCOUNT_PATH . '_images/_icons/IC_EditTMAP.png" /></div><div class="fl marginpad"><a href="javascript:void(0)" id="tmap_' . $arrFactorScores[$rankid]['leftmapid'] . '" class="edittmap">' . $arrFactorScores[$rankid]['leftname'] . '</a></div><div class="cleaner"></div>';
			}
		}
		
		if($_SESSION['rightType'] == 'MAP') {
			$rightswitchHTML = GenSelectHTML($arrFactorScores[$rankid]['rightmapUID'], 'right', $MAPobj, $arrFactorScores[$rankid]['r_mapids'], $arrFactorScores[$rankid]['rightname']);
		} else {
			if(!$MAPobj->checkTMAPAccess($arrFactorScores[$rankid]['rightmapid'], $compid, $userobj->uid, $access)) {
				$rightswitchHTML = sprintf(_('This %s is read only'), $_SESSION['rightType']);
			} else {
				$rightswitchHTML = '<strong style="font-size: 11px;">' . _("Edit this") . ' ' . $_SESSION['rightType'] . '</strong><br /><div class="fl marginpad"><img src="' . ACCOUNT_PATH . '_images/_icons/IC_EditTMAP.png" /></div><div class="fl marginpad"><a href="javascript:void(0)" id="tmap_' . $arrFactorScores[$rankid]['rightmapid'] . '" class="edittmap">' . $arrFactorScores[$rankid]['rightname'] . '</a></div><div class="cleaner"></div>';
			}
		}

 		require_once('ajaXDisplayResults.php');
    
    echo '---' . $_SESSION['leftType'] . '---' . $_SESSION['rightType'] . '---' . $arrFactorScores[$rankid]['leftname'] . '---' . $arrFactorScores[$rankid]['rightname'];
    //echo select / edit map HTML
   	echo '---' . $leftswitchHTML;
    echo '---' . $rightswitchHTML;
    $leftUser = new Admin($arrFactorScores[$rankid]['leftmapUID']);
    $rightUser = new Admin($arrFactorScores[$rankid]['rightmapUID']);
    //this is so idealMAPs do not display the creators avatar as leftmapUID etc is set for idealMAPs when it's slightly different to MAPs still
    if($_SESSION['leftType'] == 'MAP') {
    	$luAva = $leftUser->avatarURL;
    } else {
    	$luAva = '';
    }
     if($_SESSION['rightType'] == 'MAP') {
    	$ruAva = $rightUser->avatarURL;
    } else {
    	$ruAva = '';
    }
	  echo '---' . $luAva;
	  echo '---' . $ruAva;
    
    $end_time=microtime(true);

		$time_taken=$end_time-$start_time;
		//echo $time_taken;
    ?>