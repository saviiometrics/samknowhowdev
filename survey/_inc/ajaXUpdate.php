	  <?php
	  session_start();
		$phpsessid = session_id();
	
		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
		
		//include other classes
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		include FULL_PATH . '/_inc/_classes/admin-class.php';
		
		$MAPobj = new MAP();
		$adminobj = new Admin($_SESSION['uid']);
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");
		
		if(isset($_GET['getTMAP'])) {
			$MAPobj = new MAP();
			$TMAPobj = $MAPobj->getTMAP($_GET['tmapid']);
			echo $TMAPobj->tmapname . '---' . $TMAPobj->tmapdesc . '---' . $TMAPobj->mapids;
		}
	
		if(isset($_GET['updateTMAP'])) {
			if(!$MAPobj->updateTMAP($_GET['tmapid'], $_GET['newmapids'])) {
				printf(_("Your %s was not updated. Please Try again!"), $_GET['type']);
			} else {
				printf(_("Your %s was updated successfully!"), $_GET['type']);
				$adminobj->runTracker('Updated ' . trim($_GET['type']), trim($_GET['tmapname']), $_GET['tmapid'], 'TMAP');
			}
		}
		
		if(isset($_GET['addTMAP'])) {
			if($_GET['type'] == 'TalentMAP') {
				$maptype = 'talent';
			} else if($_GET['type'] == 'TeamMAP') {
				$maptype = 'team';
			}
			if(!$MAPobj->addTMAP($maptype, $adminobj->companyid, $adminobj->uid, $_GET['tmapname'], $_GET['tmapname'], $_GET['newmapids'], 0, 5, 0)) {
				printf(_("Your %s was not added. Please Try again!"), $_GET['type']);
			} else {
				printf(_("Your %s was added successfully!"), $_GET['type']);
				$newtmapid = mysql_insert_id();
				$adminobj->runTracker('Added ' . trim($_GET['type']), trim($_GET['tmapname']), $newtmapid, 'TMAP');
			}
		}

		if(isset($_GET['getnewTMAPids'])) {
			$newmapids = $MAPobj->getUpdatedTMAPids($_GET['oldmapids']);
			$newmapids = implode(',', $newmapids);
			echo $newmapids;
		}
		
		if(isset($_GET['updateAvatar'])) {
			if($_GET['gravatarType'] == 'goFB') {
				//{square|small|large}
				$uid = $_GET['uidstring'];
				$ava_url = 'http://graph.facebook.com/' . $uid . '/picture?type=square';
				$avaType = 'facebook';
			}
			if($_GET['gravatarType'] == 'goTW') {
				//{mini|bigger|normal}
				$username = $_GET['uidstring'];
				$ava_url = 'http://api.twitter.com/1/users/profile_image/' . $username . '?size=normal';
				$avaType = 'twitter';
			}
			if($_GET['gravatarType'] == 'goGR') {
				$default = "http://static.saviio.com/_images/_misc/gravatar.jpg";
				$size = 50;
				$ava_url = "http://www.gravatar.com/avatar/" . md5( strtolower( $adminobj->email ) ) . "?default=" . urlencode( $default ) . "&size=" . $size;
				$avaType = 'gravatar';
			}
			if(!$adminobj->updateAvatar($avaType, $ava_url)) {
				$message = _('Avatar update was not successful, please try again');
			} else {
				$message = _('Avatar successfully updated.');
			}
			$avaArr = array("message" => $message, "avaURL" => $ava_url);
			echo json_encode($avaArr);
		}
		
		if(isset($_GET['regenToken'])) {
			try {
				$newtoken = $adminobj->regenToken();
				$message = _("API Token regenerated successfully.");
			} catch (Exception $e) {
				$message = $e->getMessage();
				$newtoken = '';
			}
			$regenArr = array("message" => $message, "apitoken" => $newtoken);
			echo json_encode($regenArr);
		}
		
		if(isset($_GET['updateDetails'])) {
			if(!$adminobj->updateDetails($adminobj->uid, $_GET['firstname'], $_GET['surname'], $_GET['newemail'], $_GET['newpassword'])) {
				$message = _('There was an error updating your details');
			} else {
				$adminobj->runTracker('Updated their details', $_GET['firstname'] . ' ' . $_GET['surname'], $_GET['uid'], 'USER');
				$message = _('Your details were updated successfully');
			}
			$userArr = array("message" => $message);
			echo json_encode($userArr);
		}
	
    ?>