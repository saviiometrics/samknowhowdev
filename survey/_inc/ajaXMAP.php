<?php
	session_start();
	$phpsessid = session_id();
	
	//include localization and site config files
	require_once("../site.config.php");

	//include DB AND ACCOUNT INFO CLASSES
	include CONTENT_PATH . '/_classes/db-class.php';
	include CONTENT_PATH . '/_classes/account-class.php';
	$accobj = new Account($_SESSION['accid']);
	
	//include other classes
	include FULL_PATH . '/_inc/_classes/question-class.php';
	include FULL_PATH . '/_inc/_classes/MAP-class.php';
	include FULL_PATH . '/_inc/_classes/user-class.php';
	
	//this would be session UID obv!
	$uid = $_SESSION['uid'];
	$userobj = new User();
	$userobj->getUser($uid);
	
	require_once(FULL_PATH . "/_inc/localization.php");
	require_once(FULL_PATH . "/_inc/scripts.php");
	
	if(isset($_GET['cQ'])) {
		if(isset($_SESSION['cQ'])) {
			$MAPobj = new MAP();
			$questobj = new Question();
			$qid = $_SESSION['qids'][$_SESSION['cQ']];
			
			//get factorgroup and add score
			$factorgroup = $questobj->getFactorGroup($qid);
			if(($_SESSION['cQ'] + 1) < count($_SESSION['qids'])) {
				$finishtime = 0;
			} else {
				$finishtime = time();
				//remove a retake
				if($userobj->level != 1) {
					$retake = $userobj->retake;
					$retake--;
					$userobj->updateRetake($retake);
				}
			}

			$MAPobj->addScore($_GET['currmapid'], $qid, $_GET['userscore'], $_GET['timeval'], $factorgroup, $_GET['npolar'], $_SESSION['cQ'], $finishtime);
		
			//get next question and responses from DB and increment current question
			$_SESSION['cQ']++;
			if($_SESSION['cQ'] < count($_SESSION['qids'])) {
				$qid = $_SESSION['qids'][$_SESSION['cQ']];
				$responses = $questobj->getResponses($qid);
				
				//this creates a random left or right
				$align = array("l", "r");
				shuffle($align);
			
				$left = $responses[$align[0]];
				$right = $responses[$align[1]];
				
				//check if left response = response position on page
				if($responses["l"] == $left) {
					$polarize = 0;
				} else {
					$polarize = 1;
				}
		
				echo _($left) . '---' . _($right) . '---' . $polarize;
			} else {
				echo 'ended---';
				$mail = new PHPMailer();
				
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = MAIL_HOSTNAME; // SMTP server
				//$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = MAIL_USERNAME; // SMTP account username
				$mail->Password   = MAIL_PASSWORD;        // SMTP account password
				
				$aduserobj = new User();
				$aduserobj->getUser($userobj->adminid);
				$body .= sprintf(_("This email is to notify you that %s %s has completed a %s survey."), $userobj->firstname, $userobj->surname, ACC_NAME);
				$body .= '<br /><br />' . _("Thank you") . '<br /><br />';
				$body .= '<strong>' . ACC_NAME . '</strong>';
				
				$mail->From  = NR_EMAIL;
				$mail->FromName = ACC_NAME;
				$mail->CharSet = "UTF-8";
				$mail->Subject = $userobj->firstname . ' ' . $userobj->surname . ' - ' . _("Survey Completed");
				$mail->AltBody = _("To view the message, please use an HTML compatible email viewer."); // optional, comment out and test
				$mail->MsgHTML($body);
				$mail->AddAddress($aduserobj->email, $aduserobj->firstname . ' ' . $aduserobj->surname);
				$mail->Send();
			}
		} else {
			echo 'TIMEOUT';
		}//end check session set
		
	}
	?>