  		<?php
  		$uid = $userobj->uid;
			$superAccess = $userobj->getLevelAccess('admin');
  		$questobj = new Question();
  		$MAPobj = new MAP();
  		//curr q is sessioncQ - 1 (as array starts at 0)
  		if($MAPobj->checkCurrentMAP($uid)) {
  			$MAPobj->getMAP($uid);
  			if($MAPobj->timebegin == 0) {
  				$timenow = time();
  				$MAPobj->setStart($MAPobj->mapid, $timenow);
  			}
  			$qids = explode(',',$MAPobj->qidstring);
  			$currentq = ($MAPobj->currentq - 1);
  			$mapid = $MAPobj->mapid;
  			$_SESSION['cQ'] = $currentq;
  			$allowMAP = 1;
  		} else {
  			//might add a master admin tag in here too?
  			if($superAccess['GODADMIN'] == 'TRUE' || $userobj->retake != 0) {
  				//check company credits based on userid here
	  			$qids = $questobj->getQids();
	  			shuffle($qids);
	  			$qidstring = implode(',',$qids);
	  			$currentq = 0;
	  			$_SESSION['cQ'] = $currentq;
	  			//1 value means adding from the actual MAP page (for retakes etc of admins so it adds start time too not just issue time(as its starting it when issuing it :))
	  			$mainsite = 1;
	  			$MAPobj->addMAP($uid, $qidstring, $userobj->companyid, $mainsite, $userobj->accid);
	  			//update user now and set retake to 0 (if not an admin);
	  			$mapid = mysql_insert_id();
	  			$allowMAP = 1;
	  		} else {
	  			$allowMAP = 0;
	  		}
  		}
  		
  		if($allowMAP == 1) {
				$barsize = ceil((120 / count($qids)) * ($currentq + 1));
				$barincrement = ceil(120 / count($qids));
				//$barincrement = 1.8;
	
	  		//store qids in session!
				$_SESSION['qids'] = $qids;
	  		$responses = $questobj->getResponses($qids[$currentq]);
		
	  		//this creates a random left or right
	  		$align = array("l", "r");
	  		shuffle($align);
	  	}
			//echo $barincrement;
  		?>
  		
  		<script type="text/javascript">
  			$(function() {
  				var listNum = 1;
  				var listObj = new Object;
	  			$('#addtoList').click(function(){
		  			if($('.introUserText').is(':visible')) {
	  					$('.introUserText').hide();
	  				}
	  				var valid = $("#addUserToList").validationEngine({returnIsValid:true,promptPosition: "topRight"});
	  				if(valid == true) {
		  				$('#userlistbox').append('<div class="userListTab" id="list_tab_' + listNum + '">' + $('#firstname').val() + ' ' + $('#surname').val() + ' (' + $('#emailaddy').val() + ')</div>');
		  				listObj['list_' + listNum] = {firstname:$('#firstname').val(), surname:$('#surname').val(), email:$('#emailaddy').val()};
		  				listNum++;
		  				$.each(listObj, function(index, value) {
		  					$.each(value, function(indexa, valuen) {
		  						//
		  					});
		  				});
	  				}
	  			});
	  			$("#addUserToList").validationEngine('attach');
  			});
  		</script>

  		<input type="hidden" name="surcompletehead" id="surcompletehead" value="<?=_("Survey Completed");?>" />
  		<input type="hidden" name="completesurveycopy" id="completesurveycopy" value="<?=_("Thank you for completing this survey");?>" />
  		<input type="hidden" name="sessiontimeout" id="sessiontimeout" value="<?=_("I'm sorry, your session has timed out. Please click 'Logout' at the top of the page and login in again. You will be returned to your current question!");?>" />
  		<input type="hidden" name="moveslider" id="moveslider" value="<?=_("You must move the slider before continuing. If you wish to select the centre value please move the pointer away and then back to the centre point again.");?>" />
  		
  		<div id="mainContentHolder">
  			<?php if(isset($_SESSION['username'])) { ?>
        	<div id="titleBoxHolder">
           	  <div id="iconHolder"><img src="<?=ACCOUNT_PATH;?>_images/_header/mymap_header.jpg" width="89" height="74" /></div>
                <div class="MWxTitleHolder"><h2 class="MWxBox"><?=_("Create your MAP");?></h2>
                </div>
            </div>
            <?php if($allowMAP == 1) { ?>
            <div id="progressBarHolder">
            	<div id="progressText"><h3 class="MWxTitle"><?=_("Progress");?></h3></div>
            	<div id="progressBarContainer"><img src="<?=ACCOUNT_PATH;?>_images/_survey/progress_bar.jpg" width="<?=$barsize;?>" height="5" alt="<?=_("Progress Bar");?>" id="progressbar" /></div>
            	<div id="QoutOf"><span id="currq"><?=($currentq + 1);?></span> <?=_("of") . ' ' . count($qids);?></div>
            </div>
            <div class="cleaner"></div>
            <div id="responseBoxTop"></div>
            <div id="responseBoxMid">
            		<?php
								$left = $responses[$align[0]];
								$right = $responses[$align[1]];
								
								//check if left response = response position on page
								if($responses["l"] == $responses[$align[0]]) {
									$polarize = 0;
								} else {
									$polarize = 1;
								}
								
  							?>
  							<div id="questionnum"><?=_("Statement");?> <span id="currqbig"><?=($currentq + 1);?></span></div>
  							<div id="survey_container">
	                <div id="left_response"><?=_($left);?></div>
	                <div id="slider"></div>
	                <div id="right_response"><?=_($right);?></div>
	              </div>
	             	<div class="cleaner"></div>
	             	<?php
	             	/* 
	             	
	             	//This is for later on when using free trial after people have completed survey!//
	             	
	             	<div style="float:left; padding:0px 32px 0px 38px;">
		              <div class="largeHeader"><?= _("Survey Completed");?></div>
			    				<h2 class="MWxMedium"><?= _("Invite friends and colleagues to take the survey!");?></h2>
			    				<p>Now that you've completed your survey why not invites your friends and colleagues to take the survey as well? That way you can compare each others motivations and preferences! Some more copy here about such and such so pad it out a bit</p>
			    				<p>To view your results now go to the analytics Tab on the top menu and type your name in the search area on the names list. Make sure you have the type set to MAP too and not TalentMAP!</p>
			    				<div style="float:left; width:400px; font-size:14px; font-weight:bold; margin:12px 0px 0px 0px;">
			    					<h2 class="MWxMedium"><?= _("User Details");?></h2><br />
			    					<form name="addUserToList" id="addUserToList">
				    					<table border="0" cellpadding="5" cellspacing="0">
				    						<tr>
				    							<td width="120">Firstname</td>
				    							<td><input type="text" name="firstname" id="firstname" class="validate[required] textfield_invite" /></td>
				    						</tr>
				    						<tr>
				    							<td>Surname</td>
				    							<td><input type="text" name="surname" id="surname" class="validate[required] textfield_invite" /></td>
				    						</tr>
				    						<tr>
				    							<td>Email Address</td>
				    							<td><input type="text" name="emailaddy" id="emailaddy" class="validate[required,length[3,32],custom[email]] textfield_invite" /></td>
				    						</tr>
				    						<tr>
				    							<td>&nbsp;</td>
				    							<td align="right"><a href="javascript:void(0);" id="addtoList" style="width:160px; background:#68a2d0; height:30px; font-size: 12px; line-height:30px; display:block; cursor:pointer; color:#FFF; font-weight:bold; text-align:center; -moz-border-radius:4px;-webkit-border-radius:4px;-opera-border-radius:4px;-khtml-border-radius:4px;border-radius:4px;">Add user to List &gt;</a></td>
				    						</tr>
				    					</table>
				    				</form>
			    			</div>
			    			
			    			<div style="float:left; width:400px; font-size:12px; font-weight:bold; margin:12px 0px 0px 28px;">
		    					<h2 class="MWxMedium"><?= _("User List");?></h2>
		    					<div id="userlistbox">
										<p class="introUserText">No user's have current been added to the list. Add them from the form on the left!</p>
									</div>
			    			</div>
			    			
			    			<div class="cleaner"></div>
		    			</div>
		    			*/
		    			?>
		    				
            </div>
            <div id="responseBoxBottom"></div>
		         <input type="hidden" name="polarize" id="polarize" value="<?=$polarize;?>" />
		        <input type="hidden" name="mapid" id="mapid" value="<?=$mapid;?>" />
		        <input type="hidden" name="barincrement" id="barincrement" value="<?=$barincrement;?>" />
		        
		        <div id="nextqholder"><div id="nextbutton"><a href="javascript:void(0);"><?=_("Next"); ?></a></div></div>
				    <div id="amount"></div>
				    <div id="timerdiv"></div>
					<?php 
		     	 } else { 
		     	 		echo '<div class="cleaner"></div>';
		     	 		echo _("Sorry, you are not allowed to retake the Survey");
		    	 } 
		    	} else {
		    		echo _("Sorry you must be logged in to view this page");
		    	}
		    	?>
        </div>


     	 

        

