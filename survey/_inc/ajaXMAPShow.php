	  <?php
	  session_start();
		$phpsessid = session_id();
		$start_time=microtime(true);

		//include localization and site config files
		require_once("../site.config.php");
		//include DB AND ACCOUNT INFO CLASSES
		include CONTENT_PATH . '/_classes/db-class.php';
		include CONTENT_PATH . '/_classes/account-class.php';
		$accobj = new Account($_SESSION['accid']);
	
		//include other classes
		include FULL_PATH . '/_inc/_classes/question-class.php';
		include FULL_PATH . '/_inc/_classes/MAP-class.php';
		include FULL_PATH . '/_inc/_classes/user-class.php';
		
		$userobj = new User();
		$MAPobj = new MAP();
		
		require_once(FULL_PATH . "/_inc/localization.php");
		require_once(FULL_PATH . "/_inc/scripts.php");


		$fids = $MAPobj->getfids();
    $fgs = $MAPobj->getFactorGroups();
    $cids = $MAPobj->getClusters();
    $qobj = new Question();
    $stquestions = $qobj->getSTResponses();
    $_SESSION['stquestions'] = $stquestions;
		
		$leftside = $_GET['leftType'];
		$_SESSION['leftType'] = $leftside;

		//rankLink
		switch($leftside) {
			case 'idealMAP':
			$l_mapids = $_GET['leftMAP'];
			$imapObj = $MAPobj->getiMAPvid($_GET['leftMAP']);
			$leftname = $imapObj->name;
			$leftmapUID = $imapObj->uid;
			$leftEmail = null;
			$issuer = $userobj->getUserObject($imapObj->uid);
			$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
			$leftIssueData['issuedbyemail'] = $issuer->email;
			$leftIssueData['completedon'] = $imapObj->time_created;
			//$leftswitchHTML = GenSelectHTML($leftmapUID, 'left', $MAPobj, $l_mapids);
			$leftswitchHTML = '';
			break;
			case 'MAP':
			$l_mapids = $_GET['leftMAP'];
			$tmapobj = $MAPobj->getMAPvid($_GET['leftMAP']);
			$leftname = $tmapobj->firstname . ' ' . $tmapobj->surname;
			$leftmapUID = $tmapobj->uid;
			$leftEmail = $tmapobj->email;
			$issuer = $userobj->getUserObject($tmapobj->uid);
			$issuer = $userobj->getUserObject($issuer->adminid);
			$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
			$leftIssueData['issuedbyemail'] = $issuer->email;
			$leftIssueData['issuedon'] = $tmapobj->timeissue;
			$leftIssueData['completedon'] = $tmapobj->timefinish;
			//get multigen left map select0r
			$leftswitchHTML = GenSelectHTML($leftmapUID, 'left', $MAPobj, $l_mapids, $leftname);
			break;
			case 'TeamMAP':
			$l_mapids = $MAPobj->getmapidsVTMAP($_GET['leftMAP']);
			$tmapobj = $MAPobj->getTMAP($_GET['leftMAP']);
			$leftname = $tmapobj->tmapname;
			$leftswitchHTML = '<a href="javascript:void(0)" id="tmap_' . $_GET['leftMAP'] . '" class="edittmap">Edit this ' . $leftside . '</a>';
			$issuer = $userobj->getUserObject($tmapobj->uid);
			$leftEmail = null;
			$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
			$leftIssueData['issuedbyemail'] = $issuer->email;
			$leftIssueData['completedon'] = $tmapobj->created;
			break;
			case 'TalentMAP':
			$l_mapids = $MAPobj->getmapidsVTMAP($_GET['leftMAP']);
			$tmapobj = $MAPobj->getTMAP($_GET['leftMAP']);
			$leftname = $tmapobj->tmapname;
			$leftswitchHTML = '<a href="javascript:void(0)" id="tmap_' . $_GET['leftMAP'] . '" class="edittmap">Edit this ' . $leftside . '</a>';
			$issuer = $userobj->getUserObject($tmapobj->uid);
			$leftEmail = null;
			$leftIssueData['issuedbyname'] = "$issuer->firstname $issuer->surname";
			$leftIssueData['issuedbyemail'] = $issuer->email;
			$leftIssueData['completedon'] = $tmapobj->created;
			break;
		}	
		//set session var for lmapids
		$_SESSION['l_mapids'] = $l_mapids;
		if($leftside == 'idealMAP') {
			$leftscores = $MAPobj->getiScores($l_mapids);
		} else {
			$leftscores = $MAPobj->getScores($l_mapids);
		}
		
		//don't rank if only 1v1! as that's comparing innit :)
		$arrFactorScores = array();
		$arrDeviations = array();
	
		//remember arrFactorScores is generated in the loop function below! 
		loopScoreSingle($leftscores, $l_mapids, $leftname, $leftmapUID, $_GET['leftMAP'], $leftIssueData, $leftEmail);
		
		//get factor descriptions
		if(!isset($_SESSION['fdescs'])) {
			$fdescs = $MAPobj->getFactDescs();
			$_SESSION['fdescs'] = $fdescs;
		} else {
			$fdescs = $_SESSION['fdescs'];
		}
			
		//this must be set to 0 when no rankings are being used
		$rankid = 0;
		require_once('ajaXShowSingle.php');
		
	  echo '---' . $arrFactorScores[$rankid]['leftname'];
		    
		if($leftside != 'MAP' && $leftside != 'idealMAP') {
			//work out users in map/team_map
			$usercount = count($usernames);
			$usernames = $MAPobj->getNamesVMAPids($arrFactorScores[$rankid]['l_mapids']);
			$mapids = explode(',', $arrFactorScores[$rankid]['l_mapids']);
			sort($mapids);
			$i = 0;
			$userstring .= _("Click the names below to view the results for that individual on the scale") . '<br /><br />';
			foreach($usernames as $uid => $username) {
				$userstring .= '<a href="javascript:void(0);" class="MAPoverlay" id="mol_' . $mapids[$i] . '">' . $username['fullname'] . '</a><br />';
				$i++;
			}
			$userstring .= '<br /><br /><a href="javascript:void(0);" class="clearOL">' . _("Clear All") . '</a>';
			$userstring .= '<br /><br /><strong style="font-size: 11px;">Edit this ' . $leftside . '</strong><br><div class="fl marginpad"><img src="' . ACCOUNT_PATH . '_images/_icons/IC_EditTMAP.png"></div><div class="fl marginpad"><a href="javascript:void(0)" id="tmap_' . $_GET['leftMAP'] . '" class="edittmap">' . $arrFactorScores[$rankid]['leftname'] . '</a></div><div class="cleaner"></div><br />';
			echo '---' . $userstring;
		} elseif ($leftside == 'MAP') {
			//The reason no --- is needed is simply because the div is a float so just appears under the name anyway! ;) Could add it in so that it gets put into correct place, but I'll leave that till we move over everything to JSON!
			echo $leftswitchHTML;
		}
	  
	  //these sessions are set so that the ajaXMAP rank file has access to them without DB access!
	  $_SESSION['arrFactorScores'] = $arrFactorScores;
	  $_SESSION['arrDeviations'] = $arrDeviations;
	  $end_time = microtime(true);

		$time_taken = $end_time-$start_time;
		//echo $time_taken; 
    ?>